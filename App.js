import { registerNotificationHandler} from './src/common/pushNotifications';
registerNotificationHandler();
import React, { Component } from "react";
import {
    AsyncStorage,
    ActivityIndicator,
    View,
    StatusBar,
    AppState,
    I18nManager,
} from 'react-native';

import RootNavigation from "./appNavigation";
import { createStore, applyMiddleware, } from 'redux';
import reducerRoot from './src/state/reducerRoot';
import reduxThunk from 'redux-thunk';
import { Provider } from "react-redux";
import { createLogger } from 'redux-logger';
import { initialState as appInitialState } from './src/state/reducerApp';
import { initialState as postsInitialState } from './src/state/reducerPosts';
import { startLocationTracking } from './src/common/location';
import { reportError, initErrorReporter } from './src/common/errorReporter';
import { fetchOwnUserDetails } from './src/state/actionsUser';
import { appStateChanged } from './src/state/actionsApp';
import { initAuth, setupGoogleSignIn, initializeLoggedInUser } from './src/common/authApi';
import NavigationService from './src/common/navigationService';
import I18n from 'react-native-i18n';
import StyleConstants from './src/styles/constants';


export default class App extends Component {
    constructor(props) {
        super(props);
        this.state = {
            initializing: true
        }
    }

    // NBTODO: I have a feeling there's a more elegant way to load state for Redux than this.
    //         Read about it...
    //         One problem I have is that if I assign the initial state from the reduceXYZ file
    //         the copy is not deep and so things start to break when a deep value changes.
    //         For now doing it in an ugly way.
    loadState = () => {
        let state = {
            app: JSON.parse(JSON.stringify(appInitialState)),
            user: {
                tokens: {},
                info: {},
            },
            posts: JSON.parse(JSON.stringify(postsInitialState)),
            
        }


        return AsyncStorage.getItem('user.tokens')
            .then((res) => {

                if (res) {
                    let tokens = JSON.parse(res);
                    state.user.tokens = tokens;
                    state.app.showPostsCoachMarks = false; // Don't show coachmarks if the user was already logged in
                }

                if (state.user.tokens.refreshToken != null) {
                    console.info("Successfully loaded tokens from storage. Logging in.");
                }
                else {
                    console.info("Did not find tokens in storage. Asking user to login.")
                }
            })
            .then(() => { return AsyncStorage.getItem('user.info') })
            .then((res) => {
                if (res) {
                    const userInfo = JSON.parse(res);

                    if (userInfo) {
                        state.user.info.id = userInfo.id;
                        state.user.info.googleUser = userInfo.googleUser;
                    }
                }
            })
            .then(() => {
                return AsyncStorage.getItem("posts.filter");
            })
            .then((res) => {
                if (res) {
                    const postsFilter = JSON.parse(res);

                    if (res) {
                        state.posts.filter = postsFilter;
                    }
                }
            })
            .catch((error) => {
                reportError("Failed to load state from storage: ", error);
            })
            .then(() => {
                // Start the app whether state load succeeded or not...
                return state;
            });
    }

    initializeApp = async () => {
        const state = await this.loadState();
        const logger = createLogger(
            {
                logger: console,
                stateTransformer: (state) => {
                    // let items = [];
                    // state.posts.items.forEach((item) => { items.push(item._id)});
                    // return state.app;
                    // return state.location; 
                    // return state.user.info;
                    // return state.posts.locationSearch.isSearchFocused;
                    return 'not logging actions for log clarity';
                },
                actionTransformer: (action) => {
                    let result = {};
                    result.type = action.type;
                    if (action.type === 'SET_SEARCH_FOCUSED') {
                        result = action;
                        // action.posts.forEach((item) => { result.items.push(item._id)});
                    }
                    return result;
                }
            });

        this.reduxStore = createStore(reducerRoot, state, applyMiddleware(reduxThunk, /* logger*/));
        startLocationTracking(this.reduxStore);
        initAuth(this.reduxStore);
        setupGoogleSignIn();

        initializeLoggedInUser();
        // Track whether the app is in the foreground or not so we can disable some stuff to conserve battery.
        AppState.addEventListener('change', (state) => {
            this.reduxStore.dispatch(appStateChanged(state));
        });

        // If we have a token at app start, fetch the user's details.
        if (state.user.tokens.refreshToken != null) {
            this.reduxStore.dispatch(fetchOwnUserDetails());
        }

        initErrorReporter(this.reduxStore);

        const currentLocale = I18n.currentLocale().toLowerCase();
        // Only allow RTL for Hebrew for now.
        if (!(currentLocale.startsWith('he') || currentLocale.startsWith('iw'))) {
            I18nManager.allowRTL(false);
        }

        this.setState({ initializing: false });
    }

    componentDidMount = () => {
        this.initializeApp();
    }

    render() {
        if (this.state.initializing) {
            return (
                <View style={{ flex: 1, alignItems: "center", justifyContent: "center"}}>
                    <ActivityIndicator color={StyleConstants.BRAND_COLOR_DARK} size="large"/>
                    <StatusBar barStyle="default" />
                </View>
            )
        }

        return (
            <Provider store={this.reduxStore}>
                <RootNavigation ref={navigatorRef => {
                    NavigationService.setTopLevelNavigator(navigatorRef);
                }} />
            </Provider>
        )
    }
}

