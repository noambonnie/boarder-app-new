import React from 'react';
import {
    StatusBar,
    Platform,
} from 'react-native';
import {
    createStackNavigator,
    createDrawerNavigator,
    createBottomTabNavigator,
    createMaterialTopTabNavigator,
    createSwitchNavigator,
} from 'react-navigation';

import LoginScreen from './src/components/createAccountContainer';
import Drawer from './src/components/drawer';
import PostsList from './src/components/postsListContainer';
import PostView from './src/components/postViewContainer';
import PostAdd from './src/components/postAddContainer';
import ProfileView from './src/components/profileViewContainer';
import ProfileEdit from './src/components/profileEditContainer';
import ImageViewer from './src/controls/ImageViewer';

import NotificationsList from './src/components/notificationsContainer';
import HeaderLocationSearchBar from './src/controls/HeaderLocationSearchBar';
import AnimatedHeaderDefault from './src/controls/AnimatedHeaderDefault';

import StyleConstants, { cardDefaultHeaderStyle, cardDefaultStyle, navigationStylesheet } from './src/styles/constants'
import Strings from './src/assets/strings';
import { HEADER_HEIGHT, TOP_TAB_BAR_HEIGHT, SCREEN_WIDTH } from './src/common/constants';
import { headerAnim, setHeaderVisible, } from './src/common/headerAnimator';

const tabbarTranslate = headerAnim.interpolate({
    inputRange: [0, HEADER_HEIGHT],
    outputRange: [0, -HEADER_HEIGHT],
    extrapolate: 'clamp',
});

const PlatformDependentTabBarStyle = {
    android: {
        paddingTop: HEADER_HEIGHT,
        transform: [{
            translateY: tabbarTranslate
        }],
        position: "absolute",
        zIndex: 1, // Needed or else tapping the tab won't invoke anything.
        top: 0,
        left: 0,
        right: 0
    },
    ios: {
        borderTopWidth: 1,
        borderTopColor: "lightgrey"
    }
}

const PlatformDependentTabCreator = {
    ios: createBottomTabNavigator,
    android: createMaterialTopTabNavigator
}

const PlatformDependentTabStyle = {
    android: {
        paddingVertical: 0,
        borderBottomWidth: 1,
        borderBottomColor: "lightgrey",
    },
    ios: {
        borderBottomWidth: 0
    }
}

const PlatformDependentIconStyle = {
    android: {
        height: TOP_TAB_BAR_HEIGHT,
        width: TOP_TAB_BAR_HEIGHT,
    }
}

const TabNavigator = Platform.select(PlatformDependentTabCreator)(
    {
        Home: {
            screen: PostsList,
        },
        Notifications: {
            screen: NotificationsList,
        }
    },
    {
        swipeEnabled: true,
        animationEnabled: true,
        headerTintColor: "ghostwhite",
        tabBarOptions: {
            showIcon: true,
            showLabel: false,
            activeTintColor: StyleConstants.BRAND_COLOR_DARK,
            inactiveTintColor: StyleConstants.BRAND_COLOR_LIGHT_DISABLED,
            indicatorStyle: {
                backgroundColor: "lightblue"
            },
            tabStyle: Platform.select(PlatformDependentTabStyle),
            iconStyle: Platform.select(PlatformDependentIconStyle),
            style: {
                backgroundColor: StyleConstants.BACKGROUND_COLOR,
                elevation: 0,       //remove shadow on Android
                shadowOpacity: 0,   //remove shadow on iOS
                ...Platform.select(PlatformDependentTabBarStyle),
            },
        }
    }
)

TabNavigator.navigationOptions = ({ navigation }) => {
    const { routeName } = navigation.state.routes[navigation.state.index];

    switch (routeName) {
        case "Home":
            setHeaderVisible(true);
            return {
                header: (<HeaderLocationSearchBar />),
            }
        case "Notifications":
            setHeaderVisible(true);
            return {
                header: (<AnimatedHeaderDefault label={Strings.t('notifications.notifcationsScreenHeader')} />)
            }
        default:
            null;
    }
};

const HeaderStack = createStackNavigator(
    {
        HeaderScreen: {
            screen: TabNavigator,
        },
        PostView: {
            screen: PostView,
            navigationOptions: {
                ...cardDefaultHeaderStyle
            },
        },
        ProfileView: {
            screen: ProfileView,
            navigationOptions: {
                ...cardDefaultHeaderStyle
            },
        },
        ProfileEdit: {
            screen: ProfileEdit,
            navigationOptions: {
                title: Strings.t("editProfile"),
                ...cardDefaultHeaderStyle
            },
        },
        PostAdd: {
            screen: PostAdd,
            navigationOptions: {
                title: Strings.t("addPost.addPostTitle"),
                ...cardDefaultHeaderStyle
            },
        },
        ImageViewer: {
            screen: ImageViewer,
            navigationOptions: {
                title: Strings.t("photoViewerScreenTitle"),
                ...cardDefaultHeaderStyle
            }
        }
    },
    {
        cardStyle: cardDefaultStyle,
        navigationOptions: {
            headerStyle: {
                elevation: 0,       //remove shadow on Android
                shadowOpacity: 0,   //remove shadow on iOS
                borderBottomWidth: Platform.OS === "ios" ? 1 : 0,
                borderBottomColor: "lightgrey",
            },

        }

    }
);

const DrawerNavigator = createDrawerNavigator(
    {
        Home: HeaderStack
    },
    {
        contentComponent: Drawer,
        drawerWidth: SCREEN_WIDTH * 0.8, // Explicitly set the drawer width - it suddenly decided to change on me...
    }
)

const LoginNavigator = createStackNavigator(
    {
        LoginScreen: {
            screen: LoginScreen,
        },
    },
    {
        mode: "modal",
        headerMode: "none",
    });

export default RootNavigation = createSwitchNavigator(
    {
        Login: LoginNavigator,
        App: DrawerNavigator,
    },
    {
        initialRouteName: 'Login',
    }
)

StatusBar.setBarStyle(Platform.OS === "ios" ? 'dark-content' : 'light-content');
Platform.OS === 'android' && StatusBar.setBackgroundColor(StyleConstants.BRAND_COLOR_DARK);
