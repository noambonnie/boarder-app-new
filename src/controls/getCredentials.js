import {
    SafeAreaView,
    View,
    Text,
    TextInput,
    TouchableOpacity,
    Image,
    TouchableWithoutFeedback,
    StyleSheet,
    ScrollView,
    Platform,
    I18nManager,
} from 'react-native';
import React, { Component } from 'react';
import StyleConstants from '../styles/constants';
import PropTypes from 'prop-types';
import KeyboardSpacer from 'react-native-keyboard-spacer';

import Strings from '../assets/strings'

import { GoogleSigninButton } from 'react-native-google-signin';
import { LoginButton as FBLoginButton, } from 'react-native-fbsdk';

export default class GetCredentials extends Component {
    constructor(props) {
        super(props);
        this.state = {
            email: "",
            password: "",
            confirmPassword: "",
        }
    }

    done = () => {
        this.getCredentialsView.scrollTo({ x: 0, y: 0, animated: true });
        this.props.onSubmit(this.state.email, this.state.password, this.state.confirmPassword);
    }

    goToLogin = () => {
        this.props.goToLogin();
    }

    render() {
        return (
            <SafeAreaView style={{ flex: 1, backgroundColor: StyleConstants.BRAND_COLOR_LIGHT, width: "100%" }}>
                <ScrollView
                    ref={(component) => this.getCredentialsView = component}
                    keyboardShouldPersistTaps='always'
                    contentContainerStyle={{ alignItems: 'center' }}>
                    <Text style={{ color: StyleConstants.LIGHT_TEXT_COLOR, fontSize: 30, paddingTop: 20, paddingBottom: 8 }}>
                        {Strings.t("accountDetailsTitle")}
                    </Text>
                    <View style={{ width: "80%", paddingTop: 2 }}>
                        <Text style={{ color: 'yellow', fontSize: 14, paddingBottom: 10, }}>
                            {this.props.errorMessage}
                        </Text>
                        <TextInput style={Styles.input}
                            onChangeText={(text) => this.setState({ email: text })}
                            autoCapitalize="none"
                            autoCorrect={false}
                            keyboardType='email-address'
                            returnKeyType="next"
                            placeholder={Strings.t('emailField')}
                            placeholderTextColor='lightgrey'
                            underlineColorAndroid="transparent" />
                        <TextInput style={Styles.input}
                            onChangeText={(text) => this.setState({ password: text })}
                            returnKeyType="go"
                            placeholder={Strings.t('passwordField')}
                            placeholderTextColor='lightgrey'
                            underlineColorAndroid="transparent"
                            secureTextEntry />
                        <TextInput style={Styles.input}
                            onChangeText={(text) => this.setState({ confirmPassword: text })}
                            returnKeyType="go"
                            placeholder={Strings.t('confirmPasswordField')}
                            placeholderTextColor='lightgrey'
                            underlineColorAndroid="transparent"
                            secureTextEntry />

                    </View>
                    <TouchableOpacity style={Styles.buttonContainer}
                        onPress={this.done}>
                        <Text style={Styles.buttonText}>{Strings.t("nextButton")}</Text>
                    </TouchableOpacity>
                    <TouchableWithoutFeedback style={{ marginVertical: 10 }} onPress={this.props.onFacebookLogin}>
                        <View style={{ flexDirection: "row", justifyContent: "flex-start", alignItems: "center", backgroundColor: "#3B5998", width: 212, height: 40, borderRadius: 3, marginVertical: 10 }}>
                            <Image source={require("../assets/facebook-logo.png")} style={{ height: 20, width: 20, resizeMode: "stretch", marginLeft: 10 }} />
                            <View style={{ alignItems: "center", width: "80%" }}>
                                <Text style={{ color: "white", fontSize: 14, fontWeight: "bold" }}>Login with Facebook</Text>
                            </View>
                        </View>
                    </TouchableWithoutFeedback>
                    <GoogleSigninButton
                        style={{ width: 220, height: 48 }}
                        size={GoogleSigninButton.Size.Wide}
                        onPress={this.props.onGoogleLogin}
                    />
                    <TouchableOpacity style={{ marginTop: 80 }} onPress={this.props.goToLogin}>
                        <Text style={{ color: StyleConstants.LIGHT_TEXT_COLOR, fontSize: 16, paddingBottom: 10, textDecorationLine: 'underline' }}>{Strings.t("goToLogin")}</Text>
                    </TouchableOpacity>
                    {Platform.OS === 'ios' && (<KeyboardSpacer />)}
                </ScrollView>
            </SafeAreaView>
        )
    }
}

// define your styles
const Styles = StyleSheet.create({
    input: {
        height: 50,
        marginBottom: 10,
        paddingHorizontal: 10,
        fontSize: 18,
        color: 'black',
        borderRadius: 7,
        backgroundColor: StyleConstants.BACKGROUND_COLOR,
        textAlign: I18nManager.isRTL? "right" : "left",
    },
    buttonContainer: {
        backgroundColor: '#416485',
        paddingVertical: 15,
        marginBottom: 20,
        borderRadius: 7,
        width: "80%",
    },
    buttonText: {
        color: '#fff',
        textAlign: 'center',
        fontWeight: '700',
        fontSize: 18,
    },
    fieldName: {
        color: StyleConstants.LIGHT_TEXT_COLOR,
        fontSize: 14,
        marginBottom: 3
    }
});

GetCredentials.propTypes = {
    onSubmit: PropTypes.func.isRequired,
    goToLogin: PropTypes.func.isRequired,
    errorMessage: PropTypes.string.isRequired,
}
