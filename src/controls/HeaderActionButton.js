import React from "react";
import {
    TouchableOpacity,
    View,
    Text,
    Platform,
} from "react-native";

import { navigationStylesheet } from "../styles/constants"

export default class HeaderActionButton extends React.Component {
    render() {
        return (
            <TouchableOpacity disabled={this.props.disabled} onPress={this.props.onPress}>
                <View>
                    <Text style={[navigationStylesheet.headerActionButton, { opacity: this.props.disabled ? (Platform.OS === 'ios'? 0.6 : 0.4) : 1 }, this.props.style]}>{this.props.label}</Text>
                </View>
            </TouchableOpacity>
        )
    }
}