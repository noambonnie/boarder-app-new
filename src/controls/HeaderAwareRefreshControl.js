import React, { Component } from 'react';
import { RefreshControl } from 'react-native';

import { HEADER_HEIGHT, TOP_TAB_BAR_HEIGHT } from '../common/constants';

const REFRESH_CONTROL_OFFSET_ANDROID = HEADER_HEIGHT + TOP_TAB_BAR_HEIGHT;

export default class HeaderAwareRefreshControl extends Component {
    render() {
        return (
            <RefreshControl progressViewOffset={REFRESH_CONTROL_OFFSET_ANDROID}
                onRefresh={this.props.onRefresh}
                refreshing={this.props.isRefreshing}
                {...this.props} />
        )
    }
}

