import React, { Component } from 'react';
import {
    StyleSheet,
    Text,
    View,
    Image,
    TouchableOpacity,
    I18nManager,
} from 'react-native';

import StyleConstants from "../styles/constants";

export default class PostListItem extends Component {
    constructor(props) {
        super(props);
    }

    // NBTODO: Need to see what's better for performance - Component wiht shouldComponentUpdate or PureComponent.
    shouldComponentUpdate() {
        return false;
    }

    render() {
        return (
            <View>
                <View style={{ alignItems: 'flex-end', paddingRight: 20,}}>
                    <TouchableOpacity onPress={() => { this.props.showPostMenu({ id: this.props.postId, title: this.props.title, text: this.props.text, postedBy: { firstName: this.props.posterFirstName, lastName: this.props.posterLastName, id: this.props.posterId } }) }}>
                        <View style={{ width: 25, height: 25, justifyContent: 'center', alignItems: 'center', }}>
                            <Image source={require('../assets/icon-drop-down.png')} style={{ width: 15, height: 15, opacity: 0.3 }} />
                        </View>
                    </TouchableOpacity>
                </View>
                <TouchableOpacity activeOpacity={0.8} style={{ flex: 1, flexDirection: 'row', paddingBottom: 20, paddingHorizontal: 20 }}
                    onPress={() => this.props.showPostDetails(this.props.postId, this.props.title, this.props.text)}>
                    <View style={{ flex: 1, marginRight: 20, alignItems: 'center' }}>
                        <Image style={styles.image} source={this.props.image != null ? { uri: this.props.image } : require('../assets/icon-user.png')} defaultSource={require('../assets/icon-user.png')} />
                        <Text style={styles.name}>{this.props.posterFirstName}</Text>
                        <Text style={styles.count}>{this.props.postDistance}</Text>
                    </View>
                    <View style={{ flex: 4, justifyContent: 'space-between' }}>
                        <View>
                            <Text style={styles.title} ellipsizeMode='tail' numberOfLines={2}>
                                {this.props.title}
                            </Text>
                            <Text style={styles.text} ellipsizeMode='tail' numberOfLines={4} >
                                {this.props.text}
                            </Text>
                        </View>
                        <View style={{ marginTop: 5, flexDirection: 'row', justifyContent: 'space-between' }}>
                            <Text style={styles.count}>{this.props.comments}</Text>
                            <Text style={styles.timeSince}>{this.props.timeSincePost}</Text>
                        </View>
                    </View>
                </TouchableOpacity>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    title: {
        fontSize: 18,
        color: StyleConstants.BRAND_COLOR_DARK,
    },
    text: {
        color: 'grey',
    },
    image: {
        height: 64,
        width: 64,
        borderRadius: 32,
        marginBottom: 5,
    },
    icon: {
        height: 15,
        width: 15,
    },
    count: {
        fontSize: 10,
        color: 'grey',
    },
    timeSince: {
        fontSize: 10,
        color: 'grey',
        fontStyle: I18nManager.isRTL ? 'normal' : 'italic',
        paddingHorizontal: 10,
    },
    name: {
        fontSize: 10,
        color: 'grey',
        fontWeight: 'bold',
        textAlign: 'center',
    }

})
