import React, { Component } from 'react';
import {
    Text,
    View,
    Image,
    StyleSheet,
    Platform,
    I18nManager,
    TouchableWithoutFeedback,
} from 'react-native';

import { HEADER_HEIGHT, STATUS_BAR_HEIGHT, TOP_TAB_BAR_HEIGHT, SCREEN_WIDTH } from '../common/constants';
import StyleConstants from '../styles/constants';
import Strings from '../assets/strings';


export default class PostsCoachMark extends Component {
    constructor(props) {
        super(props);
    }

    render() {
        return (
            <View style={coachMarkStyleSheet.fullScreenContainer}>
                <View style={[coachMarkStyleSheet.overlayContainer, { opacity: 0.7, backgroundColor: StyleConstants.BRAND_COLOR_DARK, }]} />
                <View
                    style={[coachMarkStyleSheet.overlayContainer, { justifyContent: 'space-between', }]}>
                    <View style={{ alignItems: 'center', marginHorizontal: 20 }}>
                        <Image source={require('../assets/arrow-right.png')} style={[coachMarkStyleSheet.arrowImage, { marginTop: 5, transform: [{ rotateZ: "270deg" }] }]} />
                        <View style={coachMarkStyleSheet.textBoxContainer}>
                            <Text style={coachMarkStyleSheet.textBoxText}>{Strings.t("coachMarks.posts.searchBox")}</Text>
                        </View>
                    </View>
                    <TouchableWithoutFeedback onPress={this.props.dismissCoachMarks}>
                        <View style={{ backgroundColor: "darkmagenta", borderRadius: 25, opacity: 1, paddingHorizontal: 20 }}>
                            <Text style={{ color: StyleConstants.LIGHT_TEXT_COLOR, padding: 10, fontSize: 20, opacity: 1, fontWeight: 'bold' }}>{Strings.t("coachMarks.posts.dismiss")}</Text>
                        </View>
                    </TouchableWithoutFeedback>
                    <View style={{ width: SCREEN_WIDTH - 100, flexDirection: 'row', alignItems: "center", justifyContent: 'flex-start', marginBottom: 25, marginRight: 110, }}>
                        <View style={[coachMarkStyleSheet.textBoxContainer, { marginLeft: 20 }]}>
                            <Text style={coachMarkStyleSheet.textBoxText}>{Strings.t("coachMarks.posts.addPost")}</Text>
                        </View>
                        <Image source={require('../assets/arrow-right.png')} style={[coachMarkStyleSheet.arrowImage, { transform: I18nManager.isRTL ? [{ rotateZ: "180deg" }] : undefined }]} />
                    </View>
                </View>
            </View>
        );
    }
}

const coachMarkStyleSheet = StyleSheet.create({
    fullScreenContainer: {
        flex: 1,
        position: 'absolute',
        left: 0,
        top: 0,
        width: "100%",
        height: "100%",
    },
    overlayContainer: {
        flex: 1,
        position: 'absolute',
        left: 0,
        top: 0,
        width: "100%",
        height: "100%",
        paddingTop: Platform.OS === 'ios' ? HEADER_HEIGHT + STATUS_BAR_HEIGHT : HEADER_HEIGHT + TOP_TAB_BAR_HEIGHT,
        justifyContent: 'space-between',
        alignItems: 'center',
    },
    textBoxContainer: {
        backgroundColor: StyleConstants.BACKGROUND_COLOR,
        borderColor: "darkmagenta",
        borderRadius: 15,
        borderWidth: 5,
        opacity: 1
    },
    textBoxText: {
        color: 'dimgrey',
        padding: 10,
        fontSize: Platform.OS === 'ios' ? 16 : 18,
    },
    arrowImage: {
        width: 30, 
        height: 30, 
        tintColor: "darkmagenta",
    }
})