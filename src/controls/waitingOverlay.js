import React, { Component } from 'react';
import {
    Text,
    Animated,
} from 'react-native';

import StyleConstants from '../styles/constants';

export default class WaitingOverlay extends Component {
    constructor(props) {
        super(props);
        this.visibilityAnim = new Animated.Value(0);
    }

    componentDidMount() {
        setTimeout(() => {
            Animated.timing(this.visibilityAnim, {
                toValue: 0.85,
                duration: 200,
                useNativeDriver: true,
            }).start();
        }, 500)
    }
    render() {
        return (
            <Animated.View
                style={{
                    flex: 1,
                    position: 'absolute',
                    left: 0,
                    top: 0,
                    opacity: this.visibilityAnim,
                    backgroundColor: StyleConstants.BRAND_COLOR_DARK,
                    justifyContent: 'center',
                    alignItems: 'center',
                    width: "100%",
                    height: "100%",
                }}>
                <Text style={{ color: 'white', fontSize: 16, opacity: 1 }}>{this.props.text}</Text>
            </Animated.View>
        );
    }
}
