import React, { Component } from 'react';
import {
    StyleSheet,
    View,
    Image,
    TouchableWithoutFeedback,
} from 'react-native';

export default class CheckBox extends Component {
    constructor(props) {
        super(props);
        this.state = {
            checked: props.defaultValue || false
        }
    }

    render = () => {
        return (
            <View style={{...this.props.style}}>
                <TouchableWithoutFeedback onPress={() => {
                    this.setState({ checked: !this.state.checked }, () => this.props.onChange(this.state.checked));
                }}>
                    <Image source={this.state.checked ? require('../assets/icon-checkbox-checked.png') : require('../assets/icon-checkbox-unchecked.png')}
                        style={{ height: this.props.style.height, width: this.props.style.width }} />
                </TouchableWithoutFeedback>
            </View>
        )
    }
}
