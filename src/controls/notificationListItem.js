import React, { Component } from 'react';
import {
    StyleSheet,
    Text,
    View,
    Image,
    TouchableOpacity,
} from 'react-native';

import Strings from '../assets/strings';
import StyleConstants from "../styles/constants";

export default class NotificationListItem extends Component {
    constructor(props) {
        super(props);
    }

    // NBTODO: Need to see what's better for performance - Component wiht shouldComponentUpdate or PureComponent.
    shouldComponentUpdate() {
        return false;
    }

    render() {
        return (
            <TouchableOpacity activeOpacity={0.8} 
                            style={{ flex: 1, 
                                     paddingVertical: 20, 
                                     paddingHorizontal: 20, 
                                     backgroundColor: this.props.read? StyleConstants.BACKGROUND_COLOR : StyleConstants.UNREAD_NOTIFICATION_BG_COLOR }}
                onPress={() => this.props.goToNotification(this.props.data)}>
                <View style={{ flex: 1, }}>
                    {this.props.getNotificationText(this.props.data)}
                </View>
            </TouchableOpacity>
        )
    }
}

const styles = StyleSheet.create({
    title: {
        fontSize: 18,
    },
    text: {
        color: 'grey',
    },
    image: {
        height: 64,
        width: 64,
        borderRadius: 32,
        marginBottom: 5,
    },
    icon: {
        height: 15,
        width: 15,
    },
    count: {
        fontSize: 10,
        color: 'grey',
    }
})
