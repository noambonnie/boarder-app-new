import React, { Component } from "react";
import {
    View,
    Text,
    Image,
    StyleSheet,
    Platform
} from "react-native";

import StyleConstants from '../styles/constants';
import { TOP_TAB_BAR_HEIGHT } from '../common/constants';
import { connect } from 'react-redux';

class BadgedIcon extends Component {

    render = () => {
        return (
            <View style={badgeStyle.container}>
                <Image source={this.props.source} style={[badgeStyle.image, this.props.style, {opacity: this.props.focused? 1: 0.5}]} />
                {
                    this.props.showCount && this.props.notificationsCount > 0 &&
                    (<View style={badgeStyle.counter}>
                        <Text style={badgeStyle.text}>{this.props.notificationsCount > 9? "+9" : this.props.notificationsCount}</Text>
                     </View>)
                }
            </View>
        )
    }
}

const platformDependentBadgeSize = {
    android: 16,
    ios: 18,
}

const badgeStyle = StyleSheet.create({
    container: {
        padding: Platform.select(platformDependentBadgeSize) / 2,
        width: Platform.OS == "ios"? null : TOP_TAB_BAR_HEIGHT,
        height: Platform.OS == "ios"? null : TOP_TAB_BAR_HEIGHT,
    },
    image: {
        width: Platform.OS === "ios"? 30 : 25, 
        height: Platform.OS === "ios"? 30 : 25, 
        tintColor: StyleConstants.BRAND_COLOR_LIGHT
    },
    counter: { 
        justifyContent: 'center', 
        alignItems: 'center', 
        position: "absolute", 
        top: Platform.OS === "ios"? 8 : 4, 
        right: Platform.OS === "ios"? 6 : 10,
        backgroundColor: "firebrick", 
        width: Platform.select(platformDependentBadgeSize), 
        height: Platform.select(platformDependentBadgeSize), 
        borderRadius: Platform.select(platformDependentBadgeSize) / 2, 
    },
    text: { 
        fontSize: 12, 
        fontWeight: 'bold', 
        color: StyleConstants.LIGHT_TEXT_COLOR 
    }
})

const mapStateToProps = (state, ownProps) => {
    return {
        notificationsCount: state.notifications.count,
    }
}

const BadgedIconClass = connect(mapStateToProps, null)(BadgedIcon);

export default BadgedIconClass;