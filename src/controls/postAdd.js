import React, { Component } from 'react';
import WaitingOverlay from '../controls/waitingOverlay';

import {
    Text,
    View,
    ScrollView,
    Image,
    TextInput,
    TouchableWithoutFeedback,
    Platform,
    StyleSheet,
    Alert,
} from 'react-native';

import Strings from '../assets/strings';
import StyleConstants from '../styles/constants'
// NBTODO: Get rid of this library and implement what I need. I had to modify the lib to address my needs
//         for the widget. Those changes will get lost when I delete node_modules
import { GooglePlacesAutocomplete } from 'react-native-google-places-autocomplete';
import { HEADER_HEIGHT, SCREEN_WIDTH } from '../common/constants';
import KeyboardSpacer from 'react-native-keyboard-spacer';
import { getSearchLanguage, MIN_SEARCH_LENGTH } from '../common/googlePlacesSearch';

const IMAGE_SIZE = SCREEN_WIDTH / 3 - 24; // Don't know why 24 is the number. I calculated and didn't come up with that number. But it works on all screens nonetheless.

export default class PostAdd extends Component {
    constructor(props) {
        super(props);
        this.state = {
            locationFocused: false,
            location: {
                address: this.props.currentFilter ? this.props.currentFilter.byDistance.address : null,
                position: this.props.currentFilter ? this.props.currentFilter.byDistance.position : null,
            },
        }
    }

    renderCurrLocationButton = () => {
        return (
            <TouchableWithoutFeedback onPress={() => {
                this.setState({ location: {} });
                this.locationRef.setAddressText("");
                this.props.setPostField({ location: {} });
                this.props.setPostField({ locationValid: this.props.hasLocation });
            }}>
                <Image source={require('../assets/icon-current-location.png')} style={{ width: 20, height: 20, opacity: 0.4, marginLeft: 5, }} />
            </TouchableWithoutFeedback>
        )
    }

    render() {
        return (
            <View style={{ flex: 1, }} onLayout={() => { this.locationText.measure((x, y, width, height, pageX, pageY) => { !this.textYLocation && (this.textYLocation = pageY); }) }}>
                <ScrollView
                    ref={(component) => this.scrollView = component}
                    keyboardShouldPersistTaps="always">
                    <View style={addPostStylesheet.textFieldsContainer}>
                        <TextInput
                            autoFocus={false}
                            onChangeText={(title) => this.props.setPostField({ title })}
                            placeholder={Strings.t('addPost.newPostTitlePlaceholder')}
                            style={{ fontSize: 20, }}
                            underlineColorAndroid={StyleConstants.BRAND_COLOR_LIGHT_DISABLED} />
                        <View style={addPostStylesheet.postBodyTextInputContainer}>
                            <TextInput
                                onChangeText={(text) => this.props.setPostField({ text })}
                                placeholder={Strings.t('addPost.newPostTextPlaceholder')}
                                returnKeyType='default'
                                blurOnSubmit={false}
                                editable={true}
                                multiline={true} style={addPostStylesheet.postBodyTextInput}
                                underlineColorAndroid='transparent' />
                        </View>
                    </View>

                    <View style={{ marginHorizontal: 20, marginTop: 15, minHeight: 300, }}>

                        <Text style={{ color: 'grey' }} ref={(component) => { this.locationText = component; }}>{Strings.t("addPost.postMessageLocation")}</Text>
                        <View>
                            <View style={{ top: 60 }}>
                                <View style={{ alignItems: 'center', }}>
                                    <TouchableWithoutFeedback onPress={() => this.props.selectPhotos()}>
                                        <View style={addPostStylesheet.uploadButtonContainer}>
                                            <Image source={require("../assets/icon-upload-photo.png")} style={addPostStylesheet.uploadButtonImage} />
                                            <Text style={addPostStylesheet.uploadButtonText}>{Strings.t("addPost.addPhotos")}</Text>
                                        </View>
                                    </TouchableWithoutFeedback>
                                </View>
                                <View style={addPostStylesheet.uploadedImagesContainer}>
                                    {Object.keys(this.props.postImageUploaders).map((key) => {
                                        const imageState = this.props.postImageUploaders[key];
                                        const imageDetails = imageState.uploader.imageDetails;

                                        return (
                                            <View style={{ marginTop: 20, alignItems: "center", marginHorizontal: 5, }} key={key}>
                                                <Image source={{ uri: imageDetails.path }} resizeMode="cover" style={addPostStylesheet.uploadedImage} />
                                                <TouchableWithoutFeedback onPress={() => this.props.deleteUploadedImage(key)}>
                                                    <View style={addPostStylesheet.deleteImageButtonContainer}>
                                                        <Image source={require('../assets/icon-delete.png')} style={addPostStylesheet.deleteImageIcon}/>
                                                    </View>
                                                </TouchableWithoutFeedback>
                                                <View style={addPostStylesheet.imageUploadStatusContainer}>
                                                    {
                                                        imageState.failed ? (
                                                            <View style={[addPostStylesheet.imageUploadStatusBar, { backgroundColor: "firebrick",}]}>
                                                                <Text style={addPostStylesheet.imageUploadText}>{Strings.t("addPost.photoUploadFailed")}</Text>
                                                            </View>
                                                        ) : (
                                                                <View style={addPostStylesheet.imageUploadStatusBar}>
                                                                    <Text style={addPostStylesheet.imageUploadText}>{imageState.progress === 100? Strings.t("addPost.photoUploaded") : imageState.progress + '%'}</Text>
                                                                </View>
                                                            )
                                                    }
                                                </View>
                                            </View>
                                        )
                                    })}
                                </View>
                            </View>

                            <GooglePlacesAutocomplete
                                ref={(instance) => { this.locationRef = instance }}
                                placeholder={Strings.t("nearMe") + (!this.props.hasLocation ? ' ' + Strings.t("unavailable") : '')}
                                minLength={MIN_SEARCH_LENGTH} // minimum length of text to search
                                autoFocus={false}
                                returnKeyType={'search'} // Can be left out for default return key https://facebook.github.io/react-native/docs/textinput.html#returnkeytype
                                listViewDisplayed={'auto'}    // true/false/undefined
                                fetchDetails={true}
                                onPress={(data, details = null) => { // 'details' is provided when fetchDetails = true
                                    if (details.types.indexOf("country") >= 0 || details.types.indexOf("administrative_area_level_1") >= 0) {
                                        return Alert.alert(Strings.t("locationTooBroadTitle"), Strings.t("locationTooBroadText"));
                                    }

                                    details.geometry && details.geometry.location &&
                                        this.props.setPostField({
                                            location: {
                                                address: details.formatted_address,
                                                position: {
                                                    latitude: details.geometry.location.lat,
                                                    longitude: details.geometry.location.lng,
                                                }
                                            }
                                        });

                                    this.props.setPostField({ locationValid: true });
                                }}

                                getDefaultValue={() => { return this.state.location.address || '' }}

                                query={{
                                    // available options: https://developers.google.com/places/web-service/autocomplete
                                    key: 'AIzaSyD19kt7oAXO0jp8bI_OBjZcqiUG6qqkh8s',
                                    language: getSearchLanguage(), // language of the results
                                    // types: '(cities)' // default: 'geocode'
                                }}

                                styles={{
                                    listView: {
                                        borderWidth: 1,
                                        borderColor: 'lightgrey',
                                        backgroundColor: StyleConstants.BACKGROUND_COLOR,
                                        borderRadius: 5,
                                    },
                                    container: {
                                        width: '100%',
                                        position: "absolute",
                                        top: 0,
                                        flex: 1,
                                        backgroundColor: "lightgrey",
                                    },
                                    textInputContainer: {
                                        width: '100%',
                                        backgroundColor: StyleConstants.BACKGROUND_COLOR,
                                        borderRadius: 0,
                                        margin: 0,
                                        padding: 0,
                                        borderTopWidth: 0,
                                        borderBottomWidth: 0,
                                        alignItems: 'center',
                                    },
                                    textInput: {
                                        width: '100%',
                                        borderBottomWidth: !this.state.locationFocused && !this.props.locationValid ? 2 : 1,
                                        borderBottomColor: !this.state.locationFocused && !this.props.locationValid ? 'red' : StyleConstants.BRAND_COLOR_LIGHT_DISABLED,
                                        backgroundColor: StyleConstants.BACKGROUND_COLOR,
                                        borderRadius: 0,
                                        fontSize: 16,
                                        margin: 0,
                                        padding: 0,
                                        paddingTop: 0,
                                        paddingBottom: 0,
                                        paddingLeft: 0,
                                        paddingRight: 0,
                                        marginTop: 0,
                                        marginLeft: 0,
                                        marginRight: 0,
                                    },
                                    description: {
                                        fontSize: 14,
                                    },
                                    row: {
                                        height: 35,
                                        alignItems: 'center',
                                        paddingVertical: 5
                                    },
                                    separator: {
                                        backgroundColor: 'lightgrey',
                                        height: 1,
                                        marginHorizontal: 5,
                                        padding: 0,
                                    },
                                    poweredContainer: {
                                        borderRadius: 5,
                                        backgroundColor: StyleConstants.BACKGROUND_COLOR,
                                    },
                                }}

                                nearbyPlacesAPI='GooglePlacesSearch' // Which API to use: GoogleReverseGeocoding or GooglePlacesSearch
                                textInputProps={{
                                    onChangeText: (value) => {
                                        if (value.length === 0) {
                                            this.props.setPostField({ location: {} });
                                            this.props.setPostField({ locationValid: this.props.hasLocation });
                                        }
                                        else {
                                            this.props.setPostField({ locationValid: false });
                                        }
                                    },
                                    onFocus: () => {
                                        Platform.OS === "ios" ? this.scrollView.scrollToEnd() :
                                            this.scrollView.scrollTo({ x: 0, y: this.textYLocation - HEADER_HEIGHT - 5, animated: true });
                                        this.setState({ locationFocused: true });
                                    },
                                    onBlur: () => {
                                        this.setState({ locationFocused: false })
                                    }
                                }}
                                debounce={200} // debounce the requests in ms. Set to 0 to remove debounce. By default 0ms.
                                renderRightButton={this.renderCurrLocationButton}
                            />
                        </View>
                    </View>

                    {Platform.OS === 'ios' && (<KeyboardSpacer />)}
                </ScrollView>
                {this.props.isAddingPost && (<WaitingOverlay text={this.props.isUploadingImages? Strings.t("addPost.uploadingImages"): Strings.t("addPost.postingMessage")} />)}
            </View>

        );
    }
}

const addPostStylesheet = StyleSheet.create({
    textFieldsContainer: { 
        marginHorizontal: 20, 
        paddingTop: Platform.OS === "ios" ? 10 : 5, 
    },
    postBodyTextInputContainer: { 
        borderColor: StyleConstants.BRAND_COLOR_LIGHT_DISABLED, 
        marginTop: 10, 
        borderRadius: 5, 
        borderWidth: 1 
    },
    postBodyTextInput: { 
        fontSize: 16, 
        paddingTop: 5, 
        minHeight: 50, 
        paddingBottom: Platform.OS === 'ios' ? 5 : 2,
        paddingHorizontal: 5, 
        textAlignVertical: 'top' 
    },
    uploadButtonContainer: { 
        paddingVertical: 5, 
        paddingHorizontal: 10, 
        flexDirection: 'row', 
        alignItems: 'center', 
        justifyContent: 'center', 
        borderWidth: 2, 
        borderRadius: 20, 
        borderColor: StyleConstants.PHOTOS_UPLOAD_COLOR, 
    },
    uploadButtonImage: { 
        tintColor: StyleConstants.PHOTOS_UPLOAD_COLOR, 
        width: 30, 
        height: 30, 
    },
    uploadButtonText: { 
        fontWeight: "bold", 
        fontSize: 16, 
        color: StyleConstants.PHOTOS_UPLOAD_COLOR, 
        marginHorizontal: 5 
    },
    uploadedImagesContainer: { 
        paddingBottom: 100, 
        paddingTop: 10, 
        flexDirection: 'row', 
        flexWrap: 'wrap',
    },
    uploadedImage: { 
        width: IMAGE_SIZE, 
        height: IMAGE_SIZE, 
        borderRadius: 10, 
        borderWidth: 1, 
        borderColor: StyleConstants.BRAND_COLOR_LIGHT 
    },
    deleteImageButtonContainer: { 
        position: "absolute", 
        alignItems: "center", 
        justifyContent: "center", 
        top: -5, 
        right: -5, 
        width: 30, 
        height: 30, 
        borderRadius: 15, 
        borderWidth: 1, 
        borderColor: StyleConstants.BRAND_COLOR_LIGHT, 
        backgroundColor: StyleConstants.BRAND_COLOR_LIGHT_DISABLED,
    },
    deleteImageIcon: {
        tintColor: StyleConstants.BRAND_COLOR_DARK, 
        width: 15, 
        height: 15
    },
    imageUploadStatusContainer: { 
        position: "absolute", 
        bottom: 0, 
        left: 0, 
        width: "100%", 
    },
    imageUploadStatusBar: {
        paddingVertical: 1, 
        borderBottomLeftRadius: 10, 
        borderBottomRightRadius: 10, 
        minHeight: 20, 
        alignItems: "center",
        justifyContent: "center",
        opacity: Platform.OS === 'ios'? 0.85 : 0.8,
        backgroundColor: StyleConstants.BRAND_COLOR_LIGHT,  
    },
    imageUploadText: { 
        color: StyleConstants.LIGHT_TEXT_COLOR, 
        fontSize: 12, 
    }
})