import React, { Component } from 'react';
import PropTypes from 'prop-types';

import {
    StyleSheet,
    Text,
    View,
    Button,
    ScrollView,
    Image,
    TextInput,
    ActivityIndicator,
    TouchableWithoutFeedback,
    Platform,
    SafeAreaView,
    I18nManager,
} from 'react-native';

import Comment from './comment';
import Strings from '../assets/strings';
import StyleConstants from '../styles/constants'
import KeyboardSpacer from 'react-native-keyboard-spacer';
import ParsedText from 'react-native-parsed-text';
import { timeSince } from '../common/dateUtils';
import PostMenu from './postMenu';

import { SCREEN_WIDTH } from '../common/constants';
const THUMBNAIL_SIZE = Math.floor(SCREEN_WIDTH / 3 - 20); // Don't know why 20 is the number. I calculated and didn't come up with that number. But it works on all screens nonetheless.

export default class PostView extends Component {
    constructor(props) {
        super(props);
        // NBTODO: Maybe manage this in redux? Can be cleaner perhaps.
        this.state = {
            commentText: '',
            showPostMenu: false,
            postMenuDetails: '',
        };
    }

    showPostMenu = async (postDetails) => {
        this.setState({
            showPostMenu: true,
            postMenuDetails: postDetails,
        });
    }

    hidePostMenu = async () => {
        this.setState({
            showPostMenu: false,
        })
    }

    componentDidUpdate(prevProps) {
        // Clear comment field after successfully adding a comment and scroll to end.
        if (prevProps.isAddingComment && !this.props.isAddingComment && !this.props.addCommentError) {
            // NBTODOv2: There's a bug in react-native for ios; clear doesn't work. Currently setting text to '' field and setting the initial value of the component as workaround.
            //           See https://github.com/facebook/react-native/issues/18272
            this.textInputComponent.clear();
            this.setState({ commentText: '' });
            // NBTODO: This is a hack. For some reason on iOS it would scroll to the comment before last - something must
            //         update too fast or something and the scroll gets before the last comment is rendered or something.
            //         Doesn't make too much sense. But for now, adding a short delay before scrolling.
            setTimeout(() => { this.postScrollView && this.postScrollView.scrollToEnd({ animated: true }) }, 200);
        }
    }

    renderPostDetails = () => {
        return (
            <View>
                <View style={{ flexDirection: 'row', alignItems: 'center', marginTop: 10 }}>
                    <TouchableWithoutFeedback onPress={() => this.props.viewProfile(this.props.posterId)}>
                        <Image source={this.props.posterProfilePic}
                            style={{ width: 60, height: 60, borderRadius: 30, }} />
                    </TouchableWithoutFeedback>
                    <View style={{ flex: 1, paddingLeft: 20, justifyContent: 'flex-start' }}>
                        <Text style={{ paddingBottom: 5, fontSize: 24, color: StyleConstants.BRAND_COLOR_DARK }} numberOfLines={2} ellipsizeMode='tail'>{this.props.title}</Text>
                        <Text style={styles.name}>{this.props.posterFirstName} {this.props.posterLastName}
                            <Text style={{ fontSize: 14, color: 'grey' }}>, {this.props.timeSincePost}</Text>
                        </Text>
                    </View>

                </View>
                <View style={{ marginTop: 20, marginBottom: 10 }}>
                    {this.props.isLoadingPostDetails && !this.props.text && (
                        <View
                            style={{ paddingVertical: 20, }}>
                            <ActivityIndicator size="large" color={StyleConstants.BRAND_COLOR_DARK} />
                        </View>
                    )}
                    <ParsedText style={{ fontSize: 16, }}
                        parse={
                            [
                                { type: 'url', style: { color: StyleConstants.BRAND_COLOR_DARK }, onPress: this.props.handleUrlPress },
                                { type: 'phone', style: { color: StyleConstants.BRAND_COLOR_DARK }, onPress: this.props.handlePhonePress },
                                { type: 'email', style: { color: StyleConstants.BRAND_COLOR_DARK }, onPress: this.props.handleEmailPress },
                            ]
                        }>
                        {this.props.text}
                    </ParsedText>
                </View>
                {this.renderLinkPreview()}
                {this.renderImages()}
                <View style={{ height: 1, width: "100%", backgroundColor: "#DEE0DE", justifyContent: 'center', marginVertical: 10, }} />
            </View>
        )
    }

    renderImages = () => {
        if (this.props.images.length === 0) return;

        return (
            <View style={styles.previewImagesContainer}>
                {this.props.images.map((image, index) => {
                    return (
                        <TouchableWithoutFeedback onPress={() => this.props.viewImages(index)} key={image}>
                            <View>
                                <Image source={{ uri: `${this.props.imageResizerPrefixURL}/${THUMBNAIL_SIZE}x${THUMBNAIL_SIZE}/smart/${this.props.originalImagesPrefix}/${image}` }} resizeMode="cover" style={styles.image} />
                                {
                                    index === this.props.images.length - 1 && this.props.totalImagesNo > this.props.images.length && (
                                        <View style={[styles.image, { justifyContent: 'center', alignItems: 'center', position: "absolute", top: 0, left: 0, backgroundColor: StyleConstants.BRAND_COLOR_LIGHT, opacity: 0.8 }]}>
                                            <Text style={styles.moreImagesText}>{I18nManager.isRTL && Platform.OS !== 'ios' ?
                                                `${this.props.totalImagesNo - this.props.images.length + 1}+` :
                                                `+${this.props.totalImagesNo - this.props.images.length + 1}`
                                            }</Text>
                                        </View>
                                    )
                                }
                            </View>
                        </TouchableWithoutFeedback>
                    )
                })}
            </View>

        )
    }

    renderLinkPreview = () => {
        if (this.props.linkPreview == null || this.props.images.length > 0)
            return;

        const imageUrl = this.props.linkPreview.images[0] &&
            (this.props.linkPreview.images[0].substr(0, 7).toLowerCase() === 'http://' || this.props.linkPreview.images[0].substr(0, 8).toLowerCase() === 'https://') ?
            this.props.linkPreview.images[0] : null;

        return (
            <View style={{ borderColor: 'lightgrey', borderWidth: 1, }}>
                <TouchableWithoutFeedback onPress={() => { this.props.handleUrlPress(this.props.linkPreview.url) }}>
                    <View style={{ flexDirection: 'row', }}>
                        {imageUrl != null && <Image source={{ uri: imageUrl }} style={{ height: 80, width: 80 }} />}
                        <View style={{ flex: 1, paddingHorizontal: 10, paddingVertical: 5 }}>
                            <Text style={{ fontSize: 20 }} ellipsizeMode='tail' numberOfLines={1}>{this.props.linkPreview.title}</Text>
                            <Text ellipsizeMode='tail' numberOfLines={2}>{this.props.linkPreview.description}</Text>
                        </View>
                    </View>
                </TouchableWithoutFeedback>
            </View>
        )
    }

    renderFooter = () => {
        if (!this.props.isLoadingComments) return null;

        return (
            <View
                style={{ paddingVertical: 20, }}>
                <ActivityIndicator size="large" color={StyleConstants.BRAND_COLOR_DARK} />
            </View>
        );
    }

    render() {
        return (
            <SafeAreaView style={{ flex: 1, }}>
                <View style={{ flex: 1 }}>
                    <ScrollView style={{ flex: 1, }} ref={(component) => this.postScrollView = component}>
                        <View style={{ marginHorizontal: 20, }}>
                            {this.renderPostDetails()}
                            {this.props.loadCommentsError && (
                                <Text style={{ fontSize: 18, color: 'grey', justifyContent: 'center' }}>
                                    {Strings.t('loadCommentsFailed')}
                                </Text>
                            )}
                            {this.renderFooter()}
                            {this.props.comments && this.props.comments.map(comment => {
                                return (<Comment
                                    handleUrlPress={this.props.handleUrlPress}
                                    handleEmailPress={this.props.handleEmailPress}
                                    handlePhonePress={this.props.handlePhonePress}
                                    userId={comment.postedBy._id}
                                    viewProfile={this.props.viewProfile}
                                    key={comment._id}
                                    commentId={comment._id}
                                    firstName={comment.postedBy.firstName}
                                    lastName={comment.postedBy.lastName}
                                    text={comment.text}
                                    commentDate={timeSince(new Date(comment.postDate))}
                                    showPostMenu={this.showPostMenu}
                                    profilePic={comment.postedBy.profilePic ? { uri: comment.postedBy.profilePic } : require('../assets/icon-user.png')} />)
                            }
                            )}
                        </View>

                    </ScrollView>
                    <View style={styles.writeComment}>
                        {/* NBTODO: 
                                - returnKeyType doesn't seem to work on the emulator. 
                                - how do I get the same event from return as I do for button? 
                                - iOS seems to have a way to disable return when no text was entered. Use it. enablesReturnKeyAutomatically */}
                        <TextInput
                            ref={component => this.textInputComponent = component}
                            editable={!this.props.isAddingComment}
                            onChangeText={(commentText) => this.setState({ commentText })}
                            returnKeyType={'send'}
                            onSubmitEditing={() => this.props.addComment(this.state.commentText)}
                            value={this.state.commentText} // NBTODOv2 - Should be removed. See comment in componentWillReceiveProps
                            placeholder={Strings.t('addPost.writeCommentPlaceholder')}
                            multiline={true} style={{ flex: 1, fontSize: 16, paddingVertical: 0, marginVertical: 0, }}
                            underlineColorAndroid='transparent' />
                        {/* NBTODO: Icon instead of text. */}
                        <Button title={Strings.t('send')}
                            onPress={() => { this.props.addComment(this.state.commentText) }}
                            disabled={this.props.isAddingComment || !this.state.commentText} />
                    </View>
                    {Platform.OS === 'ios' && (<KeyboardSpacer />)}
                </View>
                {this.state.showPostMenu && (
                    <PostMenu postDetails={this.state.postMenuDetails} done={this.hidePostMenu} />
                )}
            </SafeAreaView>
        );
    }
}

const styles = StyleSheet.create({
    previewImagesContainer: {
        flexDirection: 'row',
        flexWrap: 'wrap',
        alignItems: "center",
        justifyContent: "flex-start",
    },
    image: {
        height: THUMBNAIL_SIZE,
        width: THUMBNAIL_SIZE,
        borderRadius: 10,
        borderWidth: 1,
        borderColor: StyleConstants.BRAND_COLOR_LIGHT,
        marginTop: 5,
        marginHorizontal: 3,
    },
    writeComment: {
        bottom: 0,
        flexDirection: 'row',
        alignItems: 'center',
        borderTopColor: 'lightgrey',
        borderTopWidth: 1,
        paddingHorizontal: 5,
        paddingVertical: 1,
    },
    name: {
        fontSize: 14,
        color: 'grey',
    },
    moreImagesText: {
        fontSize: 32,
        color: StyleConstants.LIGHT_TEXT_COLOR,
        opacity: 1,
    }
});

PostView.propTypes = {
    postId: PropTypes.string.isRequired,
    title: PropTypes.string,
    text: PropTypes.string,
    posterFirstName: PropTypes.string,
    posterLastName: PropTypes.string,
    posterId: PropTypes.string,
    comments: PropTypes.arrayOf(PropTypes.object),
    addComment: PropTypes.func,
    isAddingComment: PropTypes.bool.isRequired,
    addCommentError: PropTypes.bool.isRequired,
    loadCommentsError: PropTypes.bool,
}