import React, { Component } from 'react';
import PropTypes from 'prop-types';

import {
    StyleSheet,
    Text,
    View,
    ScrollView,
    Image,
    TouchableWithoutFeedback,
} from 'react-native';

import Strings from '../assets/strings';
import StyleConstants from '../styles/constants'

export default class UserProfile extends Component {
    constructor(props) {
        super(props);
    }

    componentWillReceiveProps(nextProps) {
    }

    componentDidUpdate(prevProps) {
    }

    renderFooter = () => {
        if (!this.props.isLoadingComments) return null;

        return (
            <View
                style={{ paddingVertical: 20, }}>
                <ActivityIndicator size="large" color={StyleConstants.BRAND_COLOR_DARK} />
            </View>
        );
    }

    render() {
        return (
            <View style={{ flex: 1, justifyContent: 'flex-end' }}>
                <ScrollView ref={(component) => this.postScrollView = component}>
                    <View style={{ marginTop: 20, marginHorizontal: 20, alignItems: 'center' }}>
                        <Image
                            source={this.props.profilePic}
                            style={{ width: 120, height: 120, borderRadius: 60, marginBottom: 20 }} />
                        <Text style={{ flex: 1, fontSize: 24, }}>
                            <Text>{this.props.firstName} </Text>
                            <Text>{this.props.lastName}</Text>
                        </Text>
                        {this.props.editable && (
                            <View style={{ marginTop: 10 }}>
                                <TouchableWithoutFeedback onPress={this.props.editProfile}>
                                    <View>
                                        <Text style={{
                                            borderWidth: 2,
                                            borderColor: StyleConstants.BRAND_COLOR_DARK,
                                            borderRadius: 15,
                                            fontSize: 16,
                                            paddingTop: 5,
                                            paddingBottom: 3,
                                            textAlign: 'center',
                                            color: StyleConstants.BRAND_COLOR_DARK,
                                            paddingHorizontal: 10
                                        }}>
                                            {Strings.t("editProfile")}
                                        </Text>
                                    </View>
                                </TouchableWithoutFeedback>
                            </View>
                        )}
                    </View>
                </ScrollView>
            </View>

        );
    }
}

UserProfile.propTypes = {
    // profilePic: PropTypes.object, // NBTODO: Commented out because - sometimes I pass a result of require() for default image.
    // and while that works, the type is a number for some reason, not an object.
    firstName: PropTypes.string,
    lastName: PropTypes.string,
}