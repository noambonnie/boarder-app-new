import React from "react";
import {
    View,
    Text,
    TextInput,
    TouchableWithoutFeedback,
    Animated,
    Platform,
    Image,
    StyleSheet,
} from "react-native";

import StyleConstants from "../styles/constants";
import Strings from "../assets/strings";
import DrawerButton from './DrawerButton';
import HeaderActionButton from './HeaderActionButton';
import * as Actions from '../state/actionsPosts';
import { connect } from 'react-redux';
import { AnimatedHeader } from '../common/headerAnimator';

class HeaderLocationSearchBar extends React.Component {

    constructor(props) {
        super(props);
        this.searchBarAnim = new Animated.Value(0);
        this.reverseSearchBarAnim = this.searchBarAnim.interpolate({
            inputRange: [0, 1],
            outputRange: [1, 0],
            extrapolate: 'clamp',
        });
    }

    componentWillReceiveProps(nextProps) {
        if (nextProps.isSearchFocused !== this.props.isSearchFocused) {
            Animated.timing(this.searchBarAnim, {
                toValue: nextProps.isSearchFocused ? 1 : 0,
                duration: 250,
                useNativeDriver: true,
            }).start()
        }
    }

    renderBlurredHeader = () => {
        return (
            <View style={[
                searchBarStyleSheet.animatedSearchView,
                { position: "absolute", top: 0 } // Render it over the focused view and animate opacity when focus is changed
            ]}
                key={this.props.isSearchFocused.toString()} // The key seems to determine which component is rendered on top. 
                >
                <DrawerButton />
                <TouchableWithoutFeedback onPress={() => !this.props.showPostsCoachMarks && this.props.setSearchFocused(true)}>
                    <Animated.View style={{ flex: 1, alignItems: "center", marginRight: 10, opacity: this.reverseSearchBarAnim}}>
                        <View style={searchBarStyleSheet.blurredTextInputView}>
                            <Text numberOfLines={1} style={searchBarStyleSheet.blurredTextInput}>
                                {this.props.filter.byDistance.address || Strings.t("nearMe")}
                            </Text>
                            <Image source={require('../assets/icon-search.png')} style={searchBarStyleSheet.blurredSearchIcon} />
                        </View>
                    </Animated.View>
                </TouchableWithoutFeedback>
            </View>
        )
    }

    renderFocusedHeader = () => {
        return (
            <Animated.View style={[
                searchBarStyleSheet.animatedSearchView,
                { opacity: this.searchBarAnim, },
            ]}
            key={(!this.props.isSearchFocused).toString()}
            >
                <DrawerButton />
                <View style={searchBarStyleSheet.focusedTextInputView}>
                    <TextInput
                        onChangeText={(text) => this.props.locationQuery(text)}
                        ref={(obj) => { this.searchInput = obj }}
                        allowFontScaling={false}
                        style={searchBarStyleSheet.focusedTextInput}
                        underlineColorAndroid="transparent"
                        autoFocus={this.props.isSearchFocused}
                        placeholder={Strings.t("whereTo")}
                    />
                </View>
                <HeaderActionButton label={Strings.t("done")} onPress={this.props.applyFilter} />
            </Animated.View>
        )
    }

    render() {
        const orderedHeaderComponents = this.props.isSearchFocused ?
            [this.renderBlurredHeader(), this.renderFocusedHeader()] :
            [this.renderFocusedHeader(), this.renderBlurredHeader()];

        return (
            <AnimatedHeader>
                <View style={{ flex: 1, flexDirection: "row", alignItems: "center", justifyContent: "center", backgroundColor: StyleConstants.BACKGROUND_COLOR }}>
                    <View style={{ flex: 1, flexDirection: "row", alignItems: "center", justifyContent: "center", }}>
                    {orderedHeaderComponents}
                    </View>
                </View>
            </AnimatedHeader>
        )
    }
}

const mapStateToProps = (state, ownProps) => {
    return {
        isSearchFocused: state.posts.locationSearch.isSearchFocused,
        filter: state.posts.filter,
        showPostsCoachMarks: state.app.showPostsCoachMarks,
    }
}

const mapDispatchToProps = (dispatch, ownProps) => {
    return {
        setSearchFocused: (isFocused) => {
            return dispatch(Actions.setSearchFocused(isFocused));
        },
        applyFilter: () => {
            return dispatch(Actions.applyFilter());
        },
        locationQuery: (query) => {
            return dispatch(Actions.locationQuery(query));
        }
    }
}

const HeaderLocationSearchBarClass = connect(mapStateToProps, mapDispatchToProps)(HeaderLocationSearchBar);

export default HeaderLocationSearchBarClass;

// NBTODO: This is an ugly implementation. The out-of-focus textInput field is just a look-alike text field. 
//         This took a lot of acrobatics to make it look the same as the textInput. Also, platforms behave
//         differently and I had to fine-tune certain style properties so the animation looks perfect.
//         A proper solution should probably use the same object with different sizes for enabled/disabled
const searchBarStyleSheet = StyleSheet.create(
    {
        animatedSearchView: {
            left: 0,
            right: 0,
            flex: 1,
            flexDirection: "row",
            alignItems: "center",
            justifyContent: "center",
        },
        blurredTextInputView: {
            flexDirection: "row",
            alignItems: "center",
            width: "100%",
            height: 29,
            borderRadius: 14,
            borderWidth: 1,
            borderColor: StyleConstants.BRAND_COLOR_LIGHT,
            paddingHorizontal: 12,
            paddingBottom: Platform.OS === "ios" ? 0 : 2,
        },
        blurredTextInput: {
            color: "grey",
            fontSize: 16,
        },
        blurredSearchIcon: {
            position: "absolute",
            right: 9,
            width: 16,
            height: 16,
            opacity: 0.5,
        },
        focusedTextInputView: {
            flex: 1,
            alignItems: "center",
            flexDirection: "row",
            alignItems: "center",
            height: 29,
            borderRadius: 14,
            paddingHorizontal: Platform.OS === "ios" ? 12 : 8,
            borderColor: StyleConstants.BRAND_COLOR_LIGHT,
            borderWidth: 1,
        },
        focusedTextInput: {
            paddingTop: Platform.OS === "ios" ? 0.5 : 9,
            height: Platform.OS === "ios" ? 28 : 42,
            fontSize: 16,
            width: "100%",

        }
    }
)