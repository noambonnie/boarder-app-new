import React from 'react';
import { Animated, Platform, } from 'react-native';
import { HEADER_HEIGHT, STATUS_BAR_HEIGHT, TOP_TAB_BAR_HEIGHT } from '../common/constants'
import { setHeaderVisible } from '../common/headerAnimator';

export default class HeaderAwareScrollView extends React.Component {
    constructor(props) {
        super(props);
        this.ScrollClass = Animated.createAnimatedComponent(this.props.scrollClass);

        // NBTODO: I probably want to leave the paddingTop at the developer's discretion - in case devs want some constant component below the tab and above
        //         the scroll.
        this.headerPadding = Platform.OS === "ios" ? HEADER_HEIGHT + STATUS_BAR_HEIGHT : HEADER_HEIGHT + TOP_TAB_BAR_HEIGHT;

        // Animate the padding according to header visibility. This is so pull-to-refresh shows
        // correctly and not under the header. There's probably a better way to solve it. But
        // I couldn't figure it out...

        // NBTODO - Pull to refresh. Code below works on iOS but not Android...
        // this.viewPaddingAnim = props.screenProps.headerAnim.interpolate({
        //     inputRange: [0, props.screenProps.headerHeight],
        //     outputRange: [this.headerPadding, 0],
        //     extrapolate: 'clamp',
        // });

        // this.scrollPaddingAnim = props.screenProps.headerAnim.interpolate({
        //     inputRange: [0, props.screenProps.headerHeight],
        //     outputRange: [0, this.headerPadding], 
        //     extrapolate: 'clamp',
        // });
    }

    onScrollBeginDrag = (event) => {
        // Store the initial scroll position. We'll use it in onScroll.
        this.scrollStartPosition = event.nativeEvent.contentOffset.y;

        // Call the component's functinality
        this.props.onScrollBeginDrag && this.props.onScrollBeginDrag(event);
    }

    onScroll = (event) => {
        // Compare the initial scroll position to the current. If we went down, hide the header. Otherwise show it. on iOS offset can negative 
        // (pulling beyond the beginning of the scroll). In that case we show the header. 
        setHeaderVisible(event.nativeEvent.contentOffset.y <= 0 || event.nativeEvent.contentOffset.y < this.scrollStartPosition);

        // Call the component's functinality
        this.props.onScroll && this.props.onScroll(event);
    }

    render() {
        return (
            // NBTODO - Pull to refresh. Code below works on iOS but not Android...
            // <Animated.View style={{ paddingTop: this.viewPaddingAnim, }}>
                <this.ScrollClass {...this.props}
                    contentContainerStyle={{
                        paddingTop: this.headerPadding, // NBTODO for pull to refreesh on iOS
                        ...this.props.contentContainerStyle
                    }}
                    // NBTODO - Pull to refresh. Code below works on iOS but not Android...
                    // style={{ paddingTop: this.scrollPaddingAnim, }}
                    onScroll={this.onScroll}
                    onScrollBeginDrag={this.onScrollBeginDrag}
                    scrollEventThrottle={16}
                    showsVerticalScrollIndicator={false}>
                    {this.props.children}
                </this.ScrollClass>
            // NBTODO - Pull to refresh. Code below works on iOS but not Android...
            // </Animated.View>
        );
    }
}