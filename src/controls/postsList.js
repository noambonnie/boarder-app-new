import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { timeSince } from '../common/dateUtils';

import {
    View,
    FlatList,
    ActivityIndicator,
} from 'react-native';

import PostListItem from './postListItem';
import Strings from '../assets/strings';
import HeaderAwareScrollView from './HeaderAwareScrollView';
import HeaderAwareRefreshControl from './HeaderAwareRefreshControl';
import StyleConstants from '../styles/constants';

export default class PostsList extends Component {

    renderFooter = () => {
        if (!this.props.isLoading) return null;

        return (
            <View
                style={{
                    paddingVertical: 20,
                    borderTopWidth: 1,
                    borderColor: "#DEE0DE"
                }}>
                <ActivityIndicator size="large" color={StyleConstants.BRAND_COLOR_DARK} />
            </View>
        );
    }

    // Separating lines between posts...
    renderSeparator() {
        return (
            <View
                style={{
                    height: 1,
                    width: "100%",
                    backgroundColor: "#DEE0DE",
                    justifyContent: 'center',
                }}
            />
        );
    }

    render() {
        // NBTODO: This component should be pure. All logic of filling out the fields should be in the container.
        // NBTODO: Currenly passing specific fields to a component. I should really pass the entire item object
        //         and I should do this all over the code when passing properties, not just here.
        return (
                <HeaderAwareScrollView scrollClass={FlatList}
                    ItemSeparatorComponent={this.renderSeparator}
                    data={this.props.postsList}
                    keyExtractor={(item, index) => item._id}
                    refreshControl={<HeaderAwareRefreshControl 
                        onRefresh={this.props.onRefresh}
                        refreshing={this.props.isRefreshing}/>}
                    onEndReached={this.props.onEndReached}
                    ListFooterComponent={this.renderFooter}
                    renderItem={({ item }) =>
                        <PostListItem style={{ padding: 20 }}
                            postId={item._id}
                            title={item.title}
                            text={item.text}
                            posterId={item.postedBy ? item.postedBy._id : null}
                            posterFirstName={item.postedBy ? item.postedBy.firstName : Strings.t("anonymous")}
                            posterLastName={item.postedBy ? item.postedBy.lastName : null}
                            image={item.postedBy ? item.postedBy.profilePic : null}
                            comments={Strings.t("comments.counting", { count: item.commentsCount })}
                            timeSincePost={timeSince(new Date(item.postDate).getTime())}
                            postDistance={Strings.t("distance", { distance: item.distance, unit: Strings.t("km") })}
                            showPostDetails={this.props.showPostDetails}
                            showPostMenu={this.props.showPostMenu} />
                    }
                />
        );
    }
}

PostsList.propTypes = {
    onRefresh: PropTypes.func.isRequired,
    onEndReached: PropTypes.func.isRequired,
    isRefreshing: PropTypes.bool.isRequired,
    isLoading: PropTypes.bool.isRequired,
}
