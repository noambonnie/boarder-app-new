import React, { Component } from 'react';
import PropTypes from 'prop-types';

import {
    StyleSheet,
    Text,
    View,
    ScrollView,
    Image,
    TouchableWithoutFeedback,
    TextInput,
    Platform,
} from 'react-native';

import Strings from '../assets/strings';
import StyleConstants from '../styles/constants'

export default class EditProfile extends Component {
    constructor(props) {
        super(props);
    }

    // Set the state to the default value of the fields.
    componentDidMount() {
        this.props.setInputText('firstName', this.props.firstName);
        this.props.setInputText('lastName', this.props.lastName);
    }

    render() {
        return (
            <View style={{ flex: 1, }}>
                    <View style={{ marginTop: 20, marginHorizontal: 20, alignItems: 'center' }}>
                        <TouchableWithoutFeedback onPress={this.props.getProfilePic}>
                            <Image
                                source={this.props.profilePic}
                                style={{ width: 120, height: 120, borderRadius: 60, marginBottom: 20 }} />
                        </TouchableWithoutFeedback>
                    </View>
                    <View style={{ flex: 1, alignItems: 'center' }}>
                        <View style={{ width: "90%" }}>
                            <View style={{
                                borderBottomColor: 'grey',
                                borderBottomWidth: Platform.select({ ios: 1, android: 0 }),
                                marginBottom: Platform.select({ ios: 10, android: 0 })
                            }}>
                                <TextInput style={{ textAlignVertical: 'bottom', fontSize: 18, }}
                                    defaultValue={this.props.firstName}
                                    placeholder={Strings.t("firstNameField")}
                                    underlineColorAndroid='grey'
                                    onChangeText={(text) => this.props.setInputText('firstName', text)} />
                            </View>
                            <View style={{
                                borderBottomColor: 'grey',
                                borderBottomWidth: Platform.select({ ios: 1, android: 0 }),
                                marginBottom: Platform.select({ ios: 10, android: 0 })
                            }}>
                                <TextInput style={{ textAlignVertical: 'bottom', fontSize: 18, }}
                                    defaultValue={this.props.lastName}
                                    placeholder={Strings.t("lastNameField")}
                                    underlineColorAndroid='grey'
                                    onChangeText={(text) => this.props.setInputText('lastName', text)} />
                            </View>
                        </View>
                    </View>

            </View>

        );
    }
}

EditProfile.propTypes = {
    // profilePic: PropTypes.object, // NBTODO: Commented out because - sometimes I pass a result of require() for default image.
    // and while that works, the type is a number for some reason, not an object.
    firstName: PropTypes.string,
    lastName: PropTypes.string,
}