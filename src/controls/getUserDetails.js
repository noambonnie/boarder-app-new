import {
    SafeAreaView,
    View,
    Text,
    TextInput,
    TouchableOpacity,
    Image,
    StyleSheet,
    ScrollView,
    Button,
    TouchableWithoutFeedback,
    Platform,
} from 'react-native';
import React, { Component } from 'react';
import StyleConstants from '../styles/constants';
import PropTypes from 'prop-types';
import KeyboardSpacer from 'react-native-keyboard-spacer';
import CheckBox from './checkbox'

import Strings from '../assets/strings'

export default class GetUserDetails extends Component {
    constructor(props) {
        super(props);
        this.state = {
            firstName: "",
            lastName: "",
            termsAccepted: true,
        }
    }

    done = () => {
        this.getUserDetailsView.scrollTo({ x: 0, y: 0, animated: true });
        this.props.onSubmit(this.state.firstName, this.state.lastName, this.state.termsAccepted);
    }

    // NBTODO: On small screen the error message is scrolled away and not visible. Rethink design or scroll back to top.
    render() {
        return (
            <SafeAreaView style={{ flex: 1, backgroundColor: StyleConstants.BRAND_COLOR_LIGHT, width: "100%", }}>
                <ScrollView
                    keyboardShouldPersistTaps='always'
                    ref={(component) => this.getUserDetailsView = component}
                    contentContainerStyle={{ alignItems: 'center' }}>
                    <Text style={{ color: StyleConstants.LIGHT_TEXT_COLOR, fontSize: 24, paddingTop: 20}}>
                        {Strings.t("userDetailsTitle")}
                    </Text>
                    <Text style={{ color: 'yellow', fontSize: 14, paddingBottom: 10, }}>
                        {this.props.errorMessage}
                    </Text>
                    <View style={{ width: "80%", }}>
                        <Text style={Styles.fieldName}>{Strings.t('profilePictureField')}</Text>
                        <View style={{ flexDirection: 'row', marginTop: 10, marginBottom: 20 }}>
                            <TouchableWithoutFeedback onPress={this.props.getProfilePic}>
                                <Image source={this.props.profilePic} style={{ width: 100, height: 100, borderRadius: 50, marginRight: 20 }} />
                            </TouchableWithoutFeedback>
                        </View>
                        <TextInput style={Styles.input}
                            onChangeText={(text) => this.setState({ firstName: text })}
                            autoCapitalize="words"
                            autoCorrect={false}
                            returnKeyType="next"
                            placeholder={Strings.t('firstNameField')}
                            placeholderTextColor='lightgrey'
                            underlineColorAndroid="transparent" />
                        <TextInput style={Styles.input}
                            onChangeText={(text) => this.setState({ lastName: text })}
                            onFocus={() => { this.getUserDetailsView.scrollToEnd({ animated: true }); }}
                            autoCapitalize="words"
                            autoCorrect={false}
                            returnKeyType="next"
                            placeholder={Strings.t('lastNameField')}
                            placeholderTextColor='lightgrey'
                            underlineColorAndroid="transparent" />
                    </View>
                    <View style={{ flexDirection: 'row', marginBottom: 5 }}>
                        <CheckBox style={{ width: 20, height: 20, marginRight: 5 }}
                            defaultValue={this.state.termsAccepted}
                            onChange={(checked) => { this.setState({ termsAccepted: checked }) }}
                        />
                        <View style={{ flexDirection: 'row' }}>
                            <Text style={{ color: StyleConstants.LIGHT_TEXT_COLOR, }}>{Strings.t("iAccept")}</Text>
                            <TouchableWithoutFeedback onPress={this.props.openTermsAndConditions}>
                                <View>
                                    <Text style={{ color: StyleConstants.LIGHT_TEXT_COLOR, textDecorationLine: 'underline' }}>
                                        {Strings.t("termsAndConditions")}
                                    </Text>
                                </View>
                            </TouchableWithoutFeedback>
                        </View>
                    </View>
                    <TouchableOpacity style={Styles.buttonContainer}
                        onPress={this.done}>
                        <Text style={Styles.buttonText}>{Strings.t("doneButton")}</Text>
                    </TouchableOpacity>
                    {Platform.OS === 'ios' && (<KeyboardSpacer />)}
                </ScrollView>
            </SafeAreaView>
        )
    }
}

// define your styles
const Styles = StyleSheet.create({
    input: {
        height: 50,
        marginBottom: 10,
        paddingHorizontal: 10,
        fontSize: 18,
        color: 'black',
        borderRadius: 7,
        backgroundColor: StyleConstants.BACKGROUND_COLOR,
    },
    buttonContainer: {
        backgroundColor: '#416485',
        paddingVertical: 20,
        marginVertical: 10,
        borderRadius: 7,
        width: "80%",
    },
    buttonText: {
        color: '#fff',
        textAlign: 'center',
        fontWeight: '700',
        fontSize: 18,
    },
    fieldName: {
        color: StyleConstants.LIGHT_TEXT_COLOR,
        fontSize: 14,
        marginBottom: 3
    }
});

GetUserDetails.propTypes = {
    onSubmit: PropTypes.func.isRequired,
    errorMessage: PropTypes.string.isRequired,
}
