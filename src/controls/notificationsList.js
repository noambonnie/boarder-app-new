import React, { Component } from 'react';
import PropTypes from 'prop-types';

import {
    View,
    FlatList,
    ActivityIndicator,
} from 'react-native';

import NotificationListItem from '../controls/notificationListItem';
import HeaderAwareScrollView from '../controls/HeaderAwareScrollView';
import HeaderAwareRefreshControl from '../controls/HeaderAwareRefreshControl';
import StyleConstants from '../styles/constants';

export default class NotificationsList extends Component {
    constructor(props) {
        super(props);
    }

    renderFooter = () => {
        if (!this.props.isLoading) return null;

        return (
            <View
                style={{
                    paddingVertical: 20,
                    borderTopWidth: 1,
                    borderColor: "#DEE0DE"
                }}>
                <ActivityIndicator size="large" color={StyleConstants.BRAND_COLOR_DARK} />
            </View>
        );
    }

    // Separating lines between notifications...
    renderSeparator() {
        return (
            <View
                style={{
                    height: 1,
                    width: "100%",
                    backgroundColor: "#DEE0DE",
                    justifyContent: 'center',
                }}
            />
        );
    }

    render() {
        // NBTODO: This component should be pure. All logic of filling out the fields should be in the container.
        return (
            <HeaderAwareScrollView scrollClass={FlatList}
                ItemSeparatorComponent={this.renderSeparator}
                data={this.props.notificationsList}
                keyExtractor={(item, index) => item._id}
                refreshControl={<HeaderAwareRefreshControl
                    onRefresh={this.props.onRefresh}
                    refreshing={this.props.isRefreshing} />}
                onEndReached={this.props.onEndReached}
                ListFooterComponent={this.renderFooter}
                renderItem={({ item }) =>
                    <NotificationListItem style={{ padding: 20 }}
                        notificationId={item._id}
                        type={item.type}
                        read={item.read}
                        data={item}
                        goToNotification={this.props.goToNotification}
                        getNotificationText={this.props.getNotificationText} />
                }
            />
        );
    }
}

NotificationsList.propTypes = {
    onRefresh: PropTypes.func.isRequired,
    onEndReached: PropTypes.func.isRequired,
    isRefreshing: PropTypes.bool.isRequired,
    isLoading: PropTypes.bool.isRequired,
}
