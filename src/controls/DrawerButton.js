import React, { Component } from "react";
import { TouchableWithoutFeedback, Image, } from 'react-native';
import NavigationService from '../common/navigationService';
import { DrawerActions } from 'react-navigation';
import { connect } from 'react-redux';
import StyleConstants from '../styles/constants'

class DrawerButton extends Component {

    render() {
        return (
            <TouchableWithoutFeedback onPress={() => NavigationService.dispatch(DrawerActions.openDrawer())}>
                <Image source={this.props.profilePic} style={{ width: 30, height: 30, marginHorizontal: 10, borderRadius: 15, tintColor: this.props.hasProfilePic? null : StyleConstants.BRAND_COLOR_LIGHT}} />
            </TouchableWithoutFeedback>
        )
    }
}

const mapStateToProps = (state, ownProps) => {
    return {
        hasProfilePic: state.user.info.profilePic != null,
		profilePic: state.user.info.profilePic ? { uri: state.user.info.profilePic } : require('../assets/icon-user.png'),
    }
}

const DrawerButtonClass = connect(mapStateToProps)(DrawerButton);

export default DrawerButtonClass;