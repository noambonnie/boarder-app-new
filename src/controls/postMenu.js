import React, { Component } from 'react';
import PropTypes from 'prop-types';
import StyleConstants from '../styles/constants';
import { connect } from 'react-redux';
import {
    AUTHORIZATION_SERVER_ADDRESS,
    AUTHORIZATION_SERVER_API_VERSION,
} from '../common/constants';
import { reportError } from '../common/errorReporter';
import { secureFetch } from '../common/authApi';
import Strings from '../assets/strings';

import {
    Text,
    View,
    TouchableWithoutFeedback,
    StyleSheet,
    Linking,
    Alert,
} from 'react-native';

class PostMenuClass extends Component {
    constructor(props) {
        super(props);
        this.state = {
            isReporting: false,

        }
    }

    reportItem = async () => {
        if (this.state.isReporting || this.props.isSelf)
            return;

        const body = `Reason for reporting: <Please explain...>\n\n` +
            `Posted by: ${this.props.postDetails.postedBy.firstName} ${this.props.postDetails.postedBy.lastName} (${this.props.postDetails.postedBy.id})\n` +
            `Title: ${this.props.postDetails.title}\n` +
            `Text: ${this.props.postDetails.text}\n` +
            `ID: ${this.props.postDetails.id}\n` +
            `Reported by: ${this.props.userId}\n`;

        this.setState({ isReporting: true },
            async () => {
                await Linking.openURL(`mailto:report@boarder.io?subject=Report item&body=${encodeURIComponent(body)}`);
                this.setState({ isReporting: false })
                this.props.done();
            })
    }

    blockUser = async () => {
        if (this.state.isBlockingUser || this.props.isSelf)
            return;

        // First dispatch: the app state is updated to inform
        // that the API call is starting.   

        this.setState({ isBlockingUser: true, }, async () => {
            const url = `${AUTHORIZATION_SERVER_ADDRESS}/api/${AUTHORIZATION_SERVER_API_VERSION}/users/blockUser/${this.props.postDetails.postedBy.id}`;
            try {
                const response = await secureFetch(url, {
                    method: 'GET',
                    headers: {
                        'Content-Type': 'application/json',
                    },
                });
                const json = await response.json();
                this.setState({ isBlockingUser: false });
                    if (json.success) {
                    this.props.done();
                }
                else {
                    reportError('blockUser: secure fetch error occured. ', json);
                    Alert.alert(Strings.t("somethingWentWrongAlertTitle"), Strings.t('somethingWentWrongAlert'));
                }
            }
            catch (error) {
                reportError('blockUser: secure fetch caught error: ', error)
                this.setState({ isBlockingUser: false });
                Alert.alert(Strings.t("somethingWentWrongAlertTitle"), Strings.t('somethingWentWrongAlert'));
            }
        });
    }

    render() {
        return (
            <View
                style={{
                    flex: 1,
                    left: 0,
                    top: 0,
                    position: 'absolute',
                    width: "100%",
                    height: "100%",
                    alignItems: 'center',
                }}>
                <TouchableWithoutFeedback onPress={this.props.done}>
                    <View
                        style={{
                            flex: 1,
                            opacity: 0.8,
                            backgroundColor: StyleConstants.BRAND_COLOR_DARK,
                            width: "100%",
                            height: "100%",
                        }}>
                    </View>
                </TouchableWithoutFeedback>
                <TouchableWithoutFeedback>
                    <View
                        style={{
                            width: "80%",
                            backgroundColor: StyleConstants.BACKGROUND_COLOR,
                            position: 'absolute',
                            top: 160,
                            padding: 20,
                            borderRadius: 10,
                        }}>
                        <View>
                            <TouchableWithoutFeedback onPress={this.blockUser} disabled={this.props.isSelf}>
                                <View>
                                    <Text style={this.props.isSelf ? styles.disabledMenuItem : styles.menuItem}>{Strings.t("blockUser")} {this.props.postDetails.postedBy.firstName} {this.props.postDetails.postedBy.lastName}</Text>
                                </View>
                            </TouchableWithoutFeedback>
                            <TouchableWithoutFeedback onPress={this.reportItem} disabled={this.props.isSelf}>
                                <View>
                                    <Text style={this.props.isSelf ? styles.disabledMenuItem : styles.menuItem}>{Strings.t("reportPost")}</Text>
                                </View>
                            </TouchableWithoutFeedback>
                        </View>
                    </View>
                </TouchableWithoutFeedback>
            </View>

        );
    }
}

const styles = StyleSheet.create({
    menuItem: {
        fontSize: 18,
        paddingBottom: 10,
    },
    disabledMenuItem: {
        fontSize: 18,
        paddingBottom: 10,
        opacity: 0.4,
    }
})

mapStateToProps = (state, ownProps) => {
    return {
        userId: state.user.info.id,
        blockError: state.user.blockError,
        isSelf: ownProps.postDetails.postedBy.id === state.user.info.id,
    }
}

const mapDispatchToProps = (dispatch, ownProps) => {
    return {
        blockUser: (blockedUserId) => {
            return dispatch(UserActions.blockUser(blockedUserId));
        },
    }
}

const PostMenu = connect(mapStateToProps, mapDispatchToProps)(PostMenuClass);
export default PostMenu;