
import React, { Component } from 'react';
import {
    StyleSheet,
    Text,
    View,
    Image,
    TouchableWithoutFeedback,
    TouchableOpacity,
} from 'react-native';

import ParsedText from 'react-native-parsed-text';
import PropTypes from 'prop-types';

import StyleConstants from '../styles/constants';

export default class Comment extends Component {
    constructor(props) {
        super(props);
    }

    // NBTODO: Need to see what's better for performance - Component with shouldComponentUpdate or PureComponent.
    shouldComponentUpdate() {
        return false;
    }

    render() {
        return (
            <View style={styles.view}>
                <View>
                    <View style={{flexDirection:'row', justifyContent:'space-between'}}>
                        <TouchableWithoutFeedback onPress={() => this.props.viewProfile(this.props.userId)} >
                            <View style={{ flexDirection: 'row', alignItems: 'center', paddingBottom: 5 }}>
                                <Image source={this.props.profilePic} style={styles.image} />
                                <View>
                                    <Text style={styles.nameText}>{this.props.firstName} {this.props.lastName}</Text>
                                    <Text style={styles.dateText}>{this.props.commentDate}</Text>
                                </View>
                            </View>
                        </TouchableWithoutFeedback>
                        <View style={{ alignItems: 'flex-end', }}>
                            <TouchableOpacity style={{ marginRight: 10 }} onPress={() => { this.props.showPostMenu({ id: this.props.commentId, title: 'comment', text: this.props.text, postedBy: { firstName: this.props.firstName, lastName: this.props.lastName, id: this.props.userId } }) }}>
                                <View style={{ width: 20, height: 20, justifyContent: 'center', alignItems: 'flex-start' }}>
                                    <Image source={require('../assets/icon-drop-down.png')} style={{ width: 15, height: 15, opacity: 0.3 }} />
                                </View>
                            </TouchableOpacity>
                        </View>
                    </View>
                </View>
                <ParsedText style={styles.commentText}
                    parse={
                        [
                            { type: 'url', style: { color: StyleConstants.BRAND_COLOR_DARK }, onPress: this.props.handleUrlPress },
                            { type: 'phone', style: { color: StyleConstants.BRAND_COLOR_DARK }, onPress: this.props.handlePhonePress },
                            { type: 'email', style: { color: StyleConstants.BRAND_COLOR_DARK }, onPress: this.props.handleEmailPress },
                        ]
                    }>
                    {this.props.text}
                </ParsedText>
                <View style={{ alignItems: 'flex-end', }}>
                </View>

            </View>
        )
    }
}

const styles = StyleSheet.create({
    view: {
        backgroundColor: StyleConstants.COMMENT_BG_COLOR,
        borderRadius: 15,
        // NBTODO: Maybe use the borderTopLeftRadius
        paddingVertical: 15,
        marginVertical: 5,
    },
    image: {
        width: 40,
        height: 40,
        borderRadius: 20,
        marginHorizontal: 10
    },
    nameText: {
        fontSize: 16,
    },
    dateText: {
        fontSize: 12,
        color: 'grey',
    },
    commentText: {
        fontSize: 16,
        paddingHorizontal: 10,
    }
})

Comment.propTypes = {
    userId: PropTypes.string.isRequired,
    firstName: PropTypes.string.isRequired,
    lastName: PropTypes.string.isRequired,
    text: PropTypes.string.isRequired,
    commentDate: PropTypes.string.isRequired, // NBTODO Expect a date string for now. It wouldn't take a Date object - need to see why...
}