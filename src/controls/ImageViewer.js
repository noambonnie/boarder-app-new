import React, { Component } from 'react';
import {
    Text,
    View,
    Animated,
    ActivityIndicator,
    StyleSheet,
} from 'react-native';
import ImageViewer from 'react-native-image-zoom-viewer';
import StyleConstants from '../styles/constants';
import { SCREEN_HEIGHT, HEADER_HEIGHT } from '../common/constants';

class PositionIndicator extends React.Component {
    constructor(props) {
        super(props);
        this.opacityAnim = new Animated.Value(1);
    }

    componentDidUpdate(prevProps) {
        if (prevProps.current !== this.props.current) {
            this.animateIndicator();
        }
    }

    componentDidMount() {
        this.animateIndicator();
    }

    animateIndicator = () => {
        Animated.sequence([
            Animated.timing(this.opacityAnim, {
                toValue: 1,
                duration: 200,
                useNativeDriver: true,
            }),
            Animated.timing(this.opacityAnim, {
                toValue: 0,
                duration: 1000,
                useNativeDriver: true,
                delay: 1500,
            }),
        ]).start();
    }

    render() {
        return (
            <View style={imageViewerStylesheet.positionIndicatorContainer}>
                <Animated.View style={[imageViewerStylesheet.positionIndicatorView, { opacity: this.opacityAnim }]}>
                    <Text style={imageViewerStylesheet.positionIndicatorText}>{this.props.current}/{this.props.total}</Text>
                </Animated.View>
            </View>
        )
    }
}

export default class BoarderImageViewer extends React.Component {
    render() {
        return (
            <ImageViewer
                index={this.props.navigation.getParam("index") || 0}
                imageUrls={this.props.navigation.getParam("images")} backgroundColor={StyleConstants.BACKGROUND_COLOR}
                loadingRender={() => (<View style={imageViewerStylesheet.loadingContainer}><ActivityIndicator size="large" color={StyleConstants.BRAND_COLOR_DARK}/></View>)}
                saveToLocalByLongPress={false}
                onCancel={() => this.props.navigation.pop()}
                renderIndicator={(current, total) => (<PositionIndicator current={current} total={total} />)}
                enableSwipeDown={true}
            />
        )
    }
}

const imageViewerStylesheet = StyleSheet.create({
    positionIndicatorContainer: { 
        position: "absolute", 
        bottom: 40, 
        left: 0, 
        right: 0, 
        alignItems: "center", 
    },
    positionIndicatorView: { 
        paddingVertical: 5, 
        paddingHorizontal: 10, 
        borderRadius: 16, 
        backgroundColor: StyleConstants.BRAND_COLOR_LIGHT, 
        width: 50, 
        alignItems: "center",
    },
    positionIndicatorText: { 
        color: StyleConstants.LIGHT_TEXT_COLOR, 
        fontSize: 16, 
    },
    loadingContainer: { 
        height: "100%", 
        alignItems: "center",
        justifyContent: "center",
    }
});