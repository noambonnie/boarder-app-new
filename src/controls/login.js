import {
    SafeAreaView,
    View,
    Image,
    Text,
    TextInput,
    TouchableOpacity,
    TouchableWithoutFeedback,
    StyleSheet,
    ScrollView,
    Platform,
    I18nManager,
} from 'react-native';
import React, { Component } from 'react';
import StyleConstants from '../styles/constants';
import PropTypes from 'prop-types';
import KeyboardSpacer from 'react-native-keyboard-spacer';

import Strings from '../assets/strings'
import { GoogleSigninButton } from 'react-native-google-signin';

// NBTODO: Be above the keyboard line when the keyboard is displayed.
export default class Login extends Component {
    constructor(props) {
        super(props);
        this.state = {
            email: "",
            password: "",
        }
    }

    doLogin = () => {
        this.loginScrollView.scrollTo({ x: 0, y: 0, animated: true });
        this.props.onSubmit(this.state.email, this.state.password)
    }

    doGoogleLogin = async () => {
        this.loginScrollView.scrollTo({ x: 0, y: 0, animated: true });
        this.props.onGoogleLogin();
    }

    doFacebookLogin = async (error, result) => {
        this.loginScrollView.scrollTo({ x: 0, y: 0, animated: true });
        this.props.onFacebookLogin(error, result);
    }

    render() {
        return (
            <SafeAreaView style={{ flex: 1, backgroundColor: StyleConstants.BRAND_COLOR_LIGHT }}>
                <ScrollView
                    ref={(component) => this.loginScrollView = component}
                    keyboardShouldPersistTaps="always"
                    contentContainerStyle={{ alignItems: 'center', backgroundColor: StyleConstants.BRAND_COLOR_LIGHT }}>

                    <View style={{ flexDirection: "row", justifyContent: 'center', alignItems: 'center', paddingTop: 20, width: "80%" }}>
                        <Image style={{ height: 100, width: 100 }} source={require("../assets/app-logo.png")} />
                        <View>
                            <Text style={{ color: StyleConstants.LIGHT_TEXT_COLOR, fontSize: 40, marginLeft: 10, fontWeight: '700', }}>
                                {Strings.t('appName')}
                            </Text>
                            <Text style={{ color: StyleConstants.LIGHT_TEXT_COLOR, fontSize: 16, marginLeft: 10 }}>
                                {Strings.t('tagline1')}
                            </Text>
                            <Text style={{ color: StyleConstants.LIGHT_TEXT_COLOR, fontSize: 16, marginLeft: 10 }}>
                                {Strings.t('tagline2')}
                            </Text>
                        </View>
                    </View>
                    <Text style={{ color: 'yellow', fontSize: 14, paddingVertical: 10, width: '80%' }}>
                        {this.props.errorMessage}
                    </Text>
                    <TextInput style={Styles.input}
                        onChangeText={(text) => this.setState({ email: text })}
                        autoCapitalize="none"
                        autoCorrect={false}
                        keyboardType='email-address'
                        returnKeyType="next"
                        placeholder={Strings.t('emailField')}
                        placeholderTextColor='lightgrey'
                        underlineColorAndroid="transparent" />

                    <TextInput ref={(passField) => this.passField = passField} style={Styles.input}
                        onChangeText={(text) => this.setState({ password: text })}
                        returnKeyType="go"
                        placeholder={Strings.t('passwordField')}
                        placeholderTextColor='lightgrey'
                        underlineColorAndroid="transparent"
                        secureTextEntry />
                    <TouchableOpacity style={Styles.buttonContainer}
                        onPress={this.doLogin}>
                        <Text style={Styles.buttonText}>{Strings.t('loginButton')}</Text>
                    </TouchableOpacity>
                    <TouchableWithoutFeedback style={{ marginVertical: 10 }} onPress={this.doFacebookLogin}>
                        <View style={{ flexDirection: "row", justifyContent: "flex-start", alignItems: "center", backgroundColor: "#3B5998", width: 212, height: 40, borderRadius: 3, marginVertical: 10 }}>
                            <Image source={require("../assets/facebook-logo.png")} style={{ height: 20, width: 20, resizeMode: "stretch", marginLeft: 10 }} />
                            <View style={{ alignItems: "center", width: "80%" }}>
                                <Text style={{ color: "white", fontSize: 14, fontWeight: "bold" }}>Login with Facebook</Text>
                            </View>
                        </View>
                    </TouchableWithoutFeedback>
                    <GoogleSigninButton
                        style={{ width: 220, height: 48 }}
                        size={GoogleSigninButton.Size.Wide}
                        color={GoogleSigninButton.Color.Light}
                        onPress={this.doGoogleLogin}
                    />
                    <TouchableOpacity style={{ marginTop: 80, }} onPress={this.props.goToSignUp}>
                        <Text style={{ color: StyleConstants.LIGHT_TEXT_COLOR, fontSize: 16, paddingBottom: 10, textDecorationLine: 'underline' }}>{Strings.t("goToSignUp")}</Text>
                    </TouchableOpacity>
                    {Platform.OS === 'ios' && (<KeyboardSpacer />)}
                </ScrollView>
            </SafeAreaView>
        )
    }
}

// define your styles
const Styles = StyleSheet.create({
    container: {
        padding: 20,
        alignItems: 'center',
    },
    input: {
        height: 50,
        width: "80%",
        marginBottom: 10,
        paddingHorizontal: 10,
        fontSize: 18,
        color: 'black',
        borderRadius: 7,
        backgroundColor: StyleConstants.BACKGROUND_COLOR,
        textAlign: I18nManager.isRTL? "right" : "left",
    },
    buttonContainer: {
        backgroundColor: "#416485",
        paddingVertical: 15,
        width: "80%",
        borderRadius: 7,
        marginBottom: 20,
    },
    buttonText: {
        color: '#fff',
        textAlign: 'center',
        fontWeight: '700',
        fontSize: 18,
    }
});

Login.propTypes = {
    onSubmit: PropTypes.func.isRequired,
    goToSignUp: PropTypes.func.isRequired,
    errorMessage: PropTypes.string.isRequired,
}
