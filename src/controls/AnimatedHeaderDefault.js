import React from 'react';
import {
    Platform,
    View,
    Text,
    StyleSheet,
} from 'react-native';
import { AnimatedHeader } from '../common/headerAnimator';
import DrawerButton from './DrawerButton';
import { navigationStylesheet } from '../styles/constants'

export default class AnimatedHeaderDefault extends React.Component {

    constructor(props) {
        super(props);
    }

    render() {
        return (
            <AnimatedHeader>
                {Platform.select(
                    {
                        // On iOS, I want the header centered. Since I use absolute positioning for that, I need
                        // to render the drawer button after the label. There's probably a cleaner way to do it
                        // but sticking with this for now as I'm too tired to figure it out.
                        "ios": (
                            <View style={headerStylesheet.headerContainer}>
                                <DrawerButton />
                                <View style={headerStylesheet.iosLabelContainer}>
                                    <Text style={navigationStylesheet.headerText}>{this.props.label}</Text>
                                </View>
                            </View>
                        ),
                        "android": (
                            <View style={headerStylesheet.headerContainer}>
                                <DrawerButton />
                                <Text style={navigationStylesheet.headerText}>{this.props.label}</Text>
                            </View>
                        )
                    }

                )}
            </AnimatedHeader>
        )
    }
}

const headerStylesheet = StyleSheet.create({
    headerContainer: {
        flex: 1,
        flexDirection: 'row',
        alignItems: "center"
    },
    iosLabelContainer: {
        position: "absolute",
        width: "100%",
        alignItems: "center",
        justifyContent: "center"
    },

})
