import React, { Component } from 'react';
import {
    Animated,
    Platform,
    FlatList,
    TouchableWithoutFeedback,
    View,
    Text,
    Image,
    Slider,
    StyleSheet,
    I18nManager,
} from 'react-native';
import { HEADER_HEIGHT, STATUS_BAR_HEIGHT, SCREEN_HEIGHT } from '../common/constants';
import StyleConstants from '../styles/constants';
import Strings from '../assets/strings';
import { MIN_SEARCH_LENGTH } from '../common/googlePlacesSearch';

// NBTODO: This should become a part of HeaderLocationSearchBar, which should expand the header size to full screen. It will 
//         fix issues with stuttering animation and they are tightly coupled.
export default class SearchResultsOverlay extends Component {
    constructor(props) {
        super(props);
        this.searchOverlayAnim = new Animated.Value(-SCREEN_HEIGHT);
        this.initialValue = props.currentFilter.byDistance.distance;

        this.state = {
            // Keep distance on the state for faster rendering, otherwise it's a bit jittery.
            distance: props.currentFilter.byDistance.distance,
        }
    }

    componentDidUpdate(prevProps) {
        if (this.props.isSearchFocused && !prevProps.isSearchFocused) {
            Animated.timing(this.searchOverlayAnim, {
                toValue: 0,
                duration: 500,
                useNativeDriver: true,
            }, ).start();
        }
        else if (!this.props.isSearchFocused && prevProps.isSearchFocused) {
            Animated.timing(this.searchOverlayAnim, {
                toValue: -SCREEN_HEIGHT,
                duration: 500,
                useNativeDriver: true,
            }, ).start();
        }
    }

    // NBTODO: Instead of the ugly special cases for "current location" and "no results" in the FlatList, there should be a conditional rendering of components.
    render() {
        return (
            <Animated.View style={[
                searchOverlayStyleSheet.container,
                {
                    transform: [{
                        translateY: this.searchOverlayAnim,
                    }]
                }]}>
                <View>
                    <View style={searchOverlayStyleSheet.sliderContainer}>
                        <Text style={{ color: StyleConstants.BRAND_COLOR_DARK }}>{Strings.t("search.filterDistance")}</Text>
                        <Slider
                            style={searchOverlayStyleSheet.slider}
                            step={5}
                            minimumValue={5}
                            maximumValue={50}
                            value={this.initialValue}
                            thumbTintColor={StyleConstants.BRAND_COLOR_DARK}
                            minimumTrackTintColor={StyleConstants.BRAND_COLOR_LIGHT}
                            onValueChange={(distance) => this.setState({ distance })}
                            onSlidingComplete={(distance) => this.props.setFeedDistance(distance)}
                        />
                        <Text style={{ color: StyleConstants.BRAND_COLOR_DARK }}>{Strings.t("search.filterDistanceCurrentSelection", { distance: this.state.distance, units: Strings.t("km") })}</Text>
                    </View>

                    <FlatList
                        data={[{ placeId: "current-location", description: Strings.t("nearMeSearchResult") }, ...(this.props.predictions.length > 0 ? this.props.predictions : [{ placeId: "no-results" }])]}
                        keyExtractor={(item, index) => item.placeId}
                        keyboardShouldPersistTaps='always'
                        renderItem={({ item, }) => {
                            return (
                                <TouchableWithoutFeedback onPress={() => {
                                    item.placeId !== 'no-results' && this.props.setFeedLocation(item.placeId === 'current-location'? null : item.placeId);
                                }}>
                                    <View style={[
                                        searchOverlayStyleSheet.resultContainer,
                                        {
                                            borderBottomWidth: item.placeId === 'current-location' ? 1 : 0,
                                        }
                                    ]}>
                                        <Text
                                            numberOfLines={1}
                                            style={[
                                                searchOverlayStyleSheet.result,
                                                {
                                                    fontStyle: item.placeId === 'no-results' && !I18nManager.isRTL ? "italic" : "normal",
                                                    fontWeight: item.placeId === 'current-location' ? "bold" : "normal",
                                                }
                                            ]}>
                                            {item.placeId === "no-results" ? (this.props.searchQuery.length < MIN_SEARCH_LENGTH ? Strings.t("search.startTyping") : Strings.t("search.noResults")) : item.description}
                                        </Text>
                                    </View>
                                </TouchableWithoutFeedback>
                            )
                        }}
                    />
                    {
                        this.props.predictions.length > 0 &&
                        (<View style={{ alignItems: "flex-end", paddingTop: 5, marginHorizontal: 20 }}>
                            <Image source={require("../assets/powered_by_google_on_white.png")} style={{ resizeMode: "contain", height: 20 }} />
                        </View>)
                    }
                </View>

            </Animated.View>
        )
    }
}

const searchOverlayStyleSheet = StyleSheet.create(
    {
        container: {
            paddingTop: Platform.OS === "ios" ? HEADER_HEIGHT + STATUS_BAR_HEIGHT : HEADER_HEIGHT,
            backgroundColor: StyleConstants.BACKGROUND_COLOR,
            borderBottomWidth: 1,
            borderBottomColor: StyleConstants.BRAND_COLOR_LIGHT,
            height: "100%",
            width: "100%",
            position: "absolute",
            top: 0,
            left: 0,
        },
        sliderContainer: {
            width: "90%",
            flexDirection: 'row',
            marginHorizontal: 15,
            paddingTop: 10,
            paddingBottom: Platform === "ios"? 0: 10,
            justifyContent: 'space-between',
            alignItems: 'center',
        },
        slider: {
            flex: 1,
            marginHorizontal: 5,
        },
        resultContainer: {
            marginHorizontal: 15,
            paddingVertical: 5,
            borderBottomColor: "lightgrey"
        },
        result: {
            color: StyleConstants.BRAND_COLOR_DARK,
            fontSize: 16,
        }
    }
)