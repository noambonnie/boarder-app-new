import { secureFetch } from './authApi';
import { FEED_SERVER_ADDRESS, FEED_SERVER_API_VERSION } from './constants';
import { reportError } from './errorReporter';

export default class ImageUpload {
    constructor(imageDetails, imageType, setProgress, setUploadFailed) {
        this.imageType = imageType;
        this.imageDetails = { ...imageDetails };
        this.setProgress = setProgress;
        this.setUploadFailed = setUploadFailed;
    }

    getPresignedUrl = async () => {
        const url = `${FEED_SERVER_ADDRESS}/api/${FEED_SERVER_API_VERSION}/images/uploadUrl/${this.imageType}?contentType=${this.imageDetails.mime}`;

        try {
            const response = await secureFetch(url, {
                method: 'GET',
            });

            const json = await response.json();
            if (json.success) {
                return json;
            }
            else {
                throw new Error("json failure");
            }
        }
        catch (error) {
            reportError('getPresignedUrl: secure fetch caught error: ', error)
        }
    }

    uploadImage = async () => {
        this.uploadPromise = new Promise(async (resolve, reject) => {
            const { url, key } = await this.getPresignedUrl();
            this.key = key;
    
            const xhr = new XMLHttpRequest();
            this.setProgress && xhr.upload.addEventListener('progress', (event) => {
                this.setProgress(Math.round(event.loaded / event.total * 100));
            });

            xhr.addEventListener('error', (event) => { 
                this.setUploadFailed && this.setUploadFailed();
                reportError('XHR Error', event);
                reject("XHR Error");
            });

            xhr.onreadystatechange = () => {
                if (xhr.readyState === 4) {
                    if (xhr.status === 200) {
                        resolve();
                    } else {
                        this.setUploadFailed && this.setUploadFailed();
                        reject(`Failed to upload ${key}: (${xhr.status}) ${xhr.statusText}`);
                    }
                }
            };
            xhr.open('PUT', url);
            xhr.setRequestHeader('Content-Type',this.imageDetails.mime);
            xhr.send({
                uri: this.imageDetails.path,
                name: key,
            });
    
        })

        return this.uploadPromise;
    }

    getUploadPromise = () => this.uploadPromise;
}