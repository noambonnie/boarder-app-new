// NBTODOv2  import RNFS from 'react-native-fs';
import { Platform } from 'react-native';
import { STATS_SERVER_ADDRESS, STATS_SERVER_API_VERSION } from './constants';

// At the time of writing, only Document directory was valid for Android.
// NBTODOv2 const errorFileLocation = (Platform.OS === 'android' ? RNFS.DocumentDirectoryPath : RNFS.TemporaryDirectoryPath) + '/logs';

let reduxStore;
export const initErrorReporter = (store) => {
    reduxStore = store;
    // NBTODOv2 setInterval(uploadErrorsToServer, 30000); // Attempt upload every 30 seconds
}
export const reportError = async (message, error) => {
    try {
        console.error(message, error);

        // On Android the app might run without UI and store initialized (lauched to background). So skip this for now.
        // NBTODO: There's no reason why I shouldn't be able to initialize the store in the background.
        if (!reduxStore)
            return;

        // NBTODOv2
        return;

        const state = reduxStore.getState();
        const errorString = error instanceof Error ? error.toString() : JSON.stringify(error, null, 4);
        const errorFileName = errorFileLocation + '/' + (state.user.info.id ? state.user.info.id : 'unknown') + '-' + Date.now() + '.err';
        await RNFS.mkdir(errorFileLocation);
        await RNFS.writeFile(errorFileName, message + '\n');
        await RNFS.appendFile(errorFileName, errorString);
        await RNFS.appendFile(errorFileName, '\n' + JSON.stringify(state, null, 4));

        uploadLog(errorFileName);
    }
    catch (err) {
        console.error("Failed to write error. Reason: ", err);
        console.warn(`Reported error was: '${message} ${error}'`);
    }
}
const uploadLog = async (fileName) => {
    // NBTODO: Are all user-related things handled by Auth server? Or should this go to a different server?
    let url = `${STATS_SERVER_ADDRESS}/api/clear/${STATS_SERVER_API_VERSION}/ops/uploadLog`;
    let data = new FormData();

    const file = {
        uri: 'file://' + fileName,             // e.g. 'file:///path/to/file/file.err'
        name: fileName.substr(fileName.lastIndexOf("/") + 1),            // e.g. 'file.err,
        type: 'text/plain',
    }

    data.append('file', file);

    return fetch(url, {
        method: 'POST',
        body: data,
    })
        // Fetch success - convert to JSON
        .then((res) => {
            if (!res.ok) {
                console.warn('Upload log file failed: ', res);
                return;
                // Not the end of the world - ignore error.
            }

            return RNFS.unlink(fileName);
        })
        // Fetch failure
        .catch((error) => {
            console.warn('Upload log file caught error: ', error);
        });


}

const uploadErrorsToServer = async () => {
    try {
        const exists = await RNFS.exists(errorFileLocation);

        if (!exists) {
            return;
        }
        
        const errorFiles = await RNFS.readDir(errorFileLocation);
        const uploadPromises = errorFiles.map((file) => {
            return uploadLog(file.path);
        })

        await Promise.all(uploadPromises);
    }
    catch (error) {
        console.error("Error uploading log files: ", error);
    }

    const remainingFiles = await RNFS.readDir(errorFileLocation);
    if (remainingFiles.length > 20) {
        remainingFiles.sort((a, b) => {
            return new Date(a.mtime).getTime() - new Date(b.mtime).getTime()
        })
    }

    for (let i = 0; i < remainingFiles.length - 20; i++) {
        RNFS.unlink(remainingFiles[i].path)
    }
}