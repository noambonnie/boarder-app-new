import ImagePicker from 'react-native-image-crop-picker';
import {
    Alert,
} from 'react-native';
import Strings from '../assets/strings';
import { reportError } from '../common/errorReporter';
import StyleConstants from '../styles/constants';

export const getProfilePic = async (handleImageSet) => {
    const handleGetFromCamera = async () => {
        try {
            const image = await pickImage(ImagePicker.openCamera);
            handleImageSet(image);
        }
        catch (error) {
            if (error.code === 'E_PERMISSION_MISSING' || 
                error.code === 'E_PICKER_NO_CAMERA_PERMISSION') {
                Alert.alert(Strings.t('permissionNeededToProceed'));
            }
            else if (error.code !== 'E_PICKER_CANCELLED') {
                Alert.alert(Strings.t("somethingWentWrongAlertTitle"), Strings.t('somethingWentWrongAlert'));
                reportError("Failed to take picture from camera.", error)
            }
        }
    }

    const handleGetFromGallery = async () => {
        try {
            const image = await pickImage(ImagePicker.openPicker);
            handleImageSet(image);
        }
        catch (error) {
            if (error.code === 'E_PERMISSION_MISSING') {
                Alert.alert(Strings.t('permissionNeededToProceed'));
            }
            else if (error.code !== 'E_PICKER_CANCELLED') {
                Alert.alert(Strings.t("somethingWentWrongAlertTitle"), Strings.t('somethingWentWrongAlert'));
                reportError("Failed to take picture from camera.", error)
            }
        }
    }

    Alert.alert(Strings.t("profilePicDialogTitle"),
        Strings.t("profilePicDialogText"),
        [
            { text: Strings.t("fromCamera"), onPress: handleGetFromCamera },
            { text: Strings.t("fromGallery"), onPress: handleGetFromGallery },
        ], { cancelable: true });
}

const pickImage = async(pickerFunction) => {
    const image = await pickerFunction({
        width: 300,
        height: 300,
        mediaType: 'photo',
        cropping: true,
        cropperCircleOverlay: true,
        useFrontCamera: true,
        cropperActiveWidgetColor: StyleConstants.BRAND_COLOR_DARK,
        cropperStatusBarColor: StyleConstants.BRAND_COLOR_DARK,
        cropperToolbarColor: StyleConstants.BRAND_COLOR_LIGHT,

        // NBTODO: This crashes the app, I think it only takes hex values. 
        // cropperActiveWidgetColor: StyleConstants.BRAND_COLOR_LIGHT,
        // cropperStatusBarColor: StyleConstants.BRAND_COLOR_DARK,
        // cropperToolbarColor: StyleConstants.BRAND_COLOR_LIGHT,
    })

    return { profilePic: image.path, mime: image.mime }
}

export const pickMultipleImages = async (maxFiles) => {
    try {
        const images = await ImagePicker.openPicker({
            multiple: true,
            mediaType: "photo",
            maxFiles: maxFiles,
        })
    
        return images;
    
    }
    catch (error) {
        if (error.code === 'E_PERMISSION_MISSING') {
            Alert.alert(Strings.t('permissionNeededToProceed'));
        }
        else if (error.code !== 'E_PICKER_CANCELLED') {
            Alert.alert(Strings.t("somethingWentWrongAlertTitle"), Strings.t('somethingWentWrongAlert'));
            reportError("Failed to select photos from gallery.", error)
        }
    }
    return [];
}