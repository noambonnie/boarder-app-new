import { Platform, Dimensions, StatusBar } from "react-native";
// NBTODOv2
// import Config from 'react-native-config';

// NBTODO: I'm not sure I like this. Why not read directly from config wherever needed instead of having this
//         constants file?

// Feed server
// export const FEED_SERVER_ADDRESS = Config.FEED_SERVER_ADDRESS;
// export const FEED_SERVER_API_VERSION = Config.FEED_SERVER_API_VERSION;

// // Authorization/Authentication server
// export const AUTHORIZATION_SERVER_ADDRESS = Config.AUTHORIZATION_SERVER_ADDRESS;
// export const AUTHORIZATION_SERVER_API_VERSION = Config.AUTHORIZATION_SERVER_API_VERSION;

// // Stats and log collection
// export const STATS_SERVER_ADDRESS = Config.STATS_SERVER_ADDRESS;
// export const STATS_SERVER_API_VERSION = Config.STATS_SERVER_API_VERSION;

// export const FEED_SERVER_ADDRESS = "http://192.168.1.6:3000"
// export const FEED_SERVER_API_VERSION = "1"
// export const AUTHORIZATION_SERVER_ADDRESS = "http://192.168.1.6:3000"
// export const AUTHORIZATION_SERVER_API_VERSION = "1"
// export const STATS_SERVER_ADDRESS = "http://192.168.1.6:3000"
// export const STATS_SERVER_API_VERSION = "1"

export const ENV_NAME="PROD"
export const FEED_SERVER_ADDRESS="https://prod.app.boarder.io:443"
export const FEED_SERVER_API_VERSION="1"
export const AUTHORIZATION_SERVER_ADDRESS="https://prod.app.boarder.io:443"
export const AUTHORIZATION_SERVER_API_VERSION="1"
export const STATS_SERVER_ADDRESS="https://prod.app.boarder.io:443"
export const STATS_SERVER_API_VERSION="1"

// export const ENV_NAME="AWS_FREE"
// export const ENV_TYPE="TEST"
// export const FEED_SERVER_ADDRESS="https://aws-free-lb.boarder.io:443"
// export const FEED_SERVER_API_VERSION="1"
// export const AUTHORIZATION_SERVER_ADDRESS="https://aws-free-lb.boarder.io:443"
// export const AUTHORIZATION_SERVER_API_VERSION="1"
// export const STATS_SERVER_ADDRESS="https://aws-free-lb.boarder.io:443"
// export const STATS_SERVER_API_VERSION="1"

// export const ENV_NAME="LAPTOP"
// export const ENV_TYPE="TEST"
// export const FEED_SERVER_ADDRESS="http://Noams-MacBook-Pro.local:3000"
// export const FEED_SERVER_API_VERSION="1"
// export const AUTHORIZATION_SERVER_ADDRESS="http://Noams-MacBook-Pro.local:3000"
// export const AUTHORIZATION_SERVER_API_VERSION="1"
// export const STATS_SERVER_ADDRESS="http://Noams-MacBook-Pro.local:3000"
// export const STATS_SERVER_API_VERSION="1"

export const SCREEN_HEIGHT = Dimensions.get('window').height;
export const SCREEN_WIDTH = Dimensions.get('window').width;

console.info(`Screen height: ${SCREEN_HEIGHT}, Screen width: ${SCREEN_WIDTH}`)
const isIphoneX = () => {
    return (
        // This has to be iOS duh
        Platform.OS === 'ios' &&
        // Accounting for the height in either orientation
        (SCREEN_HEIGHT === 812 || SCREEN_WIDTH === 812)
    );
}

export const HEADER_HEIGHT = Platform.OS === 'ios' ? 44 : 56;
export const TOP_TAB_BAR_HEIGHT = Platform.OS === 'ios' ? 0 : 48;
export const STATUS_BAR_HEIGHT = Platform.OS === 'ios' ? (isIphoneX() ? 44 : 20) : StatusBar.currentHeight; // iPhone X 44, others 20

