import React from 'react';
import {
    Animated,
    Platform,
    View,
    StyleSheet,
} from 'react-native';

import { HEADER_HEIGHT, STATUS_BAR_HEIGHT } from './constants';
import StyleConstants from '../styles/constants';

export const headerAnim = new Animated.Value(0);

let headerVisible = true;

export const setHeaderVisible = (visible) => {
    if (visible === headerVisible)
        return;

    headerVisible = visible;
    Animated.timing(headerAnim).stop();
    Animated.timing(headerAnim, {
        toValue: HEADER_HEIGHT * (visible ? -1 : 1),
        duration: 300,
        useNativeDriver: true,
    }, ).start();
};

export const navbarTranslate = headerAnim.interpolate({
    inputRange: [0, HEADER_HEIGHT],
    outputRange: [0, -HEADER_HEIGHT - Platform.select({ ios: STATUS_BAR_HEIGHT, android: 0 })],
    extrapolate: 'clamp',
});

export class AnimatedHeader extends React.Component {
    constructor(props) {
        super(props);
    }

    render() {
        return (
            <Animated.View style={headerStyleSheet.headerContainer}>
                { Platform.OS === 'ios' && (<View style={headerStyleSheet.statusBarFiller} />) }
                {this.props.children}
            </Animated.View>

        )
    }
}

const headerStyleSheet = StyleSheet.create({
    headerContainer: {
        backgroundColor: StyleConstants.BACKGROUND_COLOR,
        height: HEADER_HEIGHT + (Platform.OS === 'ios' ? STATUS_BAR_HEIGHT : 0),
        flex: 1,
        position: "absolute", left: 0, right: 0, top: 0,
        transform: [{
            translateY: navbarTranslate,
        }],
        borderBottomWidth: Platform.OS === "ios" ? 1 : 0,
        borderBottomColor: "lightgrey",
    },
    statusBarFiller: { 
        width: "100%", 
        minHeight: STATUS_BAR_HEIGHT, 
    }
})