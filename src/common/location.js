'use strict'
import * as Actions from '../state/actionsLocation';
import { reportError } from '../common/errorReporter';
import FusedLocation from 'react-native-fused-location';
import { Platform, PermissionsAndroid } from 'react-native';

let reduxStore;
let currentAppState = 'active';
let loggedIn = false;
let locationWatchId;

// Polling location rather than watchPosition because I don't need super accurate location, and watchPosition
// keeps the location arrow on iOS turned on, which people may find creepy. So instead, I poll every minute.
//
// To complicate things a bit, getCurrentPosition() takes forever to find a location (iOS) whereas watchPosition returns 
// almost immediately. So I enable and disable the watch every time I get a position to achieve the poll effect.

// NBTODO: This file seems overly complicated for what it does. Why is there distinction between pause/stop and start/resume?
const startLocationPolling = () => {
    locationWatchId = navigator.geolocation.watchPosition(updatePositionFromPoll, errorPositionFromPoll, { timeout: 20000 });
};

const stopLocationPolling = () => {
    navigator.geolocation.clearWatch(locationWatchId);
    locationWatchId = null;
};

const updatePositionFromPoll = (position) => {
    updatePosition(position);
    stopLocationPolling();
    setTimeout(startLocationPolling, 60000);
}

const errorPositionFromPoll = (error) => {
    errorPosition(error);
    stopLocationPolling();
    setTimeout(startLocationPolling, 5000);
}

const updatePosition = (position) => {
    let location = {};
    let { latitude, longitude } = position.coords;

    location.position = { latitude, longitude };
    location.lastReceived = position.timestamp;

    reduxStore.dispatch(Actions.receiveCurrentLocation(location));
}

const errorPosition = (error) => {
    console.warn('Error getting position: ', error);
    reduxStore.dispatch(Actions.failureCurrentLocation(error));
}

const handleStateChange = () => {
    const newLoggedIn = reduxStore.getState().user.tokens.refreshToken != null;
    const newAppState = reduxStore.getState().app.appState;

    // Start tracking location after login.
    if (!loggedIn && newLoggedIn) {
        console.info("User logged in. Starting location tracking.");
        trackLocation();
    }

    if (loggedIn && currentAppState === 'active' && newAppState !== 'active' ||
        loggedIn && !newLoggedIn) {
        // Disable location tracking.
        console.info('App is not active or user logged out. Disabling location tracking.')
        pauseLocationTracking();
    }
    else if (currentAppState !== 'active' && newAppState === 'active' && loggedIn) {
        // Enable location tracking.
        console.info('App is active. Enabling location tracking')
        resumeLocationTracking();
    }
    currentAppState = newAppState;
    loggedIn = newLoggedIn;
}

const platformDependetPauseLocationTracking = {
    ios: stopLocationPolling,
    android: () => {
        FusedLocation.stopLocationUpdates();
    }
}

const pauseLocationTracking = () => {
    Platform.select(platformDependetPauseLocationTracking)();
}

const platformDependentResumeLocationTracking = {
    ios: () => {
        if (locationWatchId) {
            clearInterval(locationWatchId);
            locationWatchId = null;
        }

        locationWatchId = startLocationPolling();
    },
    android: () => {
        FusedLocation.startLocationUpdates();
    }
}

const resumeLocationTracking = () => {
    Platform.select(platformDependentResumeLocationTracking)();
}

const fusedLocationToPosition = (location) => {
    return {
        coords: {
            latitude: location.latitude,
            longitude: location.longitude,
        },
        timestamp: Number(location.timestamp),
    }
}

const trackLocationAndroid = async () => {
    try {

        // NBTODOv2 handle the case where permission is not granted (ask again)
        const granted = await PermissionsAndroid.request(
            PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION, {
                // NBTODOv2: This title doesn't show when requesting permissions.
                title: 'App needs to access your location',
                message: 'App needs access to your location ' +
                    'so we can let our app be even more awesome.'
            }
        );

        if (granted) {

            FusedLocation.setLocationPriority(FusedLocation.Constants.HIGH_ACCURACY);

            // Get location once.
            try {
                const location = await FusedLocation.getFusedLocation();
                updatePosition(fusedLocationToPosition(location));
            }
            catch(error) {
                reportError("Error getting initial location", error);
            }


            // Set options.
            FusedLocation.setLocationPriority(FusedLocation.Constants.BALANCED);
            FusedLocation.setLocationInterval(60000);
            FusedLocation.setFastestLocationInterval(30000);
            FusedLocation.setSmallestDisplacement(100);


            // Keep getting updated location.
            FusedLocation.startLocationUpdates();

            // Place listeners.
            let subscription = FusedLocation.on('fusedLocation', location => {
                /*  
                    For reference:
                    location = {
                        latitude: 14.2323,
                        longitude: -2.2323,
                        speed: 0,
                        altitude: 0,
                        heading: 10,
                        provider: 'fused',
                        accuracy: 30,
                        bearing: 0,
                        mocked: false,
                        timestamp: '1513190221416'
                    }
                */

                // Normalize the position structure across platforms - turn the location structure we got
                // into a structure similar to what we get from navigator.geolocation.watchPosition().
                updatePosition(fusedLocationToPosition(location));
            });
        }
    }
    catch (error) {
        reportError("Error initializing location", error);
    }

}

const platformDependentTrackLocation =
{
    ios: startLocationPolling,
    android: trackLocationAndroid
}

const trackLocation = () => {
    Platform.select(platformDependentTrackLocation)();
}

export const startLocationTracking = (store) => {
    reduxStore = store;
    loggedIn = reduxStore.getState().user.tokens.refreshToken != null;

    if (loggedIn) {
        console.info("Starting location tracking");
        trackLocation();
    }

    reduxStore.subscribe(handleStateChange);
}