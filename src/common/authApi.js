import { AUTHORIZATION_SERVER_ADDRESS, AUTHORIZATION_SERVER_API_VERSION } from './constants';
import { AsyncStorage, Platform } from 'react-native';
import { reportError } from '../common/errorReporter';
import { setProfilePicture } from '../state/actionsUser';
import { GoogleSignin } from 'react-native-google-signin';
import { LoginManager as FBLoginManager, } from 'react-native-fbsdk';
import NavigationService from './navigationService';
import { initPushNotifications } from './pushNotifications';
import { fetchNotificationsCount } from '../state/actionsNotifications';
import FCM from 'react-native-fcm';

// We need access to the redux store.
let reduxStore;

// NBTODOv2 - upgrade npm
let profilePic;

export const isLoggedIn = () => reduxStore && reduxStore.getState().user.tokens.refreshToken != null;

export const initializeLoggedInUser = () => {
    if (!isLoggedIn()) return;

    initPushNotifications(reduxStore);
    reduxStore.dispatch(fetchNotificationsCount());
}

const loginStateListener = async () => {

    if (reduxStore.getState().user.info.profilePic !== profilePic) {
        // Profile pic changed. Rewrite user info to storage.
        try {
            await AsyncStorage.setItem('user.info', JSON.stringify(reduxStore.getState().user.info));
            profilePic = reduxStore.getState().user.info.profilePic;
        }
        catch (error) {
            // Allow failure - we don't want to break the app for this. Just issue a log message.
            reportError('Failed to save user info after info update: ', error);
        }
    }
}

export function initAuth(store) {
    reduxStore = store;

    // Now subscribe for any change to login state.
    reduxStore.subscribe(loginStateListener);
}

// NBTODO: Think of better splitting into files maybe. 
//         Problem is I need the store in order to logout -
//         logout API requires refreshToken.
//         So here I already have the store in this module and piggybacking.
//         But it would be nice to see the actions separated from the secureApi

// Some auth related actions
export const LOGIN = 'LOGIN';
export const LOGOUT = 'LOGOUT';

function login(user, tokens) {
    return {
        type: LOGIN,
        tokens,
        user,
    }
}

function logout() {
    return { type: LOGOUT }
}

export function doLogin(user, tokens, pictureData = null) {
    return async function (dispatch) {
        try {
            await AsyncStorage.setItem('user.tokens', JSON.stringify(tokens));
            await AsyncStorage.setItem('user.info', JSON.stringify(user));

            // Request permissions before logging in. Ideally location permissions would also
            // be requested here but I couldn't find a way to request them on iOS so they 
            // will be requested after login when we first ask for a location.
            try {
                await FCM.requestPermissions();
            }
            catch(error) {
                console.warn("Notification permission not granted", error);
            }
            dispatch(login(user, tokens));
            NavigationService.navigate('App');

            initializeLoggedInUser();


            // If we got picture data - set it. This is for account creation - upload the picture
            // after the user submitted the registration data.
            if (pictureData) {
                dispatch(setProfilePicture(pictureData));
            }
        }
        catch (error) {
            reportError('Failed to perform login.', error);
            dispatch(doLogout());
        }
    }
}

export function doLogout() {
    return async function (dispatch, getState) {
        let refreshToken = getState().user.tokens.refreshToken;

        const isGoogleAccount = (getState().user.info.googleUser != null);
        const isFacebookAccount = (getState().user.info.facebookUser != null);
        // Whether things went wrong or not, we're dispatching a logout event to clear tokens from the store.
        dispatch(logout());
        NavigationService.navigate('Login');

        try {
            await AsyncStorage.multiRemove(['user.tokens', 'user.info', 'posts.filter']);
        }
        catch (error) {
            reportError('failed to remove tokens from storage during logout');
        }

        try {
            if (isGoogleAccount) {
                await GoogleSignin.signOut();
            }

            if (isFacebookAccount) {
                await FBLoginManager.logOut();
            }
        }
        catch (error) {
            reportError("Failed to sign out of Google/FB account");
        }

        let url = `${AUTHORIZATION_SERVER_ADDRESS}/api/clear/${AUTHORIZATION_SERVER_API_VERSION}/logout`;
        if (refreshAccessToken) {
            try {
                await fetch(url, {
                    method: 'POST',
                    headers: {
                        'Content-Type': 'application/json',
                    },
                    body: JSON.stringify({
                        refreshToken: refreshToken,
                    })
                })
            }
            catch (error) {
                reportError('failed to submit logout request.');
            }
        }
    }
}

export const REQUEST_RESET_TOKEN = 'REQUEST_RESET_TOKEN';
export function requestRefreshToken(refreshTokenPromise) {
    return {
        type: REQUEST_RESET_TOKEN,
        refreshTokenPromise: refreshTokenPromise,
    }
}

export const RECEIVE_RESET_TOKEN = 'RECEIVE_RESET_TOKEN';
export function receiveRefreshToken(tokens) {
    return {
        type: RECEIVE_RESET_TOKEN,
        tokens: tokens,
    }
}

async function refreshAccessToken() {
    let refreshTokenPromise = reduxStore.getState().user.tokens.refreshTokenPromise;

    if (refreshTokenPromise) {
        try {
            await refreshTokenPromise;
            return refreshTokenPromise; // Success - return the resolved promise.
        }
        catch (error) {
            console.info('promise to refresh failed. Should retry refresh token');
        }
    }

    let url = `${AUTHORIZATION_SERVER_ADDRESS}/api/clear/${AUTHORIZATION_SERVER_API_VERSION}/refreshToken`;

    // Either the promise in the store failed or we didn't have a promise in the first place.
    // Create a new promise to refresh the token. We'll store that promise in the state.
    refreshPromise = fetch(url, {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json',
        },
        body: JSON.stringify({
            refreshToken: reduxStore.getState().user.tokens.refreshToken,
        })
    })
        .then((response) => {
            if (!response.ok) {
                throw (new Error('RefreshResponseFailed'))
            }
            return response.json()
        })
        .then((res) => {
            reduxStore.dispatch(receiveRefreshToken(res.tokens))

            return AsyncStorage.setItem('user.tokens', JSON.stringify(res.tokens));
        })
        .catch((error) => {
            reportError('Something went wrong refreshing the token. Logging out.', error);
            reduxStore.dispatch(doLogout());
        });

    reduxStore.dispatch(requestRefreshToken(refreshPromise))
    return refreshPromise;
}

export async function secureFetch(url, options) {
    // Not catching exceptions. Exceptions should be caught at the caller's level.
    let accessToken = reduxStore.getState().user.tokens.accessToken;

    options.headers = options.headers || {};
    options.headers['Authorization'] = `Bearer ${accessToken}`;

    let response = await fetch(url, options);

    //  NBTODO: Be more specific and only renew token if the problem was token expiration.
    if (response.status === 401) {

        // If there's a new access token it means that another request must have already refreshed
        // the token and we don't need to do it again. So we only refresh if the access token is
        // the one we failed with.
        if (accessToken === reduxStore.getState().user.tokens.accessToken) {
            await refreshAccessToken();
        }

        accessToken = reduxStore.getState().user.tokens.accessToken;

        options.headers['Authorization'] = `Bearer ${accessToken}`;
        return fetch(url, options);
    }

    return response;
}

export async function setupGoogleSignIn() {

    try {
        const googleSignInConfig = {
            ios: {
                iosClientId: "979794164103-ve5gsdgcdhav7rdpjbjsue7p3bmv0b7h.apps.googleusercontent.com",
            },
            android: {
                webClientId: "979794164103-l7r6hj0psal58al1ca47cnhmf4lclcqa.apps.googleusercontent.com",
            }
        }
        GoogleSignin.configure({
            ...Platform.select(googleSignInConfig),
            offlineAccess: false
        })
        console.info("Configured Google Sign In.");
    }
    catch (error) {
        console.error("Failed to configure Google sign-in", error);
    }

    // NBTODO: Do I need to get current user every time and verify with the server? (login, basically?)
    // try {
    //     const googleUser = await GoogleSignin.currentUserAsync();
    //     if (googleUser != null) {
    //         reduxStore.dispatch(setGoogleUser(googleUser));
    //     }
    // }
    // catch(error) {
    //     console.error("Failed to configure Google sign-in", error);
    // }
}