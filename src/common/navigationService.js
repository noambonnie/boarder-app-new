import { NavigationActions, StackActions } from 'react-navigation';

let navigator;

function setTopLevelNavigator(navigatorRef) {
    navigator = navigatorRef;
}

function navigate(routeName, params) {
    navigator && navigator.dispatch(
        NavigationActions.navigate({
            routeName,
            params,
        })
    );
}

function push(routeName, params) {
    navigator && navigator.dispatch(
        StackActions.push({
            routeName,
            params,
        })
    );
}

function dispatch(...args) {
    navigator.dispatch(...args);
}

export default {
    navigate,
    setTopLevelNavigator,
    dispatch,
    push,
};