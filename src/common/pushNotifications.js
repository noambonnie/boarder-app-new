import FCM, { FCMEvent, RemoteNotificationResult, WillPresentNotificationResult, NotificationType } from 'react-native-fcm';
import { reportError } from './errorReporter';
import Strings from '../assets/strings';
import { updateNotificationToken, } from '../state/actionsApp';
import { fetchNotificationsCount } from '../state/actionsNotifications';
import * as NotificationsActions from '../state/actionsNotifications';
import { Platform } from 'react-native';
import NavigationService from './navigationService';
import { isLoggedIn } from './authApi';
import StyleConstants from '../styles/constants'

let reduxStore;
export const initPushNotifications = async (store) => {
    reduxStore = store;
    try {
        await FCM.createNotificationChannel({
            id: 'com.boarder.app.COMMENTS',
            name: 'Boarder Comments',
            description: 'Notifiations for new comments on Boarder',
            priority: 'high'
        })

        const token = await FCM.getFCMToken();
        token && reduxStore.dispatch(updateNotificationToken(token));

        const notification = await FCM.getInitialNotification();
        if (notification)
            notificationHandler(notification);
    }
    catch (error) {
        console.warn('Failed to pull notification token. Maybe no permissions or running on Simulator?', error);
    }

    // NBTODOv2 I got a warning at some point that token changed but no handlers were registered. Not sure how to reproduce.
    //          Also not sure how big a problem it is because I dispatch the token on app login.
    FCM.on(FCMEvent.RefreshToken, (token) => {
        // NBTODO: From library documentation:
        // fcm token may not be available on first load, catch it here
        // I'm not sure when this would happen or why, or what's the recommended way to handle this.
        // Keeping track for now...
        reduxStore && reduxStore.dispatch(updateNotificationToken(token));
    });

}

const openedFromTray = (opened_from_tray) => {
    // From the documentation: 
    // How do I tell if user clicks the notification banner?
    // Check open from tray flag in notification. It will be either 0 or 1 for iOS 
    // and undefined or 1 for android. 

    // Anything that's not undefined, false or zero will be true...
    return !(!opened_from_tray || opened_from_tray === 0);
}

const notificationHandler = async (notif) => {
    console.info("Received new notification");
    if ((notif.local_notification) && openedFromTray(notif.opened_from_tray)) {
        const notificationData = notif.boarderData && JSON.parse(notif.boarderData) || {};

        switch (notif.click_action) {
            case 'postComment':
                NavigationService.push("PostView", { postId: notificationData.post._id })
                break;
            case 'notifications': 
                NavigationService.navigate("Notifications");
                break;
        }
    }

    // If this a remote notification that has our data, create a local notification based on that data.
    if (!notif.local_notification && !openedFromTray(notif.opened_from_tray) && notif.boarderData) {

        // Refresh the notifications tab and the notifications list when we got a new notification.
        reduxStore && reduxStore.dispatch(NotificationsActions.fetchNotificationsCount());

        try {
            // If the app is active, notification badge will be update by the drawer.
            // On Android the reduxStore won't be set if the app was not running and started in order
            // to process the notification.
            (!reduxStore || reduxStore.getState().app.state !== 'active') && FCM.setBadgeNumber((await FCM.getBadgeNumber()) + 1);
            reduxStore && reduxStore.dispatch(fetchNotificationsCount());

        }
        catch (error) {
            reportError('Failed to set badge number in notification: ', error)
        }

        const notificationData = JSON.parse(notif.boarderData);
        switch (notificationData.type) {
            case 'postComment':

                FCM.presentLocalNotification({
                    channel: 'com.boarder.app.COMMENTS',
                    title: notificationData.post.title,
                    body: `${notificationData.commenter.firstName}: ${notificationData.commentText}`, 
                    big_text: `${notificationData.commenter.firstName}: ${notificationData.commentText}`,                    // as FCM payload (required)
                    large_icon: "ic_launcher",
                    icon: "icon_comment",
                    priority: "high",
                    sound: "default",
                    group: "comments",
                    color: StyleConstants.BRAND_COLOR_DARK,
                    click_action: "postComment",                             // as FCM payload
                    boarderData: notif.boarderData,                     // extra data you want to throw
                    wake_screen: false,                                  // Android only, wake up screen when notification arrives
                    show_in_foreground: true,
                    lights: true,                            // notification when app is in foreground (local & remote)
                });

                // Summary notifications for Android
                Platform.OS === 'android' &&
                FCM.presentLocalNotification({
                    id: "comments",                               // (optional for instant notification)
                    channel: 'com.boarder.app.COMMENTS',
                    title: Strings.t("notifications.newCommentsTitle"),
                    body: Strings.t("notifications.newCommentsBody"),
                    group: "comments",
                    large_icon: "ic_launcher",
                    icon: "icon_comment",
                    click_action: "notifications",                             // as FCM payload
                    groupSummary: true,
                    groupAlertBehavior: "children",
                    color: StyleConstants.BRAND_COLOR_DARK,
                    show_in_foreground: true,                            // notification when app is in foreground (local & remote)
                    wake_screen: false,                                  // Android only, wake up screen when notification arrives
                    lights: true,                                       // Android only, LED blinking (default false)
                });
    
                break;
            default:
                console.warn('Unknown notification type')
        }
    }

    return;
    // NBTODO: I should probably remove this. As the comment says, this is done for me by the library.
    //         But for now I do it explicitly because I had some issues with iOS and want to make sure
    //         this is not an issue.
    if (Platform.OS === 'ios') {
        //optional
        //iOS requires developers to call completionHandler to end notification process. If you do not call it your background remote notifications could be throttled, to read more about it see https://developer.apple.com/documentation/uikit/uiapplicationdelegate/1623013-application.
        //This library handles it for you automatically with default behavior (for remote notification, finish with NoData; for WillPresent, finish depend on "show_in_foreground"). However if you want to return different result, follow the following code to override
        //notif._notificationType is available for iOS platfrom
        switch (notif._notificationType) {
            case NotificationType.Remote:
                notif.finish(RemoteNotificationResult.NewData) //other types available: RemoteNotificationResult.NewData, RemoteNotificationResult.ResultFailed
                break;
            case NotificationType.NotificationResponse:
                notif.finish();
                break;
            case NotificationType.WillPresent:
                notif.finish(WillPresentNotificationResult.All) //other types available: WillPresentNotificationResult.None
                break;
        }
    }
};

export const registerNotificationHandler = () => {
    FCM.on(FCMEvent.Notification, notificationHandler);
}