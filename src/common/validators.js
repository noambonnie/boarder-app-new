import Strings from '../assets/strings';

export const validateUserDetails = async (userDetails) => {
    if (!userDetails.firstName.trim()) {
        return { errorMessage: Strings.t("fieldRequired", { field: Strings.t("firstNameField") }) };
    }

    if (!userDetails.lastName.trim()) {
        return { errorMessage: Strings.t("fieldRequired", { field: Strings.t("lastNameField") }) };
    }

    if (!userDetails.profilePic) {
        return { errorMessage: Strings.t("fieldRequired", { field: Strings.t("profilePicField") }) };
    }

    return null;
}
