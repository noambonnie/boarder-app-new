import 'abortcontroller-polyfill'; // NBTODO: Using a polyfill until this is supported by RN
import I18n from 'react-native-i18n';

const GOOGLE_MAPS_API_KEY = "AIzaSyD19kt7oAXO0jp8bI_OBjZcqiUG6qqkh8s";

export const MIN_SEARCH_LENGTH = 2;
export function getSearchLanguage() {
    // Currently support only Hebrew and English. The API expects 'iw' for language but locale may be 'he'.
    return['he', 'iw'].indexOf(I18n.currentLocale().substr(0,2).toLowerCase()) >= 0? 'iw' : 'en';
}

export default class GooglePlacesSearch {

    constructor() {
        this.abortController = new AbortController();
    }

    makeRequest = async (url) => {
        // Abort previous request if one exists.
        this.abortController.abort();
        this.abortController = new AbortController();
        try {
            const response = await fetch(
                url,
                {
                    method: 'GET',
                    signal: this.abortController.signal,
                }
            )

            if (!response.ok) throw ("Something went wrong");
            const jsonResponse = await response.json();
            return jsonResponse;
        }
        catch (error) {
            if (error.name === "AbortError") {
                return null;
            }
            console.error('Location Search error', error);
        }
        return null; // Should only happen in case of error.
    }

    search = async (query) => {
        if (query.length < MIN_SEARCH_LENGTH) return []; // Don't bother...

        const predictionsResponse = await this.makeRequest(`https://maps.googleapis.com/maps/api/place/autocomplete/json?&input=${query}&key=${GOOGLE_MAPS_API_KEY}&language=${getSearchLanguage()}`);
        if (!predictionsResponse) {
            return [];
        }

        const predictionData = predictionsResponse.predictions.map((item) => ({ placeId: item.place_id, description: item.description })) || [];
        return predictionData;
    }

    getPlaceDetails = async (placeId) => {
        return await this.makeRequest(`https://maps.googleapis.com/maps/api/place/details/json?&placeid=${placeId}&key=${GOOGLE_MAPS_API_KEY}&language=${getSearchLanguage()}`);
    }
}

