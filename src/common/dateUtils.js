import Strings from '../assets/strings';

export const timeSince = (date) => {
    let seconds = Math.floor((new Date() - date) / 1000);
    let interval = Math.floor(seconds / 31536000);

    if (interval >= 1) {
        return Strings.t("yearsAgo.counting", { count: interval })
    }

    interval = Math.floor(seconds / 2592000);
    if (interval >= 1) {
        return Strings.t("monthsAgo.counting", { count: interval })
    }

    interval = Math.floor(seconds / 86400);
    if (interval >= 1) {
        return Strings.t("daysAgo.counting", { count: interval })
    }

    interval = Math.floor(seconds / 3600);
    if (interval >= 1) {
        return Strings.t("hoursAgo.counting", { count: interval })
    }

    interval = Math.floor(seconds / 60);
    if (interval >= 1) {
        return Strings.t("minutesAgo.counting", { count: interval })
    }

    return Strings.t("secondsAgo")
}
