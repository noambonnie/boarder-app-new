import i18n from 'react-native-i18n';

i18n.fallbacks = true;

// NBTODO: Arrange things into sub catrgories. For example:
// en:
//      inbox:
//          counting:
//              one: You have 1 new message
//              other: You have {{count}} new messages
//              zero: You have no messages
i18n.translations = {
    'en': {
        appName: "Boarder",
        loginButton: "LOGIN",
        loginFailedMessage: "Invalid email/password combination. Please try again.",
        somethingWentWrongAlertTitle: "Ooops...",
        somethingWentWrongAlert: "Something went wrong. Please try again later.",
        loadPostsFailed: "We seem to be having a problem loading new messages. Please try again later.",
        loadPostsNoLocation: "Waiting for location to get messages near you. We'll refresh this automatically once we know where you are.",
        noPostsToShow: "There doesn't seem to be any message in this area. Be the first to post a message here!\n\nYou can also expand the search range or view messages in other places by tapping the search bar above.",
        loadCommentsFailed: "Failed to load comments. Please try again later.",
        loginWelcome: "Travelers community. Here and now.",
        emailField: "E-mail",
        passwordField: "Password",
        confirmPasswordField: "Confirm password",
        permissionNeededToProceed: "Please grant the required permissions to proceed.",
        send: "Send",
        addPost: {
            writeCommentPlaceholder: "Write a comment...",
            newPostTitlePlaceholder: "Message title...",
            postMessageButton: "Post",
            addPostTitle: "New Message",
            postMessageLocation: "Where should we post your message?",
            newPostTextPlaceholder: "Write your message",
            addPhotos: "Add Photos",
            tooManyPhotosTitle: "Too many photos",
            tooManyPhotosText: "Please upload up to {{count}} photos",
            photoUploaded: "Uploaded",
            photoUploadFailed: "Failed",
            uploadingImages: "Uploading photos...",
            postingMessage: "Posting message..."
        },
        passwordsDontMatch: "Passwords don't match. Please try again.",
        fieldRequired: "{{field}} is required",
        invalidEmail: "Invalid email address",
        passwordTooShort: "Password too short. Please use minimum 8 characters.",
        nextButton: "NEXT",
        doneButton: "DONE",
        accountDetailsTitle: "Account details",
        emailAlreadyUsed: "This e-mail address is already used by another user.",
        userDetailsTitle: "Tell others who you are",
        firstNameField: "First name",
        lastNameField: "Last name",
        profilePictureField: "Profile Picture:",
        profilePicField: "Profile picture",
        iAccept: "I accept the ",
        termsAndConditions: "terms and conditions",
        termsNotAccepted: "Please accept the terms and conditions",
        goToLogin: "Already a user? Login.",
        goToSignUp: "Not a user? Sign up.",
        loggingIn: "Logging in...",
        anonymous: "Anonymous",
        nearMe: "Current Location",
        nearMeSearchResult: "View Messages in Current Location",
        whereTo: "Search location",
        unavailable: "(Unavailable)",
        comments: {
            counting: {
                one: "1 comment",
                other: "{{count}} comments",
                zero: "No comments",
            }
        },
        drawer: {
            logout: "Logout",
            feedback: "Feedback and Support",
        },
        "distance": "{{distance}} {{unit}}",
        loadNotificationsFailed: "We seem to be having a problem loading notifications. Please try again later.",
        noNotificationsToShow: "No new notifications.",
        newComment: "\"{{title}}\" ",
        hasNewComments: {
            counting: {
                one: "has a new comment.",
                other: "has {{count}} new comments.",
            }
        },
        editProfile: "Edit Profile",
        profilePicDialogTitle: "Profile Picture",
        profilePicDialogText: "Where would you like to upload from?",
        fromCamera: "Take Photo",
        fromGallery: "From Library",
        "ok": "OK",
        "save": "Save",
        "done": "Apply",
        "search": {
            "filterDistanceCurrentSelection": "{{distance}} {{units}}",
            "filterDistance": "Range:",
            "noResults": "No results",
            "startTyping": "Start typing...",
        },
        "locationTooBroadTitle": "Location too broad",
        "locationTooBroadText": "The location you selected is too broad. Try a more specific location such as city or point of interest. For example, 'Bangkok', 'Museum of Modern Art'.\n\nSometimes states and cities have the same name. For example, New York or Chiang Mai. Make sure you select the city.",
        "errorAlertTitle": "Error",
        "saving": "Saving...",
        "yearsAgo": {
            counting: {
                one: "A year ago",
                other: "{{count}} years ago",
            }
        },
        "monthsAgo": {
            counting: {
                one: "A month ago",
                other: "{{count}} months ago",
            }
        },
        "daysAgo": {
            counting: {
                one: "Yesterday",
                other: "{{count}} days ago",
            }
        },
        "hoursAgo": {
            counting: {
                one: "An hour ago",
                other: "{{count}} hours ago",
            }
        },
        "minutesAgo": {
            counting: {
                one: "A minute ago",
                other: "{{count}} minutes ago",
            }
        },
        "secondsAgo": "A few seconds ago",
        "notifications": {
            "notifcationsScreenHeader": "Notifications",
            "newCommentsTitle": "New Comments",
            "newCommentsBody": "There are new comments on messages you interacted with",
        },
        "blockUser": "Block",
        "reportPost": "Report this message",
        "facebookRequiredPermissions": "We need permission to access your email address.",
        "postViewScreenTitle": "Message",
        "profileViewScreenTitle": "Profile",
        "tagline1": "Travelers' community.",
        "tagline2": "Here and Now.",
        "km": "km",
        "coachMarks": {
            "posts": {
                "searchBox": "Use the search box at the top of the screen to view messages in other places and expand search range.",
                "addPost": "Use this button to post your own message.",
                "dismiss": "Got it"
            }
        },
        "photoViewerScreenTitle": "Photos"
    },
    'he': {
        appName: "Boarder",
        loginButton: "התחבר",
        loginFailedMessage: "משתמש/סיסמא שגויים. נסו שוב",
        somethingWentWrongAlertTitle: "אופס...",
        somethingWentWrongAlert: "משהו השתבש. נסו שנית מאוחר יותר.",
        loadPostsFailed: "נראה שיש בעיה בטעינת מודעות חדשות. נסו שנית מאוחר יותר.",
        loadPostsNoLocation: "ממתין למיקום כדי להציג מודעות סביבך. המסך ירוענן אוטומטית כשנמצא את מיקומך.",
        noPostsToShow: "נראה שאין מודעות חדשות באיזור זה. היו הראשונים לפרסם כאן מודעה!\n\nניתן להרחיב את טווח החיפוש או לראות מודעות במקומות אחרים ע״י לחיצה על תיבת החיפוש למעלה.",
        loadCommentsFailed: "שגיאה בטעינת תגובות. נסו שנית מאוחר יותר.",
        loginWelcome: "לוח מודעות למטיילים. כאן ועכשיו.",
        emailField: "דוא״ל",
        passwordField: "סיסמא",
        confirmPasswordField: "אישור סיסמא",
        permissionNeededToProceed: "אנא העניקו הרשאות כדי להמשיך.",
        send: "שלח",
        addPost: {
            writeCommentPlaceholder: "כתבו תגובה...",
            newPostTitlePlaceholder: "כותרת המודעה...",
            postMessageButton: "שלח",
            addPostTitle: "מודעה חדשה",
            postMessageLocation: "איפה לפרסם את המודעה?",
            newPostTextPlaceholder: "תוכן המודעה",
            addPhotos: "הוספת תמונות",
            tooManyPhotosTitle: "יותר מדי תמונות",
            tooManyPhotosText: "ניתן להעלות עד {{count}} תמונות.",
            photoUploaded: "הועלה",
            photoUploadFailed: "תקלה",
            uploadingImages: "מעלה תמונות...",
            postingMessage: "מפרסם את המודעה..."

        },
        passwordsDontMatch: "הסיסמאות אינן תואמות. נסו שנית.",
        fieldRequired: "יש להזין {{field}}",
        invalidEmail: "כתובת דוא״ל שגוייה",
        passwordTooShort: "סיסמא קצרה מדי. אנא השתמשו בשמונה תווים או יותר.",
        nextButton: "הבא",
        doneButton: "התחבר",
        accountDetailsTitle: "פרטי משתמש",
        emailAlreadyUsed: "כתובת דוא״ל כבר בשימוש ע״י משתמש אחר.",
        userDetailsTitle: "ספרו לאחרים מי אתם",
        firstNameField: "שם פרטי",
        lastNameField: "שם משפחה",
        profilePictureField: "תמונת פרופיל:",
        profilePicField: "תמונת פרופיל",
        iAccept: "אני מסכים ל",
        termsAndConditions: "תנאי השימוש",
        termsNotAccepted: "נא קבלו את תנאי השימוש",
        goToLogin: "כבר יש לכם חשבון? התחברו.",
        goToSignUp: "אין לכם חשבון? הרשמו.",
        loggingIn: "מתחבר...",
        anonymous: "משתמש לא ידוע",
        nearMe: "מיקום נוכחי",
        nearMeSearchResult: "הצג מודעות במיקום נוכחי",
        whereTo: "חפש מקום",
        unavailable: "(לא זמין)",
        comments: {
            counting: {
                one: "תגובה אחת",
                other: "{{count}} תגובות",
                zero: "אין תגובות",
            }
        },
        drawer: {
            logout: "התנתקות",
            feedback: "תמיכה ורעיונות",
        },
        "distance": "{{distance}} {{unit}}",
        loadNotificationsFailed: "נראה שיש בעיה בטעינת התראות חדשות. נסו שנית מאוחר יותר.",
        noNotificationsToShow: "אין התראות חדשות.",
        newComment: "\"{{title}}\" ",
        hasNewComments: {
            counting: {
                one: "קיבל תגובה חדשה",
                other: "קיבל {{count}} תגובות חדשות",
            }
        },
        editProfile: "עריכת פרופיל",
        profilePicDialogTitle: "תמונת פרופיל",
        profilePicDialogText: "מאיפה תרצו להעלות את התמונה?",
        fromCamera: "צילום תמונה",
        fromGallery: "גלריית התמונות",
        "ok": "אישור",
        "save": "שמור",
        "done": "אישור",
        "search": {
            "filterDistanceCurrentSelection": "{{distance}} {{units}}",
            "filterDistance": "טווח:",
            "noResults": "אין תוצאות",
            "startTyping": "התחילו לכתוב...",
        },
        "locationTooBroadTitle": "מיקום כללי מדי",
        "locationTooBroadText": "המיקום שבחרתם כללי מדי. נסו מיקום ממוקד יותר כמו עיר או נקודות עניין. למשל: 'בנגקוק', 'מוזיאון ישראל'.\n\nלפעמים לעיר ולמדינה יש שם זהה. לדוגמא ניו יורק או צ׳יאנג מאי. בחרו בעיר.",
        "errorAlertTitle": "שגיאה",
        "saving": "שומר...",
        "yearsAgo": {
            counting: {
                one: "לפני שנה",
                other: "לפני {{count}} שנים",
            }
        },
        "monthsAgo": {
            counting: {
                one: "לפני חודש",
                other: "לפני {{count}} חודשים",
            }
        },
        "daysAgo": {
            counting: {
                one: "אתמול",
                other: "לפני {{count}} ימים",
            }
        },
        "hoursAgo": {
            counting: {
                one: "לפני שעה",
                other: "לפני {{count}} שעות",
            }
        },
        "minutesAgo": {
            counting: {
                one: "לפני דקה",
                other: "לפני {{count}} דקות",
            }
        },
        "secondsAgo": "לפני כמה שניות",
        "notifications": {
            "notifcationsScreenHeader": "התראות",
            "newCommentsTitle": "תגובות חדשות",
            "newCommentsBody": "יש תגובות חדשות על מודעות שהשתתפת בהן.",
        },
        "blockUser": "חסום את",
        "reportPost": "דווח על מודעה זו",
        "facebookRequiredPermissions": "דרוש אישור לגישה לכתובת הדוא״ל שלכם.",
        "postViewScreenTitle": "מודעה",
        "profileViewScreenTitle": "פרופיל משתמש",
        "tagline1": "מודעות מטיילים.",
        "tagline2": "כאן ועכשיו.",
        "km": "ק״מ",
        "coachMarks": {
            "posts": {
                "searchBox": "השתמשו בתיבת החיפוש בכותרת כדי להציג מודעות במקומות אחרים וכדי להגדיל את טווח החיפוש.",
                "addPost": "לחצו על הכפתור הזה כדי לפרסם מודעה משלכם.",
                "dismiss": "סבבה"
            }
        },
        "photoViewerScreenTitle": "תמונות"
    },
};

export default i18n; 
