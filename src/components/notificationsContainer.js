import React, { Component } from 'react';
import {
    View,
    FlatList,
    Text,
} from 'react-native';

import { connect } from 'react-redux';

import * as Actions from '../state/actionsNotifications';

import Strings from '../assets/strings';
import NotificationsList from '../controls/notificationsList';
import HeaderAwareScrollView from '../controls/HeaderAwareScrollView';
import BadgedIcon from '../controls/BadgedIcon';
import HeaderAwareRefreshControl from '../controls/HeaderAwareRefreshControl';

import FCM from 'react-native-fcm';

class NotificationsContainer extends Component {
    static navigationOptions = ({ navigation }) => {
        const params = navigation.state.params || {};
        return {
            tabBarIcon: ({ focused }) => (<BadgedIcon source={require("../assets/icon-notifications.png")} showCount={true} focused={focused} />),
        }
    }

    constructor(props) {
        super(props);
        this.state = { notificationClearTimer: '', doesAppear: false }
    }

    clearNotifications = () => {
        this.props.markNotificationsSeen();
        this.props.clearNotificationCount();
    }

    componentWillReceiveProps(nextProps) {
        if (this.props.notificationsCount != nextProps.notificationsCount) {
            // NBTODOv2 - I should clear the badge where I mark notifications as read, not here.
            FCM.setBadgeNumber(nextProps.notificationsCount || 0); // badge value, 0 to remove badge

            // If a new notification comes when user is not viewing the screen we'll refresh the list to get the 
            // new notifications. If this is the first update of the notification count (current prop is null) we won't
            // refresh because this is app start-up.
            if (nextProps.notificationsCount && this.props.notificationsCount != null && !this.props.navigation.isFocused()) {
                this.getNotifications();
            }
        }
    }
    // NBTODO: Consider separating the presentation from the data when reusing this view for saved or filtered list.
    getNotifications(page = 1, firstNotificationDate) {
        if (this.props.loggedIn)
            this.props.fetchNotifications(page, firstNotificationDate);
    }

    // Why like this and not funcName()? See https://reactjs.org/docs/react-without-es6.html
    handleRefresh = () => {
        if (this.props.loggedIn) {
            this.props.refreshNotifications();
            this.getNotifications();
            this.clearNotifications();
        }
    }

    handleLoadMore = () => {
        if (!this.props.resultsAvailable) {
            return;
        }

        this.getNotifications(this.props.page + 1, this.props.firstNotificationDate ? this.props.firstNotificationDate : null);
    }

    goToNotification = (notificationData) => {
        this.props.markNotificationRead(notificationData._id);
        this.clearNotifications();
        switch (notificationData.type) {
            case 'postComment':
                this.props.navigation.push("PostView", { postId: notificationData.referenceObject._id, })
                break;
        }
    }

    getNotificationText = (notificationData) => {
        switch (notificationData.type) {
            case 'postComment':
                return (
                    <Text style={{ fontSize: 16 }}>
                        <Text style={{ fontWeight: 'bold', }}>{Strings.t("newComment", { title: notificationData.referenceObject.title })}</Text>
                        <Text>{Strings.t("hasNewComments.counting", { count: notificationData.notificationCount })}</Text>
                    </Text>
                )
            default:
                return (<View><Text>Unsupported notification</Text></View>);
        }
    }

    componentWillMount = () => {
        this.willBlurSubscription = this.props.navigation.addListener(
            'willBlur',
            payload => {
                clearTimeout(this.notificationClearTimer);
            }
        )

        this.didFocusSubscription = this.props.navigation.addListener(
            'didFocus',
            payload => {
                this.notificationClearTimer = setTimeout(() => {
                    this.clearNotifications()
                }, 2000);
            }
        )

    }

    componentWillUnmount = () => {
        this.didFocusSubscription.remove();
        this.willBlurSubscription.remove();
    }

    componentDidMount() {
        this.getNotifications();
    }

    render() {
        // NBTODOv2 - Why the flat lists? for Pull to Refresh? Can use refreshControl instead
        if (this.props.loadError) {
            return (
                <View style={{ flex: 1, justifyContent: 'flex-start', }}>
                    <HeaderAwareScrollView scrollClass={FlatList}
                        style={{ paddingHorizontal: 10 }}
                        data={[{ key: "0" }]}
                        refreshControl={<HeaderAwareRefreshControl
                            onRefresh={this.handleRefresh}
                            refreshing={this.props.isRefreshing} />}
                        renderItem={({ item }) =>
                            <Text style={{ fontSize: 18, color: 'grey', marginHorizontal: 20, marginVertical: 10, justifyContent: 'center' }}>
                                {Strings.t('loadNotificationsFailed')}
                            </Text>} />
                </View>
            )
        }

        if (!this.props.isLoading && (!this.props.items || this.props.items.length === 0)) {
            return (
                <View style={{ flex: 1, justifyContent: 'flex-start', }}>
                    <HeaderAwareScrollView scrollClass={FlatList}
                        style={{ paddingHorizontal: 10 }}
                        data={[{ key: "0" }]}
                        refreshControl={<HeaderAwareRefreshControl
                            onRefresh={this.handleRefresh}
                            refreshing={this.props.isRefreshing} />}
                        renderItem={({ item }) =>
                            <Text style={{ fontSize: 18, color: 'grey', marginHorizontal: 20, marginVertical: 10, justifyContent: 'center' }}>
                                {Strings.t('noNotificationsToShow')}
                            </Text>} />
                </View>
            )
        }

        return (
            <View style={{ flex: 1, justifyContent: 'flex-start', }}>
                <NotificationsList
                    notificationsList={this.props.items}
                    isRefreshing={this.props.isRefreshing}
                    onRefresh={this.handleRefresh}
                    onEndReached={this.handleLoadMore}
                    isLoading={this.props.isLoading}
                    goToNotification={this.goToNotification}
                    getNotificationText={this.getNotificationText} />
            </View>
        );
    }
}

const mapStateToProps = (state, ownProps) => {
    return {
        loggedIn: (state.user.tokens.refreshToken != null),
        isRefreshing: state.notifications.isRefreshing,
        isLoading: state.notifications.isLoading,
        firstNotificationDate: state.notifications.firstNotificationDate,
        items: state.notifications.items,
        page: state.notifications.page,
        resultsAvailable: state.notifications.resultsAvailable,
        loadError: state.notifications.loadError,
        notificationsCount: state.notifications.count,
    }
}

const mapDispatchToProps = (dispatch, ownProps) => {
    return {
        fetchNotifications: (page, firstNotificationDate) => {
            return dispatch(Actions.fetchNotifications(page, firstNotificationDate));
        },
        refreshNotifications: () => {
            return dispatch(Actions.refreshNotifications());
        },
        markNotificationRead: (notificationId) => {
            return dispatch(Actions.markNotificationRead(notificationId));
        },
        markNotificationsSeen: () => {
            return dispatch(Actions.markNotificationsSeen());
        },
        clearNotificationCount: () => {
            return dispatch(Actions.receiveNotificationsCount(0));
        }
    }
}

const NotificationsContainerClass = connect(mapStateToProps, mapDispatchToProps)(NotificationsContainer);

export default NotificationsContainerClass;