import React, { Component } from 'react';
import {
    Text,
    View,
    Image,
    FlatList,
    TouchableWithoutFeedback,
} from 'react-native';

import { connect } from 'react-redux';

import PostsList from '../controls/postsList';
import * as Actions from '../state/actionsPosts';
import * as AppActions from '../state/actionsApp';

import Strings from '../assets/strings'
import StyleConstants from '../styles/constants';
import PostMenu from '../controls/postMenu';
import { AndroidBackHandler } from 'react-navigation-backhandler';
import SearchResultsOverlay from '../controls/searchResultsOverlay';
import BadgedIcon from '../controls/BadgedIcon';
import HeaderAwareScrollView from '../controls/HeaderAwareScrollView';
import HeaderAwareRefreshControl from '../controls/HeaderAwareRefreshControl';
import PostsCoachMark from '../controls/PostsCoachMark';

class PostsListContainer extends Component {
    static navigationOptions = ({ navigation }) => {
        const params = navigation.state.params || {};
        return {
            tabBarIcon: ({ focused }) => (<BadgedIcon source={require("../assets/icon-posts.png")} focused={focused} />),
            tabBarVisible: params.isSearchFocused == null || !params.isSearchFocused,
        }
    }

    constructor(props) {
        super(props);

        this.state = {
            showPostMenu: false,
            postMenuDetails: {},
            filter: this.props.filter,
        };
    }


    addPost = () => {
        this.props.navigation.push("PostAdd");
    }

    handleBackPress = () => {
        if (this.props.isSearchFocused) {
            this.props.setSearchFocused(false);
            return true;
        }

        return false;
    }
    componentDidUpdate(prevProps) {
        // If we got a location or if we got logged in we'll get posts.
        if (this.props.hasLocation && !prevProps.hasLocation) {
            // Don't call getPosts() because it won't get posts if there's no location. Call the fetch directly.
            this.getPosts();
        }

        if (this.props.isSearchFocused !== prevProps.isSearchFocused) {
            this.props.navigation.setParams({ isSearchFocused: this.props.isSearchFocused });
        }
    }

    getPosts = async (page = 1, firstPostDate) => {
        if (!this.props.hasLocation) {
            return;
        }

        this.props.fetchPosts(page, firstPostDate);
    }

    // Why like this and not funcName()? See https://reactjs.org/docs/react-without-es6.html
    handleRefresh = () => {
        if (this.props.hasLocation) {
            this.props.refreshPosts();
            this.getPosts(1, null);
        }
    }

    handleLoadMore = () => {
        if (!this.props.resultsAvailable) {
            return;
        }

        this.getPosts(this.props.page + 1, this.props.firstPostDate ? this.props.firstPostDate : null);
    }

    showPostDetails = (postId, title, text, profilePic) => {
        this.props.navigation.push("PostView", { postId, })
    }

    componentDidMount() {
        // NBTODOv2 I think all references to loggedIn can be removed - We won't get to this screen if not logged in.
        this.getPosts();
    }

    showPostMenu = async (postDetails) => {
        this.setState({
            showPostMenu: true,
            postMenuDetails: postDetails,
        });
    }

    hidePostMenu = async () => {
        this.setState({
            showPostMenu: false,
        })
    }

    renderBody = () => {
        // NBTODO: What use flatlists here? for the pullToRefresh?
        if (!this.props.hasLocation) {
            return (
                <HeaderAwareScrollView scrollClass={FlatList}
                    style={{ paddingHorizontal: 10 }}
                    data={[{ key: "0" }]}
                    refreshControl={<HeaderAwareRefreshControl
                        onRefresh={this.handleRefresh}
                        refreshing={this.props.isRefreshing} />}
                    refreshing={this.props.isRefreshing}
                    renderItem={({ item }) =>
                        <Text style={{ fontSize: 18, color: 'grey', marginHorizontal: 20, marginVertical: 10, justifyContent: 'center' }}>
                            {Strings.t('loadPostsNoLocation')}
                        </Text>} />
            )
        }
        if (this.props.loadError) {
            return (
                <HeaderAwareScrollView scrollClass={FlatList}
                    style={{ paddingHorizontal: 10 }}
                    data={[{ key: "0" }]}
                    refreshControl={<HeaderAwareRefreshControl
                        onRefresh={this.handleRefresh}
                        refreshing={this.props.isRefreshing} />}
                    renderItem={({ item }) =>
                        <Text style={{ fontSize: 18, color: 'grey', marginHorizontal: 20, marginVertical: 10, justifyContent: 'center' }}>
                            {Strings.t('loadPostsFailed')}
                        </Text>} />
            )
        }

        if (!this.props.isLoading && (!this.props.items || this.props.items.length === 0)) {
            return (
                <HeaderAwareScrollView scrollClass={FlatList}
                    style={{ paddingHorizontal: 10 }}
                    data={[{ key: "0" }]}
                    refreshControl={<HeaderAwareRefreshControl
                        onRefresh={this.handleRefresh}
                        refreshing={this.props.isRefreshing} />}
                    renderItem={({ item }) =>
                        <Text style={{ fontSize: 18, color: 'grey', marginHorizontal: 20, marginVertical: 10, justifyContent: 'center' }}>
                            {Strings.t('noPostsToShow')}
                        </Text>} />
            )
        }

        return (
            <PostsList
                postsList={this.props.items}
                isRefreshing={this.props.isRefreshing}
                onRefresh={this.handleRefresh}
                onEndReached={this.handleLoadMore}
                isLoading={this.props.isLoading}
                showPostDetails={this.showPostDetails}
                showPostMenu={this.showPostMenu}
                serverTime={this.props.serverTime}
            />
        );
    }

    renderPostMenu = () => {
        return this.state.showPostMenu && (
            <PostMenu postDetails={this.state.postMenuDetails} done={this.hidePostMenu} />
        )
    }

    renderCoachmarks = () => {
        return this.props.showPostsCoachMarks && (
            <PostsCoachMark dismissCoachMarks={() => this.props.setShowPostsCoachMarks(false)}/>
        )
    }
    
    renderSearchResults = () => {
        return (
            <SearchResultsOverlay
                currentFilter={this.props.filter}
                isSearchFocused={this.props.isSearchFocused}
                searchQuery={this.props.searchQuery}
                predictions={this.props.predictions}
                setFeedLocation={this.props.setFeedLocation}
                setFeedDistance={this.props.setFeedDistance}
            />
        )
    }

    render() {
        return (
            <AndroidBackHandler onBackPress={this.handleBackPress}>
                <View style={{ flex: 1, justifyContent: 'flex-start', }}>
                    {this.renderBody()}
                    {
                        !this.props.isSearchFocused && (
                            <TouchableWithoutFeedback onPress={this.addPost}>
                                <View style={{
                                    position: 'absolute',
                                    width: 56,
                                    height: 56,
                                    alignItems: 'center',
                                    justifyContent: 'center',
                                    right: 16,
                                    bottom: 30,
                                    backgroundColor: StyleConstants.BRAND_COLOR_DARK,
                                    borderRadius: 28,
                                    elevation: 5,
                                    shadowOpacity: 0.3,
                                    shadowOffset: {
                                        width: 2,
                                        height: 2,
                                    }
                                }}>
                                    <Image source={require('../assets/icon-add-post.png')} style={{ width: 18, height: 18, tintColor: "white"}} />
                                </View>
                            </TouchableWithoutFeedback>
                        )
                    }
                    {this.renderSearchResults()}
                    {this.renderPostMenu()}
                    {this.renderCoachmarks()}
                </View>
            </AndroidBackHandler>
        );
    }
}

const mapStateToProps = (state, ownProps) => {
    return {
        loggedIn: (state.user.tokens.refreshToken != null),
        isRefreshing: state.posts.isRefreshing,
        isLoading: state.posts.isLoading,
        firstPostDate: state.posts.firstPostDate,
        items: state.posts.items,
        serverTime: state.posts.serverTime ? state.posts.serverTime : Date.now(),
        page: state.posts.page,
        resultsAvailable: state.posts.resultsAvailable,
        loadError: state.posts.loadError,
        hasLocation: state.location && state.location.position != null || state.posts.filter.byDistance.address,
        filter: state.posts.filter,
        isSearchFocused: state.posts.locationSearch.isSearchFocused,
        predictions: state.posts.locationSearch.predictions,
        searchQuery: state.posts.locationSearch.searchQuery,
        showPostsCoachMarks: state.app.showPostsCoachMarks,
    }
}

const mapDispatchToProps = (dispatch, ownProps) => {
    return {
        fetchPosts: (page, firstPostDate) => {
            return dispatch(Actions.fetchPosts(page, firstPostDate));
        },
        refreshPosts: () => {
            return dispatch(Actions.refreshPosts());
        },
        setFilter: (filter) => {
            return dispatch(Actions.setFeedFilter(filter))
        },
        applyFilter: (filter) => {
            return dispatch(Actions.applyFilter(filter));
        },
        setSearchFocused: (isFocused) => {
            return dispatch(Actions.setSearchFocused(isFocused));
        },
        setFeedLocation: (placeId) => {
            return dispatch(Actions.setFeedLocation(placeId));
        },
        setFeedDistance: (distance) => {
            return dispatch(Actions.setFeedDistance(distance));
        },
        setShowPostsCoachMarks: (showPostsCoachMarks) => {
            return dispatch(AppActions.setShowPostsCoachMarks(showPostsCoachMarks))
        }
    }
}

const PostsListContainerClass = connect(mapStateToProps, mapDispatchToProps)(PostsListContainer);

export default PostsListContainerClass;