import React, { Component } from 'react';
import {
    Alert,
    ActivityIndicator,
    ScrollView,
} from 'react-native';

import { connect } from 'react-redux';
import ProfileEdit from '../controls/profileEdit';
import * as Photos from '../common/photos';
import Strings from '../assets/strings';
import * as Actions from '../state/actionsEditProfile';
import * as UserActions from '../state/actionsUser';
import { validateUserDetails } from '../common/validators';
import WaitingOverlay from '../controls/waitingOverlay';
import HeaderActionButton from '../controls/HeaderActionButton';
import StyleConstants from '../styles/constants';

class ProfileEditContainer extends Component {

    static navigationOptions = ({ navigation }) => {
        const params = navigation.state.params || {};
        const { disabled = false, saveProfile } = params;

        return {
            headerRight: (
                <HeaderActionButton
                    disabled={disabled}
                    onPress={saveProfile}
                    label={Strings.t("save")}
                />
            ),
        };
    }

    constructor(props) {
        super(props);
        this.state = {
            profilePic: null,
            mime: null,
            firstName: null,
            lastName: null,
            detailsSaved: false,
            pictureSaved: false,
        }
    }

    // This will be used by profileEdit (the control) to set the values of the currently typed
    // text of various fields. This is weird, but is needed because the button's "onPress" for SAVE
    // is done in this class by the navigator. Perhaps this is not the best navigation library...
    setPropText = (propName, text) => {
        const newState = {};
        newState[propName] = text;
        this.setState(newState);
    }

    saveProfile = async () => {
        // A way to disable the navigation button...
        if (this.props.isSaving)
            return;

        this.setState(
            (prevState, props) => {
                return { detailsSaved: false, pictureSaved: prevState.profilePic == null || false }
            },
            async () => {
                const validationError = await validateUserDetails({
                    firstName: this.state.firstName,
                    lastName: this.state.lastName,
                    profilePic: this.state.profilePic || this.props.profilePicUri,
                });

                if (validationError != null) {
                    return Alert.alert(Strings.t("errorAlertTitle"), validationError.errorMessage);
                }

                // New profile pic was set. Upload it.
                if (this.state.profilePic != null) {
                    this.props.setProfilePic({ filename: this.state.profilePic, mime: this.state.mime })
                }

                const { firstName, lastName } = this.state;
                this.props.setOwnUserDetails({ firstName, lastName });
            })
    }

    componentWillReceiveProps(nextProps) {
        if (nextProps.saveError || nextProps.profilePicSetError) {
            return Alert.alert(Strings.t("somethingWentWrongAlertTitle"), Strings.t("somethingWentWrongAlert"));
        }

        if (this.props.isSaving && !nextProps.isSaving && !nextProps.saveError) {
            this.setState({ detailsSaved: true })
        }

        if (this.props.isSavingProfilePic && !nextProps.isSavingProfilePic && !nextProps.profilePicSetError) {
            // Saved successfully. Dismiss this screen.
            this.setState({ pictureSaved: true })
        }
    }

    getProfilePic = async () => {
        Photos.getProfilePic((image) => {
            this.setState({ ...image });
        });
    }

    componentDidUpdate() {
        if (this.state.detailsSaved && this.state.pictureSaved) {
            this.setState({ detailsSaved: false, pictureSaved: false },
                () => this.props.navigation.goBack());
        }
    }

    componentWillMount() {
        this.props.navigation.setParams({ saveProfile: this.saveProfile })
    }
    
    componentWillUnmount() {
        this.props.resetEditProfile();
    }

    componentDidMount() {
        this.props.fetchOwnUserDetails();
    }

    render() {
        if (this.props.isLoading) {
            return (
                <View
                    style={{
                        paddingVertical: 20,
                        borderTopWidth: 1,
                        borderColor: "#DEE0DE"
                    }}>
                    <ActivityIndicator size="large" color={StyleConstants.BRAND_COLOR_DARK}/>
                </View>
            )
        }

        return (
            <ScrollView style={{ flex: 1 }} keyboardShouldPersistTaps='always'>
                <ProfileEdit
                    profilePic={this.state.profilePic ? { uri: this.state.profilePic } : this.props.profilePic}
                    firstName={this.props.firstName}
                    lastName={this.props.lastName}
                    getProfilePic={this.getProfilePic}
                    setInputText={this.setPropText}
                />
                {(this.props.isSaving || this.props.isSavingProfilePic)
                    && (<WaitingOverlay text={Strings.t("saving")} />)}
            </ScrollView>
        );
    }
}

const mapStateToProps = (state, ownProps) => {
    return {
        profilePic: state.user.info.profilePic ? { uri: state.user.info.profilePic } : require('../assets/icon-user.png'),
        profilePicUri: state.user.info.profilePic,
        isLoading: state.editProfile.isLoading,
        loadError: state.editProfile.loadError,
        isSaving: state.editProfile.isSaving,
        saveError: state.editProfile.saveError,
        isSavingProfilePic: state.user.isSavingProfilePic,
        profilePicSetError: state.user.profilePicSetError,
        firstName: state.user.info.firstName,
        lastName: state.user.info.lastName,
    }
}

const mapDispatchToProps = (dispatch, ownProps) => {
    return {
        fetchOwnUserDetails: (userId) => {
            return dispatch(UserActions.fetchOwnUserDetails());
        },
        setOwnUserDetails: (userDetails, pictureData) => {
            return dispatch(Actions.setOwnUserDetails(userDetails, pictureData));
        },
        setProfilePic: (pictureData) => {
            return dispatch(UserActions.setProfilePicture(pictureData));
        },
        resetEditProfile: () => {
            dispatch(UserActions.receiveOwnUserDetails({}));
            dispatch(UserActions.resetSetProfilePicture());
            dispatch(Actions.resetSetOwnUserDetails());
        },
    }
}

const ProfileEditContainerClass = connect(mapStateToProps, mapDispatchToProps)(ProfileEditContainer);

export default ProfileEditContainerClass;