import React, { Component } from 'react';
import {
    Alert,
    Linking,
} from 'react-native';

import { connect } from 'react-redux';

import PostView from '../controls/postView';
import * as ActionsComments from '../state/actionsComments';
import * as ActionsPostDetails from '../state/actionsPostDetails';
import Strings from '../assets/strings';
import LinkPreview from 'react-native-link-preview';
import { timeSince } from '../common/dateUtils';

class PostViewContainer extends Component {
    static navigationOptions = ({ navigation }) => {
        return {
            title: Strings.t("postViewScreenTitle"),
        }
    }

    constructor(props) {
        super(props);
        this.state = {};
    }

    handleUrlPress = (url) => {
        const formattedUrl = url.substr(0, 7).toLowerCase() === "http://" || url.substr(0, 8).toLowerCase() === "https://" ? url : "http://" + url;
        Linking.openURL(formattedUrl);
    }

    handleEmailPress = (email) => {
        Linking.openURL('mailto:' + email);
    }

    handlePhonePress = (phoneNumber) => {
        Linking.openURL('tel:' + phoneNumber);
    }

    getLinkPreview = async (text) => {
        try {
            // Don't fail over no preview.
            return linkPreview = await LinkPreview.getPreview(text);
        }
        catch (error) {
            return;
        }
    }

    getPostDetails() {
        this.props.fetchPostDetails(this.props.navigation.getParam('postId'));
    }

    addComment = (commentText) => {
        if (commentText)
            this.props.addComment(this.props.navigation.getParam('postId'), { text: commentText });
    }

    getComments() {
        this.props.fetchComments(this.props.navigation.getParam('postId'));
    }

    viewProfile = (userId) => {
        this.props.navigation.push("ProfileView", { userId, })
    }

    componentWillReceiveProps = async (nextProps) => {
        if (!this.props.text && nextProps.text) {
            const linkPreview = await this.getLinkPreview(nextProps.text);
            this.setState({ linkPreview });
        }
    }

    componentDidUpdate(prevProps) {
        // Error adding comment. Alert (unless we got logged out, in which case user is directed to the login screen...)
        if (this.props.addCommentError && !prevProps.addCommentError && this.props.loggedIn) {
            Alert.alert(Strings.t("somethingWentWrongAlertTitle"), Strings.t('somethingWentWrongAlert'));
        }
    }

    componentDidMount() {
        this.getPostDetails();
        this.getComments();
    }

    componentWillMount() {
        this.props.resetPostView();
        this.willFocusSubscription = this.props.navigation.addListener(
            'willFocus',
            payload => {
                this.props.resetPostView();
                this.getPostDetails();
                this.getComments();
            }

        )
    }

    componentWillUnmount() {
        this.willFocusSubscription.remove();
    }

    viewImages = (index) => {
        this.props.navigation.push("ImageViewer", { 
            images: this.props.images.map((image) => ({ 
                url: `${this.props.imageResizerPrefixURL}/filters:max_bytes(${700*1024})/${this.props.originalImagesPrefix}/${image}` 
            })), 
            index }
        )
    }

    render() {
        return (
            <PostView
                postId={this.props.navigation.getParam('postId')}
                posterProfilePic={this.props.postedBy.profilePic ? { uri: this.props.postedBy.profilePic } :
                    require('../assets/icon-user.png')}
                posterFirstName={this.props.postedBy.firstName}
                posterLastName={this.props.postedBy.lastName}
                posterId={this.props.postedBy._id}
                comments={this.props.comments}
                viewProfile={this.viewProfile}
                addComment={this.addComment}
                isAddingComment={this.props.isAddingComment}
                addCommentError={this.props.addCommentError}
                loadCommentsError={this.props.loadCommentsError}
                isLoadingComments={this.props.isLoadingComments}
                isLoadingPostDetails={this.props.isLoadingPostDetails}
                title={this.props.title}
                text={this.props.text}
                images={this.props.images.slice(0,6)}
                totalImagesNo={this.props.images.length}
                imageResizerPrefixURL={this.props.imageResizerPrefixURL}
                originalImagesPrefix={this.props.originalImagesPrefix}
                viewImages={this.viewImages}
                timeSincePost={timeSince(new Date(this.props.postDate))}
                handleUrlPress={this.handleUrlPress}
                handleEmailPress={this.handleEmailPress}
                handlePhonePress={this.handlePhonePress}
                linkPreview={this.state.linkPreview}
            />
        );
    }
}

const mapStateToProps = (state, ownProps) => {
    return {
        loggedIn: (state.user.tokens.refreshToken != null),
        isRefreshing: state.viewPost.comments.isRefreshing,
        isLoadingComments: state.viewPost.comments.isLoading,
        comments: state.viewPost.comments.items,
        loadCommentsError: state.viewPost.comments.loadError,
        isAddingComment: state.viewPost.comments.isAddingComment,
        addCommentError: state.viewPost.comments.addCommentError,
        title: state.viewPost.postDetails.title,
        text: state.viewPost.postDetails.text,
        postDate: state.viewPost.postDetails.postDate,
        postedBy: state.viewPost.postDetails.postedBy,
        images: state.viewPost.postDetails.images || [],
        originalImagesPrefix: state.viewPost.postDetails.originalImagesPrefix,
        imageResizerPrefixURL: state.viewPost.postDetails.imageResizerPrefixURL,
        isLoadingPostDetails: state.viewPost.postDetails.isLoading,
    }
}

const mapDispatchToProps = (dispatch, ownProps) => {
    return {
        fetchPostDetails: (postId) => {
            return dispatch(ActionsPostDetails.fetchPostDetails(postId));
        },
        fetchComments: (postId) => {
            return dispatch(ActionsComments.fetchComments(postId));
        },
        addComment: (postId, comment) => {
            return dispatch(ActionsComments.addComment(postId, comment));
        },
        refreshComments: () => {
            return dispatch(ActionsComments.refreshComments());
        },
        resetPostView: () => {
            dispatch(ActionsComments.receiveComments([]));
            dispatch(ActionsPostDetails.receivePostDetails({ postedBy: {} }));
        },

    }
}

const PostViewContainerClass = connect(mapStateToProps, mapDispatchToProps)(PostViewContainer);

export default PostViewContainerClass;