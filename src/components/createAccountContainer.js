import {
    Alert,
    Platform,
    Linking,
    View,
} from 'react-native';
import React, { Component } from 'react';
import { connect } from 'react-redux'
import { doLogin, doLogout } from '../common/authApi';
import GetCredetials from '../controls/getCredentials';
import GetUserDetails from '../controls/getUserDetails';
import {
    AUTHORIZATION_SERVER_ADDRESS,
    AUTHORIZATION_SERVER_API_VERSION,
} from '../common/constants';
import Strings from '../assets/strings';
import validator from 'validator';
import StyleConstants from '../styles/constants';
import { reportError } from '../common/errorReporter';
import { validateUserDetails } from '../common/validators';
import * as Photos from '../common/photos';
import Login from '../controls/login';
import WaitingOverlay from '../controls/waitingOverlay';

import { GoogleSignin, } from 'react-native-google-signin';
import { AccessToken as FBAccessToken, LoginManager as FBLoginManager } from 'react-native-fbsdk';

const emptyState = {
    errorMessage: "",
    email: "",
    password: "",
    firstName: "",
    lastName: "",
    profilePic: null,
    loggingIn: false,
}

class CreateAccountContainer extends Component {
    constructor(props) {
        super(props);
        this.state = { ...emptyState, step: "login" };
    }

    getClientDetails = () => {
        // Client details as expected in login/registration
        return {
            os: {
                name: Platform.OS,
                version: String(Platform.Version)
            },
        }
    }

    openTermsAndConditions = () => {
        Linking.openURL("https://1drv.ms/b/s!AnSwFXfuREiMpO4aBGPoUGWXY52spw");
    }

    emailAvailable = async (email) => {
        const url = `${AUTHORIZATION_SERVER_ADDRESS}/api/clear/${AUTHORIZATION_SERVER_API_VERSION}/emailAvailable/${email}`;

        // Simple GET to see if email is available.
        return await fetch(url)
            .then((response) => {
                if (!response.ok) {
                    Alert.alert(Strings.t("somethingWentWrongAlertTitle"), Strings.t('somethingWentWrongAlert'));
                    throw new Error("Invalid response checking email availability");
                }
                return response.json()
            })
            .then((res) => {
                return res.available;
            })
            .catch((error) => {
                Alert.alert(Strings.t("somethingWentWrongAlertTitle"), Strings.t('somethingWentWrongAlert'));
                console.error("Error checking email availability", error);
                throw new Error("Error checking email availability");
            });
    }

    getCredentials = async (email, password, confirmPassword) => {
        if (!email) {
            return this.setState({ errorMessage: Strings.t("fieldRequired", { field: Strings.t("emailField") }) });
        }

        if (!validator.isEmail(email)) {
            return this.setState({ errorMessage: Strings.t("invalidEmail") });
        }

        // NBTODO: For now we're waiting for the response. Can do that in the background once user leaves the 
        //         email field etc.
        if (!(await this.emailAvailable(email))) {
            return this.setState({ errorMessage: Strings.t("emailAlreadyUsed") });
        }

        if (!password.trim()) {
            return this.setState({ errorMessage: Strings.t("fieldRequired", { field: Strings.t("passwordField") }) });
        }

        if (password.length < 8) {
            return this.setState({ errorMessage: Strings.t("passwordTooShort") });
        }

        if (password != confirmPassword) {
            return this.setState({ errorMessage: Strings.t("passwordsDontMatch") });
        }

        this.setState({ email: email.trim(), password: password, step: "getUserDetails", errorMessage: "" });
    }

    getProfilePic = async () => {
        Photos.getProfilePic((image) => {
            this.setState({ ...image });
        });
    }

    getUserDetails = async (firstName, lastName, termsAccepted) => {
        if (!termsAccepted) {
            return this.setState({ errorMessage: Strings.t("termsNotAccepted") });
        }

        const validationError = await validateUserDetails({ firstName, lastName, profilePic: this.state.profilePic })

        if (validationError != null) {
            return this.setState(validationError);
        }

        await this.setState({ firstName: firstName.trim(), lastName: lastName.trim(), errorMessage: "" });
        this.doRegister();
    }

    // NBTODO: Validation - here, on the server or on the login component? I think both...
    doRegister = () => {
        this.setState({ loggingIn: true });
        let url = `${AUTHORIZATION_SERVER_ADDRESS}/api/clear/${AUTHORIZATION_SERVER_API_VERSION}/register`;
        console.info('Register ' + this.state.firstName + ' ' + this.state.lastName + '(' + this.state.email + ') to ' + url);

        fetch(url,
            {
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json',
                },
                body: JSON.stringify(
                    {
                        email: this.state.email,
                        password: this.state.password,
                        firstName: this.state.firstName,
                        lastName: this.state.lastName,
                        client: this.getClientDetails(),
                    })
            })
            // Fetch success - convert to JSON
            .then((res) => {
                if (!res.ok && res.status >= 500) {
                    reportError('Register user failed: ', res);
                    Alert.alert(Strings.t("somethingWentWrongAlertTitle"), Strings.t('somethingWentWrongAlert'));
                }
                else {
                    res.json().then((res) => this.props.loginSuccess({
                        id: res.user.id,
                        firstName: this.state.firstName,
                        lastName: this.state.lastName
                    }, res.tokens, { filename: this.state.profilePic, mime: this.state.mime }));
                }
                this.setState({ loggingIn: false });
            })
            // Fetch failure
            .catch((error) => {
                Alert.alert(Strings.t("somethingWentWrongAlertTitle"), Strings.t('somethingWentWrongAlert'));
                reportError('Register user caught error: ', JSON.stringify(error));
                this.setState({ loggingIn: false });
            });

    }

    goToLogin = () => {
        this.setState({ ...emptyState, step: "login" });
    }

    goToSignUp = () => {
        this.setState({ ...emptyState, step: "getCredentials" });
    }

    authSuccess = (res) => {
        this.setState({ errorMessage: "" }) // Clear message

        // NBTODO: If something goes wrong with dispatching the login user will not see any error.
        //         For example, if it fails to write tokens to the DB. 
        this.props.loginSuccess(res.user, res.tokens);
    }

    authFailed = (res) => {
        this.setState({
            errorMessage: Strings.t('loginFailedMessage'),
        })

        console.info('Auth failed');
        this.props.loginFailure();
    }

    // NBTODO: Validation - here, on the server or on the login component? I think both...
    doLogin = (email, password) => {
        if (this.state.loggingIn) {
            return;
        }

        this.setState({ loggingIn: true });
        let url = `${AUTHORIZATION_SERVER_ADDRESS}/api/clear/${AUTHORIZATION_SERVER_API_VERSION}/login`;
        console.info('Login ' + email + ' to ' + url);

        fetch(url,
            {
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json',
                },
                body: JSON.stringify(
                    {
                        email: email.trim(),
                        password,
                        client: this.getClientDetails(),
                    })
            })
            // Fetch success - convert to JSON
            .then((res) => {
                if (!res.ok && res.status >= 500) {
                    reportError('Login failed: ', res);
                    Alert.alert(Strings.t("somethingWentWrongAlertTitle"), Strings.t('somethingWentWrongAlert'));
                    this.props.loginFailure();
                }
                else if (!res.ok && res.status === 401) {
                    this.authFailed();
                }
                else {
                    res.json().then((res) => this.authSuccess(res));
                }
                this.setState({ loggingIn: false });
            })
            // Fetch failure
            .catch((error) => {
                reportError('Login Error: ', error);
                Alert.alert(Strings.t("somethingWentWrongAlertTitle"), Strings.t('somethingWentWrongAlert'));
                this.props.loginFailure();
                this.setState({ loggingIn: false });
            });

    }

    doGoogleLogin = async () => {
        try {
            this.setState({ loggingIn: true });
            let user;
            try {
                user = await GoogleSignin.signIn();
            }
            catch (error) {
                if (error.code !== 12501 && error.code !== -5) { // Canceled
                    throw error;
                }
                else {
                    this.props.loginFailure();
                    this.setState({ loggingIn: false });
                    return;
                }
            }
            let url = `${AUTHORIZATION_SERVER_ADDRESS}/api/clear/${AUTHORIZATION_SERVER_API_VERSION}/googleLogin`;
            console.info('Google Login ' + user.email + ' to ' + url);

            fetch(url,
                {
                    method: 'POST',
                    headers: {
                        'Content-Type': 'application/json',
                    },
                    body: JSON.stringify(
                        {
                            idToken: user.idToken,
                            client: this.getClientDetails(),
                        })
                })
                // Fetch success - convert to JSON
                .then(async (res) => {
                    if (!res.ok && res.status >= 500) {
                        reportError('Login failed: ', res);
                        Alert.alert(Strings.t("somethingWentWrongAlertTitle"), Strings.t('somethingWentWrongAlert'));
                        this.props.loginFailure();
                    }
                    else if (!res.ok && res.status === 401) {
                        this.authFailed();
                    }
                    else {
                        const jsonRes = await res.json();
                        jsonRes.user.googleUser = user;
                        this.authSuccess(jsonRes);
                    }
                    this.setState({ loggingIn: false });
                })
                // Fetch failure
                .catch((error) => {
                    reportError('Login Error: ', JSON.stringify(error));
                    Alert.alert(Strings.t("somethingWentWrongAlertTitle"), Strings.t('somethingWentWrongAlert'));
                    this.props.loginFailure();
                    this.setState({ loggingIn: false });
                });
        }
        catch (err) {
            Alert.alert(Strings.t("somethingWentWrongAlertTitle"), Strings.t('somethingWentWrongAlert'));
            this.props.loginFailure();
            this.setState({ loggingIn: false });
        }
    }

    doFacebookLogin = async () => {
        this.setState({ loggingIn: true });

        let result;
        let error;

        try {
            result = await FBLoginManager.logInWithReadPermissions(['public_profile', 'email']);
        }
        catch(err) {
            error = err;
        }

        if (error) {
            reportError("Facebook login error", error);
            Alert.alert(Strings.t("somethingWentWrongAlertTitle"), Strings.t('somethingWentWrongAlert'));
            this.setState({ loggingIn: false });
            return;
        }

        if (result.isCancelled) {
            this.setState({ loggingIn: false });
            return;
        }

        if (result.declinedPermissions.length > 0) {
            this.setState({ loggingIn: false });
            Alert.alert(Strings.t('facebookRequiredPermissions'));
            FBLoginManager.logOut();
            return;
        }

        const tokenInfo = await FBAccessToken.getCurrentAccessToken();
        let url = `${AUTHORIZATION_SERVER_ADDRESS}/api/clear/${AUTHORIZATION_SERVER_API_VERSION}/facebookLogin`;
        console.info('Facebook Login to ' + url);

        fetch(url,
            {
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json',
                },
                body: JSON.stringify(
                    {
                        accessToken: tokenInfo.accessToken,
                        client: this.getClientDetails(),
                    })
            })
            // Fetch success - convert to JSON
            .then(async (res) => {
                if (!res.ok && res.status >= 500) {
                    reportError('Facebook Login failed: ', res);
                    Alert.alert(Strings.t("somethingWentWrongAlertTitle"), Strings.t('somethingWentWrongAlert'));
                    this.props.loginFailure();
                }
                else if (!res.ok && res.status === 401) {
                    this.authFailed();
                }
                else {
                    const jsonRes = await res.json();
                    jsonRes.user.facebookUser = tokenInfo;
                    this.authSuccess(jsonRes);
                }
                this.setState({ loggingIn: false });
            })
            // Fetch failure
            .catch((error) => {
                reportError('Facebook Login Error: ', error);
                Alert.alert(Strings.t("somethingWentWrongAlertTitle"), Strings.t('somethingWentWrongAlert'));
                this.props.loginFailure();
                this.setState({ loggingIn: false });
            });
    }

    renderBody = () => {
        switch (this.state.step) {
            case 'getCredentials':
                return (<GetCredetials errorMessage={this.state.errorMessage}
                    onSubmit={this.getCredentials}
                    onGoogleLogin={this.doGoogleLogin}
                    onFacebookLogin={this.doFacebookLogin}
                    goToLogin={this.goToLogin} />)
            case 'getUserDetails':
                return (<GetUserDetails errorMessage={this.state.errorMessage} profilePic={this.state.profilePic ? { uri: this.state.profilePic } : require('../assets/icon-user.png')}
                    onSubmit={this.getUserDetails}
                    openTermsAndConditions={this.openTermsAndConditions}
                    getProfilePic={this.getProfilePic} />)
            case 'login':
                return (<Login errorMessage={this.state.errorMessage}
                    onSubmit={this.doLogin}
                    onGoogleLogin={this.doGoogleLogin}
                    onFacebookLogin={this.doFacebookLogin}
                    goToSignUp={this.goToSignUp} />)

        }
    }

    componentWillMount = () => {
        if (this.props.loggedIn) {
            this.props.navigation.navigate('App');
        }
    }
    render() {
        // Setting the modal over the other components rather than in each individual component) to 
        // prevent flickering when switching between views.
        return (
            <View style={{ flex: 1 }}>
                {this.renderBody()}
                {this.state.loggingIn && (<WaitingOverlay text={Strings.t("loggingIn")} />)}
            </View>
        )
    }
}

const mapStateToProps = (state, ownProps) => {
    return {
        loggedIn: (state.user.tokens.refreshToken != null),
    }
}

const mapDispatchToProps = (dispatch, ownProps) => {
    return {
        loginSuccess: (user, tokens, pictureData) => {
            dispatch(doLogin(user, tokens, pictureData));
        },
        loginFailure: () => {
            dispatch(doLogout());
        },
    }
}

const CreateAccountClass = connect(mapStateToProps, mapDispatchToProps)(CreateAccountContainer);
export default CreateAccountClass;