import React, { Component } from 'react';
import {
	Text,
	View,
	SafeAreaView,
	TouchableOpacity,
	Image,
	TouchableWithoutFeedback,
	Linking,
	Platform,
} from 'react-native';
import { DrawerActions } from 'react-navigation';
import { connect } from 'react-redux'

import { doLogout, } from '../common/authApi';
import Styles from '../styles/drawer';
import StyleConstants from '../styles/constants';
import Strings from '../assets/strings';
// NBTODOv2 import Config from 'react-native-config';
import { reportError } from '../common/errorReporter';

class Drawer extends Component {
	constructor(props) {
		super(props);
	}

	closeDrawer() {
		this.props.navigation.dispatch(DrawerActions.closeDrawer());
	}

	showProfile = () => {
		this.closeDrawer();
		this.props.navigation.navigate("ProfileEdit"); 
			// animationType: 'slide-horizontal',
	}

	doLogout = () => {
		this.closeDrawer();
		this.props.logout();
	}

	sendFeedback = async () => {
		try {
			const locationString = this.props.location && this.props.location.position ? this.props.location.position.latitude + ' ' + this.props.location.position.longitude : 'Unknown';
			const locationTimeString = this.props.location && this.props.location.lastReceived ? this.props.location.lastReceived : ''
			await Linking.openURL(`mailto:feedback@boarder.io?subject=Feedback and Support&body=\n\n------ Support Information ------\n${locationTimeString} ${locationString}\n${Platform.OS} ${Platform.Version} ${this.props.userId}\n---- End Support Information ----`);
		}
		catch (error) {
			reportError('Error opening email message for feedback:', error)
		}
	}

	openMap = () => {
		if (this.props.location && this.props.location.position) {
			const scheme = Platform.OS === 'ios' ? 'maps:0,0?q=' : 'geo:0,0?q=';
			const latLng = `${this.props.location.position.latitude},${this.props.location.position.longitude}`;
			const url = Platform.OS === 'ios' ? `${scheme}@${latLng}` : `${scheme}${latLng}`;

			Linking.openURL(url);
		}
	}

	render() {
		return (
			<SafeAreaView style={Styles.container}>
				<View>
					<View>
						<TouchableWithoutFeedback onPress={this.showProfile}>
							<View style={Styles.drawerProfile}>
								<Image source={this.props.profilePic} style={{ width: 80, height: 80, borderRadius: 40, }} />
							</View>
						</TouchableWithoutFeedback>
						<View style={{ paddingHorizontal: 25, }}>
							<Text style={{ fontSize: 20, color: StyleConstants.LIGHT_TEXT_COLOR, textAlign: "left" }}>{this.props.firstName} {this.props.lastName}</Text>
						</View>
					</View>
					<View
						style={{
							height: 1,
							width: "100%",
							backgroundColor: StyleConstants.BACKGROUND_COLOR,
							justifyContent: "center",
							marginVertical: 10,
							opacity: 0.3,
						}}
					/>
					<View style={Styles.drawerList}>
						<TouchableOpacity onPress={this.doLogout}>
							<View style={Styles.drawerListItem}>
								<Image source={require('../assets/icon-logout.png')} style={Styles.drawerListIcon} />
								<Text style={Styles.drawerListItemText}>
									{Strings.t("drawer.logout")}
								</Text>
							</View>
						</TouchableOpacity>
						<TouchableOpacity onPress={this.sendFeedback}>
							<View style={Styles.drawerListItem}>
								<Image source={require('../assets/icon-support.png')} style={Styles.drawerListIcon} />
								<Text style={Styles.drawerListItemText}>
									{Strings.t("drawer.feedback")}
								</Text>
							</View>
						</TouchableOpacity>
					</View>
				</View>
				<View>
					{
						// NBTODOv2
						// Config.ENV_TYPE === 'TEST' && (
						// 	<View style={{ paddingLeft: 25, paddingBottom: 25, }}>
						// 		<Text style={Styles.debug}>{Strings.t("appName")}</Text>
						// 		<Text style={Styles.debug}>Environment: {Config.ENV_NAME}</Text>
						// 		<View>
						// 			<Text style={Styles.debug}>Last known location: </Text>
						// 			<TouchableWithoutFeedback onPress={this.openMap}>
						// 				<View>
						// 					<Text style={Styles.debug}>{this.props.location && this.props.location.position ? this.props.location.position.latitude + ', ' + this.props.location.position.longitude : 'Unknown'}</Text>
						// 					<Text style={Styles.debug}>{this.props.location && this.props.location.lastReceived ? "on " + new Date(this.props.location.lastReceived).toLocaleString() : ''}</Text>
						// 				</View>
						// 			</TouchableWithoutFeedback>
						// 		</View>
						// 	</View>
						// )
					}
				</View>
			</SafeAreaView>
		);
	}
}

const mapStateToProps = (state, ownProps) => {
	return {
		loggedIn: (state.user.tokens.refreshToken != null),
		userId: state.user.info.id,
		profilePic: state.user.info.profilePic ? { uri: state.user.info.profilePic } : require('../assets/icon-user.png'),
		firstName: state.user.info.firstName,
		lastName: state.user.info.lastName,
		location: state.location,
		notificationsCount: state.notifications.count,
		appActive: (state.app.appState === 'active'),
		notificationToken: state.app.notificationToken,
		notificationTokenUpdated: state.app.notificationTokenUpdated,
		notificationTokenUpdating: state.app.notificationTokenUpdating,
	}
}

const mapDispatchToProps = (dispatch, ownProps) => {
	return {
		logout: () => {
			dispatch(doLogout());
		},
	};
}

export default connect(mapStateToProps, mapDispatchToProps)(Drawer);
