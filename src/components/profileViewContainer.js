import React, { Component } from 'react';
import {
    StyleSheet,
    Text,
    View,
    Image,
    Alert,
} from 'react-native';

import { connect } from 'react-redux';

// NBTODO: Should this view be aware of login state?
import ProfileView from '../controls/profileView';
import * as Actions from '../state/actionsUserProfile';
import Strings from '../assets/strings';

class ProfileViewContainer extends Component {
    static navigationOptions = ({ navigation }) => {
        return {
            title: Strings.t("profileViewScreenTitle")
        }
    }

    constructor(props) {
        super(props);
    }

    goToEditProfile = () => {
        this.props.navigation.navigate("ProfileEdit");
    }

    getProfileDetails() {
        this.props.fetchUserDetails(this.props.navigation.getParam('userId'));
    }

    componentWillMount() {

        this.willFocusSubscription = this.props.navigation.addListener(
            'willFocus',
            payload => {
                this.props.resetProfileView();
                this.getProfileDetails();
            }
        )
    }

    componentWillUnmount() {
        this.willFocusSubscription.remove();
    }

    render() {
        return (
            <ProfileView
                profilePic={this.props.profilePic}
                firstName={this.props.firstName}
                lastName={this.props.lastName}
                editable={this.props.editable}
                editProfile={this.goToEditProfile}
            />
        );
    }
}

const mapStateToProps = (state, ownProps) => {
    return {
        loggedIn: (state.user.tokens.refreshToken != null),
        isLoading: state.userProfile.isLoading,
        loadError: state.userProfile.loadError,
        profilePic: state.userProfile.profilePic ? { uri: state.userProfile.profilePic } : require('../assets/icon-user.png'),
        firstName: state.userProfile.firstName,
        lastName: state.userProfile.lastName,
        editable: ownProps.navigation.getParam('userId') === state.user.info.id,
    }
}

const mapDispatchToProps = (dispatch, ownProps) => {
    return {
        fetchUserDetails: (userId) => {
            return dispatch(Actions.fetchUserDetails(userId));
        },
        resetProfileView: () => {
            return dispatch(Actions.receiveUserDetails({}));
        },
    }
}

const ProfileViewContainerClass = connect(mapStateToProps, mapDispatchToProps)(ProfileViewContainer);

export default ProfileViewContainerClass;