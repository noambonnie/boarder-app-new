import React, { Component } from 'react';
import {
    Alert,
} from 'react-native';

import { connect } from 'react-redux';
import PostAdd from '../controls/postAdd';
import HeaderActionButton from '../controls/HeaderActionButton'
import * as Actions from '../state/actionsAddPost';
import Strings from '../assets/strings';
import { pickMultipleImages } from '../common/photos';
import ImageUploader from '../common/imageUploader';

// The Post button on the header needs to be enabled/disabled based on the state.
// Using navigation.setParams() to change the disabled state triggers a render of the
// entire header, resulting in flickering (on iOS). So instead, I create a component
// and map it to the redux state. This way only the component gets re-rendered.
class PostButton extends Component {
    render() {
        return (
            <HeaderActionButton
                disabled={this.props.disabled}
                onPress={this.props.addPost}
                label={Strings.t("addPost.postMessageButton")}
            />
        )
    }
}

const mapStateToButtonProps = (state, ownProps) => {
    return {
        disabled: state.addPost.buttonDisabled || state.addPost.isAddingPost,
    }
}

const PostButtonClass = connect(mapStateToButtonProps)(PostButton);

class PostAddContainer extends Component {
    static navigationOptions = ({ navigation }) => {
        const params = navigation.state.params || {};
        const { postButton } = params;

        return {
            headerRight: postButton ||
                // Default to a disabled button (for when the screen still gets rendered)
                (<HeaderActionButton disabled={true} label={Strings.t("addPost.postMessageButton")} />),
        };
    }

    constructor(props) {
        super(props);
        this.state = {
            title: '',
            text: '',
            location: {
                address: props.filter ? props.filter.byDistance.address : null,
                position: props.filter ? props.filter.byDistance.position : null,
            },
            locationValid: this.props.hasLocation || props.filter.byDistance.position != null,
            postImageUploaders: {},
        }

        this.uploadedImageKey = 0; // uploaded images need a unique key. Simply use a sequence.

        this.setPostButtonState();
    }

    componentWillReceiveProps(nextProps) {
        // Clear comment field after successfully adding a post. 
        if (this.props.isAddingPost && !nextProps.isAddingPost && !nextProps.addPostError) {
            this.setState({ title: '', text: '', });
            this.props.navigation.pop();
        }
    }

    componentDidUpdate(prevProps) {
        // Error adding post. Alert.
        if (this.props.addPostError && !prevProps.addPostError) {
            Alert.alert(Strings.t("somethingWentWrongAlertTitle"), Strings.t('somethingWentWrongAlert'));
        }
    }

    componentWillMount() {
        this.setPostButtonState();
    }

    addPost = () => {
        if (this.props.isAddingPost) return; // In the process of posting already (should not happen)

        const { title, text, location: geolocation, postImageUploaders } = this.state;
        this.props.addPost({ title, text, geolocation, postImageUploaders })
    }

    selectPhotos = async () => {
        const MAX_IMAGES = 9;
        const images = await pickMultipleImages(MAX_IMAGES) || [];

        const currentImagesLength = Object.keys(this.state.postImageUploaders).length;
        currentImagesLength + images.length > MAX_IMAGES && Alert.alert(Strings.t("addPost.tooManyPhotosTitle"), Strings.t("addPost.tooManyPhotosText", { count: MAX_IMAGES }));

        // Remove excess images
        images.splice(MAX_IMAGES - currentImagesLength);

        const imageUploaders = images.reduce((acc, imageDetails) => {
            const key = this.uploadedImageKey++;
            acc[(key).toString()] = {
                uploader: new ImageUploader(
                    imageDetails,
                    'postImages',
                    (value) => this.setUploadProgress(key, value),
                    () => this.setUploadFailed(key)
                ),
                progress: 0,
                failed: false,
            };
            return acc;
        }, { ...this.state.postImageUploaders });

        // Set the state and kick off the uploads
        this.setState({ postImageUploaders: imageUploaders }, () => {
            Object.keys(this.state.postImageUploaders).map((key) => {
                !this.state.postImageUploaders[key].uploader.getUploadPromise() && this.state.postImageUploaders[key].uploader.uploadImage();
            })
        });
    }

    setUploadProgress = (key, value) => {
        if (!this.state.postImageUploaders[key]) return;

        // Update the key with the new progress value. Copy the rest of the stuff (Warning: Shallow copy!)
        this.setState({
            postImageUploaders: {
                ...this.state.postImageUploaders,
                [key]: { ...this.state.postImageUploaders[key], progress: value }
            }
        })
    }

    deleteUploadedImage = (key) => {
        const postImageUploaders = { ...this.state.postImageUploaders };
        delete postImageUploaders[key];

        this.setState({ postImageUploaders });
    }

    setUploadFailed = (key) => {
        if (!this.state.postImageUploaders[key]) return;

        this.setState({
            postImageUploaders: {
                ...this.state.postImageUploaders,
                [key]: { ...this.state.postImageUploaders[key], failed: true }
            }
        })
    }

    setPostButtonState = () => {
        const disabled = !this.state.locationValid || !this.state.title || !this.state.text;

        this.props.setPostButtonDisabled(disabled);
    }

    setPostField = (object) => {
        this.setState({ ...object }, this.setPostButtonState);
    }

    componentWillMount() {
        this.props.navigation.setParams({
            postButton:
                (<PostButtonClass addPost={this.addPost} />)
        });
    }

    render() {
        return (
            <PostAdd
                setPostField={this.setPostField}
                currentFilter={this.props.filter}
                hasLocation={this.props.hasLocation}
                locationValid={this.state.locationValid}
                selectPhotos={this.selectPhotos}
                postImageUploaders={this.state.postImageUploaders}
                deleteUploadedImage={this.deleteUploadedImage}
                isAddingPost={this.props.isAddingPost}
                isUploadingImages={this.props.isUploadingImages}
            />
        );
    }
}

const mapStateToProps = (state, ownProps) => {
    return {
        loggedIn: (state.user.tokens.refreshToken != null),
        isAddingPost: state.addPost.isAddingPost,
        isUploadingImages: state.addPost.isUploadingImages,
        addPostError: state.addPost.addPostError,
        hasLocation: state.location && state.location.position != null,
        filter: state.posts.filter,
    }
}

const mapDispatchToProps = (dispatch, ownProps) => {
    return {
        addPost: (postData) => {
            return dispatch(Actions.addPost(postData));
        },
        setPostButtonDisabled: (disabled) => {
            return dispatch(Actions.setPostButtonDisabled(disabled));
        }
    }
}

const PostAddContainerClass = connect(mapStateToProps, mapDispatchToProps)(PostAddContainer);

export default PostAddContainerClass;