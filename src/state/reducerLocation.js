'use strict'
import * as Actions from "./actionsLocation";

const initialState = {
    position: null, 
    lastReceived: null,
};

// NBTODO: Load comments since last comment date - this way I can add whatever new comments came after
//         the user posts a comment (and for the future auto fetch comments)
export default function (state = initialState, action) {
    switch (action.type) {
        case Actions.RECEIVE_CURRENT_LOCATION:
            return Object.assign({}, state, {
                position:  {
                    latitude: action.location.position.latitude,
                    longitude: action.location.position.longitude,
                },
                lastReceived: action.location.lastReceived,
            })
        case Actions.FAILURE_CURRENT_LOCATION:
            // NBTODO: Currently do nothing....
            return Object.assign({}, state, {
            })
        default:
            return state;
    }

}
