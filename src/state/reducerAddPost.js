'use strict'
import * as Actions from "./actionsAddPost";

const initialState = {
    isAddingPost: false,
    addPostError: false,
    valid: false,
    isUploadingImages: false,
};

export default function (state = initialState, action) {
    switch (action.type) {
        case Actions.START_UPLOAD_IMAGES:
            return {
                ...state,
                isUploadingImages: true,
            }
        case Actions.DONE_UPLOAD_IMAGES:
            return {
                ...state,
                isUploadingImages: false
            }
        case Actions.REQUEST_ADD_POST:
            return { 
                ...state,
                isAddingPost: true,
                addPostError: false,
                isUploadingImages: false,
            }
        case Actions.RECEIVE_ADD_POST:
            return { 
                ...state,
                isAddingPost: false,
            }
        case Actions.FAILURE_ADD_POST: {
            return {
                ...state,
                isAddingPost: false,
                addPostError: true,
                isUploadingImages: false,
            }
        }
        case Actions.SET_POST_BUTTON_DISABLED: {
            return { ...state, buttonDisabled: action.buttonDisabled };
        }
        default:
            return state;
    }

}
