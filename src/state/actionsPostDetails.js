import { FEED_SERVER_ADDRESS, FEED_SERVER_API_VERSION } from '../common/constants';
import { secureFetch } from '../common/authApi';
import { reportError } from '../common/errorReporter';

export const REQUEST_POST_DETAILS = 'REQUEST_POST_DETAILS'
function requestPostDetails(postId) {
    return {
        type: REQUEST_POST_DETAILS,
        postId: postId,
    }
}

export const RECEIVE_POST_DETAILS = 'RECEIVE_POST_DETAILS'
export function receivePostDetails(post) {
    return {
        type: RECEIVE_POST_DETAILS,
        post,
    }
}

export const FAILURE_POST_DETAILS = 'FAILURE_POST_DETAILS'
function failurePostDetails(error) {
    return {
        type: FAILURE_POST_DETAILS,
        error: error,
    }
}

export function fetchPostDetails(postId) {
    return function (dispatch) {
        // First dispatch: the app state is updated to inform
        // that the API call is starting.

        dispatch(requestPostDetails(postId));

        let url = `${FEED_SERVER_ADDRESS}/api/${FEED_SERVER_API_VERSION}/posts/${postId}`;

        return secureFetch(url, {
            method: 'GET',
            headers: {
                'Content-Type': 'application/json',
            },
        })
            .then(response => response.json())
            .then(json => {
                if (json.success) {
                    dispatch(receivePostDetails(json.post))
                }
                else {
                    reportError('fetchPostDetails: secure fetch error occured. ', json);
                    dispatch(failurePostDetails(json));
                }
            })
            .catch(error => {
                reportError('fetchPostDetails: secure fetch caught error: ', error)
                dispatch(failurePostDetails(error));
            })
    }
}
