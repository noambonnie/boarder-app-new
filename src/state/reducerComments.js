'use strict'
import * as Actions from "./actionsComments";
import { LOGOUT } from "../common/authApi";

const initialState = {
    isLoading: false,
    isRefreshing: false,
    items: [],
    loadError: false,
    isAddingComment: false,
    addCommentError: false,
};

// NBTODO: Load comments since last comment date - this way I can add whatever new comments came after
//         the user posts a comment (and for the future auto fetch comments)
export default function (state = initialState, action) {
    switch (action.type) {
        case Actions.REFRESH_COMMENTS:
            return Object.assign({}, state, {
                isRefreshing: true,
                items: [],
                loadError: false,
            })
        case Actions.REQUEST_COMMENTS:
            return Object.assign({}, state, {
                isLoading: true,
                isRefreshing: false,
                loadError: false,
            })
        case Actions.RECEIVE_COMMENTS:
            return Object.assign({}, state, {
                isLoading: false,
                isRefreshing: false,
                items: action.comments,
            })
        case Actions.FAILURE_COMMENTS: {
            return Object.assign({}, state, {
                loadError: true,
            })
        }
        case Actions.REQUEST_ADD_COMMENT:
            return Object.assign({}, state, {
                isAddingComment: true,
                addCommentError: false,
            })
        case Actions.RECEIVE_ADD_COMMENT:
            return Object.assign({}, state, {
                isAddingComment: false,
            })
        case Actions.FAILURE_ADD_COMMENT: {
            return Object.assign({}, state, {
                isAddingComment: false,
                addCommentError: true,
            })
        }
        case LOGOUT: {
            return initialState;
        }
        default:
            return state;
    }

}
