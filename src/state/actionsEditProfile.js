import { AUTHORIZATION_SERVER_ADDRESS, AUTHORIZATION_SERVER_API_VERSION } from '../common/constants';
import { secureFetch } from '../common/authApi';
import { reportError } from '../common/errorReporter';
import { receiveOwnUserDetails } from './actionsUser';

export const REQUEST_SET_OWN_DETAILS = 'REQUEST_SET_OWN_DETAILS'
function requestSetOwnUserDetails() {
    return {
        type: REQUEST_SET_OWN_DETAILS,
    }
}

export const RECEIVE_SET_OWN_DETAILS = 'RECEIVE_SET_OWN_DETAILS'
export function receiveSetOwnUserDetails() {
    return {
        type: RECEIVE_SET_OWN_DETAILS,
    }
}

export const FAILURE_SET_OWN_DETAILS = 'FAILURE_SET_OWN_DETAILS'
function failureSetOwnUserDetails(error) {
    return {
        type: FAILURE_SET_OWN_DETAILS,
        error: error,
    }
}

export const RESET_SET_OWN_DETAILS = 'RESET_SET_OWN_DETAILS'
export function resetSetOwnUserDetails(error) {
    return {
        type: RESET_SET_OWN_DETAILS,
    }
}

export function setOwnUserDetails(details, profilePic) {
    return function (dispatch) {
        // First dispatch: the app state is updated to inform
        // that the API call is starting.

        dispatch(requestSetOwnUserDetails());

        let url = `${AUTHORIZATION_SERVER_ADDRESS}/api/${AUTHORIZATION_SERVER_API_VERSION}/users/userDetails`;

        return secureFetch(url, {
            method: 'PUT',
            headers: {
                'Content-Type': 'application/json',
            },
            body: JSON.stringify(details),
        })
            .then(response => response.json())
            .then(json => {
                if (json.success) {
                    dispatch(receiveSetOwnUserDetails())
                    dispatch(receiveOwnUserDetails(json.userDetails))
                }
                else {
                    reportError('setOwnUserDetails: secure fetch error occured. ', json);
                    dispatch(failureSetOwnUserDetails(json));
                }
            })
            .catch(error => {
                reportError('setOwnUserDetails: secure fetch caught error: ', error)
                dispatch(failureSetOwnUserDetails(error));
            })
    }
}
