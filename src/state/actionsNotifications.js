import { FEED_SERVER_ADDRESS, FEED_SERVER_API_VERSION } from '../common/constants';
import { secureFetch } from '../common/authApi';
import { reportError } from '../common/errorReporter';

export const REQUEST_NOTIFICATIONS = 'REQUEST_NOTIFICATIONS';
function requestNotifications(page, firstNotificationDate) {
    return {
        type: REQUEST_NOTIFICATIONS,
        page: page,
        firstNotificationDate: firstNotificationDate,
    }
}

export const RECEIVE_NOTIFICATIONS = 'RECEIVE_NOTIFICATIONS';
function receiveNotifications(notifications) {
    return {
        type: RECEIVE_NOTIFICATIONS,
        notifications: notifications,
    }
}

export const FAILURE_NOTIFICATIONS = 'FAILURE_NOTIFICATIONS';
function failureNotifications(error) {
    return {
        type: FAILURE_NOTIFICATIONS,
        error: error,
    }
}

// NBTODO: Is this action needed? Maybe if page === 1 we consider it a refresh and set isRefreshing?
//         Currently all it does is tell the UI to show the refresh spinner.
//         I feel like my current way of deciding to refresh based on changes to the state
//         is wrong. I should revisit the redux example to see how this is used. I should be able to dispatch
//         an action to cause a refresh.
export const REFRESH_NOTIFICATIONS = 'REFRESH_NOTIFICATIONS'
export function refreshNotifications() {
    return {
        type: REFRESH_NOTIFICATIONS,
    }
}

export const RECEIVE_NOTIFICATION_COUNT = 'RECEIVE_NOTIFICATION_COUNT';
export function receiveNotificationsCount(count) {
    return {
        type: RECEIVE_NOTIFICATION_COUNT,
        count,
    }
}

export function fetchNotifications(page, firstNotificationDate) {
    return function (dispatch, getState) {
        // First dispatch: the app state is updated to inform
        // that the API call is starting.

        dispatch(requestNotifications(page, firstNotificationDate));

        let url = `${FEED_SERVER_ADDRESS}/api/${FEED_SERVER_API_VERSION}/notifications/?page=${page}&items=10`;
        if (firstNotificationDate) {
            url += `&startDate=${firstNotificationDate}`;
        }

        return secureFetch(url, {
            method: 'GET',
            headers: {
                'Content-Type': 'application/json',
            },
        })
            .then(response => response.json())
            .then(json => {
                if (json.success) {
                    dispatch(receiveNotifications(json.notifications));
                }
                else {
                    reportError('fetchNotifications: secure fetch error occured.', json);
                    dispatch(failureNotifications(json));
                }
            })
            .catch(error => {
                reportError('fetchNotifications: secure fetch caught error: ', error);
                dispatch(failureNotifications(error));
            })
    }
}

export function markNotificationRead(notificationId) {
    // NBTODO: Currently not acting on marking notification as read.
    //         However, it should change the state to refresh the notifications list.
    return function (dispatch, getState) {

        let url = `${FEED_SERVER_ADDRESS}/api/${FEED_SERVER_API_VERSION}/notifications/markRead/${notificationId}`;

        return secureFetch(url, {
            method: 'PUT',
            headers: {
                'Content-Type': 'application/json',
            },
        })
            .then(response => response.json())
            .then(json => {
                if (json.success) {
                    // NBTODO: Refresh the notification status to read.
                }
                else {
                    reportError('markNotificationRead: secure fetch error occured.', json);
                }
            })
            .catch(error => {
                reportError('markNotificationRead: secure fetch caught error: ', error);
            })
    }
}

export function fetchNotificationsCount() {
    // NBTODO: Currently not acting on marking notification as read.
    //         However, it should change the state to refresh the notifications list.
    return function (dispatch, getState) {

        let url = `${FEED_SERVER_ADDRESS}/api/${FEED_SERVER_API_VERSION}/notifications/count`;

        return secureFetch(url, {
            method: 'GET',
            headers: {
                'Content-Type': 'application/json',
            },
        })
            .then(response => response.json())
            .then(json => {
                if (json.success) {
                    dispatch(receiveNotificationsCount(json.count));
                }
                else {
                    reportError('fetchNotificationsCount: secure fetch error occured.', json);
                }
            })
            .catch(error => {
                reportError('fetchNotificationsCount: secure fetch caught error: ', error);
            })
    }
}

export function markNotificationsSeen() {
    // NBTODO: Currently not acting on marking notification as read.
    //         However, it should change the state to refresh the notifications list.
    return function (dispatch, getState) {

        let url = `${FEED_SERVER_ADDRESS}/api/${FEED_SERVER_API_VERSION}/notifications/markAllSeen`;

        return secureFetch(url, {
            method: 'PUT',
            headers: {
                'Content-Type': 'application/json',
            },
        })
            .then(response => response.json())
            .then(json => {
                if (json.success) {
                    // NBTODO: Refresh the notification status to read.
                }
                else {
                    reportError('markNotificationsSeen: secure fetch error occured.', json);
                }
            })
            .catch(error => {
                reportError('markNotificationsSeen: secure fetch caught error: ', error);
            })
    }
}

