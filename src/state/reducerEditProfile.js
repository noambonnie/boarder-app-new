'use strict'
import * as Actions from "./actionsEditProfile";
import { LOGOUT } from "../common/authApi";

const initialState = {
    isLoading: false,
    loadError: false,
    profilePic: '',
    firstName: '',
    lastName: '',
    email: '',
    isSaving: false,
    saveError: false,
};

export default function (state = initialState, action) {
    switch (action.type) {
        case Actions.REQUEST_SET_OWN_DETAILS:
            return Object.assign({}, state, {
                isSaving: true,
                saveError: false,
            })
        case Actions.RECEIVE_SET_OWN_DETAILS:
            return Object.assign({}, state, {
                isSaving: false,
            })
        case Actions.FAILURE_SET_OWN_DETAILS: {
            return Object.assign({}, state, {
                saveError: true,
                isSaving: false,
            })
        }
        case Actions.RESET_SET_OWN_DETAILS: {
            return Object.assign({}, state, {
                saveError: false,
                isSaving: false,
            })
        }
        case LOGOUT: {
            return initialState;
        }
        default:
            return state;
    }

}
