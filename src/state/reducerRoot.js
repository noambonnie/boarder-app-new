import {combineReducers} from 'redux';
import app from './reducerApp';
import user from "./reducerUser";
import posts from "./reducerPosts";
import viewPost from "./reducerViewPost";
import addPost from "./reducerAddPost";
import location from "./reducerLocation";
import notifications from "./reducerNotifications";
import userProfile from "./reducerUserProfile";
import editProfile from "./reducerEditProfile";

const rootReducer = combineReducers({
    app,
    user,
    posts,
    viewPost,
    addPost,
    location,
    notifications,
    userProfile,
    editProfile,
});

export default rootReducer;
