import { secureFetch } from '../common/authApi';
import { AUTHORIZATION_SERVER_ADDRESS, 
         AUTHORIZATION_SERVER_API_VERSION, } from '../common/constants';
import { reportError } from '../common/errorReporter';

// NBTODO: Combine with actionsEditProfile. Note that this means combining the reducers as well
//         and making sure it all works well.
export const REQUEST_SET_PROFILE_PICTURE = 'REQUEST_SET_PROFILE_PICTURE'
function requestSetProfilePicture() {
    // NBTODO: Am I using postData for anything?
    return {
        type: REQUEST_SET_PROFILE_PICTURE,
    }
}

export const RECEIVE_SET_PROFILE_PICTURE = 'RECEIVE_SET_PROFILE_PICTURE'
function receiveSetProfilePicture(profilePic) {
    // NBTODO: Am I using postData for anything?
    return {
        type: RECEIVE_SET_PROFILE_PICTURE,
        profilePic: profilePic,
    }
}

export const FAILURE_SET_PROFILE_PICTURE = 'FAILURE_SET_PROFILE_PICTURE'
function failureSetProfilePicture(error) {
    return {
        type: FAILURE_SET_PROFILE_PICTURE,
    }
}


export const RESET_SET_PROFILE_PICTURE = 'RESET_SET_PROFILE_PICTURE'
export function resetSetProfilePicture(error) {
    return {
        type: RESET_SET_PROFILE_PICTURE,
    }
}

export function setProfilePicture(pictureData) {
    return function (dispatch) {
        // First dispatch: the app state is updated to inform
        // that the API call is starting.

        dispatch(requestSetProfilePicture());

        // NBTODO: Are all user-related things handled by Auth server? Or should this go to a different server?
        let url = `${AUTHORIZATION_SERVER_ADDRESS}/api/${AUTHORIZATION_SERVER_API_VERSION}/users/setProfilePic`;
        let data = new FormData();

        const file = {
            uri: pictureData.filename,             // e.g. 'file:///path/to/file/image123.jpg'
            name: pictureData.filename.substr(pictureData.filename.lastIndexOf("/") + 1),            // e.g. 'image123.jpg',
            type: pictureData.mime,             // e.g. 'image/jpg'
        }

        data.append('file', file);

        return secureFetch(url, {
            method: 'POST',
            body: data,
        })
            // Fetch success - convert to JSON
            .then((res) => {
                if (!res.ok) {
                    reportError('Upload profile pic failed: ', res);
                    return dispatch(failureSetProfilePicture(error));
                    // Not the end of the world - ignore error. User can set profile pic later...
                }
                return res.json();
            })
            .then((json) => {
                dispatch(receiveSetProfilePicture(json.userProfilePic));
            })
            // Fetch failure
            .catch((error) => {
                // Not the end of the world - ignore error. User can set profile pic later...
                dispatch(failureSetProfilePicture(error));
                console.info('Upload profile pic caught error: ', error);
            });

    }
}

export const REQUEST_OWN_DETAILS = 'REQUEST_OWN_DETAILS'
function requestOwnUserDetails() {
    return {
        type: REQUEST_OWN_DETAILS,
    }
}

export const RECEIVE_OWN_DETAILS = 'RECEIVE_OWN_DETAILS'
export function receiveOwnUserDetails(userDetails) {
    return {
        type: RECEIVE_OWN_DETAILS,
        userDetails,
    }
}

export const FAILURE_OWN_DETAILS = 'FAILURE_OWN_DETAILS'
function failureOwnUserDetails(error) {
    return {
        type: FAILURE_OWN_DETAILS,
        error: error,
    }
}

export function fetchOwnUserDetails() {
    return function (dispatch) {
        // First dispatch: the app state is updated to inform
        // that the API call is starting.

        dispatch(requestOwnUserDetails());

        let url = `${AUTHORIZATION_SERVER_ADDRESS}/api/${AUTHORIZATION_SERVER_API_VERSION}/users/ownDetails`;

        return secureFetch(url, {
            method: 'GET',
            headers: {
                'Content-Type': 'application/json',
            },
        })
            .then(response => response.json())
            .then(json => {
                if (json.success) 
                    dispatch(receiveOwnUserDetails(json.userDetails))
                else {
                    reportError('fetchOwnUserDetails: secure fetch error occured. ', json);
                    dispatch(failureOwnUserDetails(json));
                }
            })
            .catch(error => {
                reportError('fetchOwnUserDetails: secure fetch caught error: ', error)
                dispatch(failureOwnUserDetails(error));
            })
    }
}
