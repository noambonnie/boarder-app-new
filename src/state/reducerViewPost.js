import {combineReducers} from 'redux';
import comments from "./reducerComments"
import postDetails from "./reducerPostDetails"

const viewPostReducer = combineReducers({
    comments,
    postDetails,
});

export default viewPostReducer;
