import { AsyncStorage, Alert } from 'react-native';
import { FEED_SERVER_ADDRESS, FEED_SERVER_API_VERSION } from '../common/constants';
import { secureFetch } from '../common/authApi';
import { reportError } from '../common/errorReporter';
import GooglePlacesSearch from '../common/googlePlacesSearch';
import Strings from '../assets/strings';

const googlePlacesSearch = new GooglePlacesSearch();
export const REQUEST_POSTS = 'REQUEST_POSTS'
function requestPosts(page, firstPostDate) {
    return {
        type: REQUEST_POSTS,
        page: page,
        firstPostDate: firstPostDate,
    }
}

export const RECEIVE_POSTS = 'RECEIVE_POSTS'
function receivePosts(json) {
    return {
        type: RECEIVE_POSTS,
        posts: json.posts,
        serverTime: json.now,   // User server time to calculate today/yesterday/x days ago
    }
}

export const FAILURE_POSTS = 'FAILURE_POSTS'
function failurePosts(error) {
    return {
        type: FAILURE_POSTS,
        error: error,
    }
}

// NBTODO: Is this action needed? Maybe if page === 1 we consider it a refresh and set isRefreshing?
//         Currently all it does is tell the UI to show the refresh spinner.
//         I feel like my current way of deciding to refresh based on changes to the state
//         is wrong. I should revisit the redux example to see how this is used. I should be able to dispatch
//         an action to cause a refresh.
export const REFRESH_POSTS = 'REFRESH_POSTS'
export function refreshPosts() {
    return {
        type: REFRESH_POSTS,
    }
}

export const SET_FEED_FILTER = 'SET_FEED_FILTER'
export function setFeedFilter(filter) {
    return {
        type: SET_FEED_FILTER,
        filter,
    }
}

export function applyFilter() {
    return async function (dispatch, getState) {
        try {
            await AsyncStorage.setItem("posts.filter", JSON.stringify(getState().posts.filter))
        }
        catch (error) {
            console.warn("Failed to write filter to storage.", error);
        }
        
        dispatch(setSearchFocused(false));
        dispatch(setSearchQuery(""));
        dispatch(setSearchPredictions([]));
        dispatch(refreshPosts());
        dispatch(fetchPosts(1, null))
    }
}

export const SET_SEARCH_FOCUSED = 'SET_SEARCH_FOCUSED'
export function setSearchFocused(isSearchFocused) {
    return {
        type: SET_SEARCH_FOCUSED,
        isSearchFocused,
    }
}

export const SET_SEARCH_QUERY = 'SET_SEARCH_QUERY'
export function setSearchQuery(query) {
    return {
        type: SET_SEARCH_QUERY,
        query,
    }
}

export const SET_SEARCH_PREDICTIONS = 'SET_SEARCH_PREDICTIONS'
export function setSearchPredictions(predictions) {
    return {
        type: SET_SEARCH_PREDICTIONS,
        predictions,
    }
}

export function locationQuery(query) {
    return async function (dispatch, getState) {
        try {
            dispatch(setSearchQuery(query));
            const predictions = await googlePlacesSearch.search(getState().posts.locationSearch.searchQuery);
            dispatch(setSearchPredictions(predictions))
        }
        catch (error) {
            console.error(error);
        }
    }
}

export function setFeedDistance(distance) {
    return async function (dispatch, getState) {
        const filter = { ...getState().posts.filter };
        filter.byDistance.distance = distance;
        dispatch(setFeedFilter(filter));
    }
}


export function setFeedLocation(placeId) {
    return async function (dispatch, getState) {
        try {
            const filter = { ...getState().posts.filter };

            if (placeId != null) {
                const placeDetails = await googlePlacesSearch.getPlaceDetails(placeId);
                if (placeDetails.result.types.indexOf("country") >= 0 || placeDetails.result.types.indexOf("administrative_area_level_1") >= 0) {
                    return Alert.alert(Strings.t("locationTooBroadTitle"), Strings.t("locationTooBroadText"));
                }

                const { name, geometry: { location: { lat: latitude, lng: longitude } } } = placeDetails.result;
                filter.byDistance.address = name;
                filter.byDistance.position = { latitude, longitude };
            }
            else {
                filter.byDistance.address = null;
                filter.byDistance.position = null;
            }

            dispatch(setFeedFilter(filter));
            dispatch(applyFilter())
        }
        catch (error) {
            Alert.alert(Strings.t("somethingWentWrongAlertTitle"), Strings.t('somethingWentWrongAlert'));
            console.error(error)
        }
    }
}

export function fetchPosts(page, firstPostDate) {
    return function (dispatch, getState) {
        // First dispatch: the app state is updated to inform
        // that the API call is starting.

        dispatch(requestPosts(page, firstPostDate));

        let url = `${FEED_SERVER_ADDRESS}/api/${FEED_SERVER_API_VERSION}/posts/feed?page=${page}&items=10`;
        if (firstPostDate) {
            url += `&startDate=${firstPostDate}`;
        }

        // Still can't find a clean way to clone an object so for now using the good ol' way.
        // https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Object/assign
        let filter = JSON.parse(JSON.stringify(getState().posts.filter));
        filter.byDistance.position = filter.byDistance.position || getState().location.position;
        filter.byDistance.distance *= 1000; // Convert km to meter.

        return secureFetch(url, {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
            },
            body: JSON.stringify(filter)
        })
            .then(response => response.json())
            .then(json => {
                if (json.success) {
                    dispatch(receivePosts(json));
                }
                else {
                    reportError('fetchPosts: secure fetch error occured.', json);
                    dispatch(failurePosts(json));
                }
            })
            .catch(error => {
                reportError('fetchPosts: secure fetch caught error: ', error);
                dispatch(failurePosts(error));
            })
    }
}