'use strict'
import * as Actions from "./actionsPosts";
import { LOGOUT } from "../common/authApi";

export const initialState = {
    isLoading: false,
    isRefreshing: false,
    page: 1,
    items: [],
    loadError: false,
    filter: {
        byDistance: {
            distance: 25,
        }
    },
    locationSearch: {
        isSearchFocused: false,
        searchQuery: "",
        predictions: [],
    }
};

export default function (state = initialState, action) {
    switch (action.type) {
        case Actions.REFRESH_POSTS:
            return Object.assign({}, state, {
                isRefreshing: true,
                items: [],            // NBTODO: Should I clear the current posts on refresh? What do other apps do? How do I show a spinner if keeping posts.
                page: 1,
                firstPostDate: null,
                loadError: false,
            })
        case Actions.REQUEST_POSTS:
            return Object.assign({}, state, {
                isLoading: true,
                isRefreshing: false,
                page: action.page,
                firstPostDate: action.firstPostDate,
                loadError: false,
            })
        case Actions.RECEIVE_POSTS:
            return Object.assign({}, state, {
                isLoading: false,
                isRefreshing: false,
                items: (state.page === 1 ? action.posts : [...state.items, ...action.posts]),
                serverTime: action.serverTime,
                firstPostDate: state.page === 1 ? (action.posts.length > 0 && action.posts[0].postDate) : state.firstPostDate,
                resultsAvailable: action.posts.length > 0, // NBTODO: Should be length == 10 (or however many items we asked for)
            })
        case Actions.FAILURE_POSTS: {
            return Object.assign({}, state, {
                loadError: true,
            })
        }
        case Actions.SET_FEED_FILTER: {
            return {
                ...state,
                filter: { ...state.filter, ...action.filter },
            };
        }
        case Actions.SET_SEARCH_FOCUSED: {
            return {
                ...state,
                locationSearch: {
                    ...state.locationSearch,
                    isSearchFocused: action.isSearchFocused,
                }
            }
        }
        case Actions.SET_SEARCH_QUERY: {
            return {
                ...state,
                locationSearch: {
                    ...state.locationSearch,
                    searchQuery: action.query,
                }
            }
        }
        case Actions.SET_SEARCH_PREDICTIONS: {
            return {
                ...state,
                locationSearch: {
                    ...state.locationSearch,
                    predictions: [...action.predictions],
                }
            }
        }
        case LOGOUT: {
            return initialState;
        }
        default:
            return state;
    }

}
