import { secureFetch } from '../common/authApi';
import { AUTHORIZATION_SERVER_ADDRESS, 
         AUTHORIZATION_SERVER_API_VERSION, } from '../common/constants';
import { reportError } from '../common/errorReporter';

export const APP_STATE_CHANGE = 'APP_STATE_CHANGE';
export function appStateChanged(newState) {
    return {
        type: APP_STATE_CHANGE,
        newState,
    }
}

export const SET_NOTIFICATION_TOKEN = 'SET_NOTIFICATION_TOKEN'
export function setNotificationToken(token) {
    return {
        type: SET_NOTIFICATION_TOKEN,
        notificationToken: token,
    }
}

export const SHOW_POSTS_COACH_MARKS = 'SHOW_POSTS_COACH_MARKS'
export function setShowPostsCoachMarks(showPostsCoachMarks) {
    return {
        type: SHOW_POSTS_COACH_MARKS,
        showPostsCoachMarks,
    }
}
export const REQUEST_UPDATE_NOTIFICATION_TOKEN = 'REQUEST_UPDATE_NOTIFICATION_TOKEN'
function requestUpdateNotificationToken() {
    return {
        type: REQUEST_UPDATE_NOTIFICATION_TOKEN,
    }
}

export const RECEIVE_UPDATE_NOTIFICATION_TOKEN = 'RECEIVE_UPDATE_NOTIFICATION_TOKEN'
export function receiveUpdateNotificationToken(userDetails) {
    return {
        type: RECEIVE_UPDATE_NOTIFICATION_TOKEN,
    }
}

export const FAILURE_UPDATE_NOTIFICATION_TOKEN = 'UPDATE_NOTIFICATION_TOKEN'
function failureUpdateNotificationToken(error) {
    return {
        type: FAILURE_UPDATE_NOTIFICATION_TOKEN,
        error: error,
    }
}

export function updateNotificationToken(token) {
    return function (dispatch) {
        // If the notification token update was not successfull, we currently will retry upon next launch of the app. 
        // Maybe that's good enough.

        // First dispatch: the app state is updated to inform
        // that the API call is starting.
        dispatch(setNotificationToken(token));

        dispatch(requestUpdateNotificationToken());

        let url = `${AUTHORIZATION_SERVER_ADDRESS}/api/${AUTHORIZATION_SERVER_API_VERSION}/setNotificationToken/${token}`;

        return secureFetch(url, {
            method: 'GET',
            headers: {
                'Content-Type': 'application/json',
            },
        })
            .then(response => response.json())
            .then(json => {
                if (json.success) {
                    console.info("Successfully updated notification token ", token);
                    dispatch(receiveUpdateNotificationToken(json.userDetails))
                }
                else {
                    reportError('updateNotificationToken: secure fetch error occured. ', json);
                    dispatch(failureUpdateNotificationToken(json));
                }
            })
            .catch(error => {
                reportError('updateNotificationToken: secure fetch caught error: ', error)
                dispatch(failureUpdateNotificationToken(error));
            })
    }
}
