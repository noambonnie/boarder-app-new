import { LOGIN, LOGOUT, REQUEST_RESET_TOKEN, RECEIVE_RESET_TOKEN, } from "../common/authApi";
import * as UserActions from "./actionsUser";

const initialState = {
    tokens: {
        accessToken: null,
        refreshToken: null,
        refreshTokenPromise: null,
    },
    info: {}
}

export default function (state = initialState, action) {
    switch (action.type) {
        case LOGIN:
            // Set tokens.
            return Object.assign({}, state,
                {
                    tokens: { ...action.tokens },
                    info: { ...action.user },
                })
        case LOGOUT:
            // Clear system state.
            return initialState;
        case REQUEST_RESET_TOKEN:
            // Set a promise to refresh the tokens. Others trying to refresh the token will wait on this promnise.
            return Object.assign({}, state, {
                tokens: Object.assign({}, state.tokens, {
                    refreshTokenPromise: action.refreshTokenPromise
                })
            })
        case RECEIVE_RESET_TOKEN:
            // Set the tokens, reset the refresh promise.
            return Object.assign({}, state, {
                tokens: {
                    refreshTokenPromise: null,
                    accessToken: action.tokens.accessToken,
                    refreshToken: action.tokens.refreshToken,
                }
            })
        case UserActions.REQUEST_SET_PROFILE_PICTURE: {
            return Object.assign({}, state, {
                isSavingProfilePic: true,
                profilePicSetError: false,
            })
        }
        case UserActions.RECEIVE_SET_PROFILE_PICTURE: {
            return Object.assign({}, state, {
                info: {
                    ...state.info,
                    profilePic: action.profilePic,
                },
                isSavingProfilePic: false,
                profilePicSetError: false,
            })
        }
        case UserActions.FAILURE_SET_PROFILE_PICTURE: {
            return Object.assign({}, state, {
                isSavingProfilePic: false,
                profilePicSetError: true,
            })
        }
        case UserActions.RESET_SET_PROFILE_PICTURE: {
            return Object.assign({}, state, {
                isSavingProfilePic: false,
                profilePicSetError: false,
            })
        }
        case UserActions.REQUEST_OWN_DETAILS:
            return Object.assign({}, state, {
                isLoading: true,
                loadError: false,
            })
        case UserActions.RECEIVE_OWN_DETAILS:
            return Object.assign({}, state, {
                isLoading: false,
                isRefreshing: false,
                info: {
                    ...state.info,
                    ...action.userDetails,
                },
            })
        case UserActions.FAILURE_OWN_DETAILS: {
            return Object.assign({}, state, {
                loadError: true,
            })
        }
        default:
            return state;
    }

}