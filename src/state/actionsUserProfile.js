import { AUTHORIZATION_SERVER_ADDRESS, AUTHORIZATION_SERVER_API_VERSION } from '../common/constants';
import { secureFetch } from '../common/authApi';
import { reportError } from '../common/errorReporter';

export const REQUEST_USER_DETAILS = 'REQUEST_USER_DETAILS'
function requestUserDetails(userId) {
    return {
        type: REQUEST_USER_DETAILS,
        userId,
    }
}

export const RECEIVE_USER_DETAILS = 'RECEIVE_USER_DETAILS'
export function receiveUserDetails(userDetails) {
    return {
        type: RECEIVE_USER_DETAILS,
        userDetails,
    }
}

export const FAILURE_USER_DETAILS = 'FAILURE_USER_DETAILS'
function failureUserDetails(error) {
    return {
        type: FAILURE_USER_DETAILS,
        error: error,
    }
}

export function fetchUserDetails(userId) {
    return function (dispatch) {
        // First dispatch: the app state is updated to inform
        // that the API call is starting.

        dispatch(requestUserDetails(userId));

        let url = `${AUTHORIZATION_SERVER_ADDRESS}/api/${AUTHORIZATION_SERVER_API_VERSION}/users/userDetails/${userId}`;

        return secureFetch(url, {
            method: 'GET',
            headers: {
                'Content-Type': 'application/json',
            },
        })
            .then(response => response.json())
            .then(json => {
                if (json.success) 
                    dispatch(receiveUserDetails(json.userDetails))
                else {
                    reportError('fetchUserDetails: secure fetch error occured. ', json);
                    dispatch(failureUserDetails(json));
                }
            })
            .catch(error => {
                reportError('fetchUserDetails: secure fetch caught error: ', error)
                dispatch(failureUserDetails(error));
            })
    }
}
