'use strict'
import * as Actions from "./actionsApp";
import { LOGOUT } from "../common/authApi";

export const initialState = {
    appState: 'active',
    notificationTokenUpdated: false,
    notificationTokenUpdating: false,
    showPostsCoachMarks: true,
};

export default function (state = initialState, action) {
    switch (action.type) {
        case Actions.APP_STATE_CHANGE:
            return {
                ...state,
                appState: action.newState,
            };
        case Actions.SET_NOTIFICATION_TOKEN:
            return {
                ...state,
                notificationToken: action.notificationToken,
                notificationTokenUpdated: false,
            }
        case Actions.REQUEST_UPDATE_NOTIFICATION_TOKEN:
            // For now do nothing with progress/failure/success
            return { ...state, notificationTokenUpdating: true };
        case Actions.RECEIVE_UPDATE_NOTIFICATION_TOKEN:
            // For now do nothing with progress/failure/success
            return { ...state, notificationTokenUpdated: true, notificationTokenUpdating: false };
        case Actions.FAILURE_UPDATE_NOTIFICATION_TOKEN: {
            // For now do nothing with progress/failure/success
            return { ...state, notificationTokenUpdating: false };
        }
        case Actions.SHOW_POSTS_COACH_MARKS: 
            return { ...state, showPostsCoachMarks: action.showPostsCoachMarks };
        case LOGOUT: {
            return { ...state, notificationTokenUpdated: false, notificationTokenUpdating: false }
        }
        default:
            return state;
    }

}
