import { FEED_SERVER_ADDRESS, FEED_SERVER_API_VERSION } from '../common/constants';
import { secureFetch } from '../common/authApi';
import { fetchPosts, refreshPosts } from './actionsPosts';
import { reportError } from '../common/errorReporter';

export const SET_POST_BUTTON_DISABLED = 'SET_POST_BUTTON_DISABLED'
export function setPostButtonDisabled(buttonDisabled) {
    return {
        type: SET_POST_BUTTON_DISABLED,
        buttonDisabled,
    }
}

export const START_UPLOAD_IMAGES = 'START_UPLOAD_IMAGES'
function startUploadImages() {
    return {
        type: START_UPLOAD_IMAGES,
    }
}

export const DONE_UPLOAD_IMAGES = 'DONE_UPLOAD_IMAGES'
function doneUploadImages() {
    return {
        type: DONE_UPLOAD_IMAGES,
    }
}
export const REQUEST_ADD_POST = 'REQUEST_ADD_POST'
function requestAddPost() {
    return {
        type: REQUEST_ADD_POST,
    }
}

export const RECEIVE_ADD_POST = 'RECEIVE_ADD_POST'
function receiveAddPost(json) {
    return {
        type: RECEIVE_ADD_POST,
        posts: json.posts,
    }
}

export const FAILURE_ADD_POST = 'FAILURE_ADD_POST'
function failureAddPost(error) {
    return {
        type: FAILURE_ADD_POST,
        error: error,
    }
}

export function addPost(postData) {
    return async (dispatch, getState) => {
        // First dispatch: the app state is updated to inform
        // that the API call is starting.
        dispatch(requestAddPost());
        const { title, text, geolocation, postImageUploaders } = postData;

        const imageUploadPromises = Object.keys(postImageUploaders).map((key) => postImageUploaders[key].uploader.getUploadPromise()) || [];

        // Wait for all images to upload
        dispatch(startUploadImages());
        try {
            await Promise.all(imageUploadPromises);
        }
        catch (error) {
            reportError('Image upload failure', error)
            dispatch(failureAddPost(error));
        }
        dispatch(doneUploadImages());


        const images = Object.keys(postImageUploaders).map((key) => postImageUploaders[key].uploader.key);

        const post = {
            title,
            text,
            geolocation: geolocation.position ? geolocation : { position: { ...getState().location.position } },
            images,
        }

        let url = `${FEED_SERVER_ADDRESS}/api/${FEED_SERVER_API_VERSION}/posts/`;

        return secureFetch(url, {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
            },
            body: JSON.stringify(post),
        })
            .then(response => response.json())
            .then(json => {
                if (json.success) {
                    dispatch(receiveAddPost(json));
                    dispatch(setPostButtonDisabled(true));
                    dispatch(refreshPosts());
                    dispatch(fetchPosts(1)); // Refresh the posts list.
                }
                else {
                    reportError('addPost: secure fetch error occured.', json);
                    dispatch(failureAddPost(json));
                }
            })
            .catch(error => {
                reportError('addPost: secure fetch caught error: ', error)
                dispatch(failureAddPost(error));
            })
    }
}
