'use strict'
import * as Actions from "./actionsNotifications";
import { LOGOUT } from "../common/authApi";

const initialState = {
    isLoading: false,
    isRefreshing: false,
    page: 1,
    items: [],
    loadError: false,
};

export default function (state = initialState, action) {
    switch (action.type) {
        case Actions.REFRESH_NOTIFICATIONS:
            return Object.assign({}, state, {
                isRefreshing: true,
                items: [],            // NBTODO: Should I clear the current notifications on refresh? What do other apps do? How do I show a spinner if keeping notifications.
                page: 1,
                firstNotificationDate: null,
                loadError: false,
            })
        case Actions.REQUEST_NOTIFICATIONS:
            return Object.assign({}, state, {
                isLoading: true,
                isRefreshing: false,
                page: action.page,
                firstNotificationDate: action.firstNotificationDate,
                loadError: false,
            })
        case Actions.RECEIVE_NOTIFICATIONS:
            return Object.assign({}, state, {
                isLoading: false,
                isRefreshing: false,
                items: (state.page === 1 ? action.notifications : [...state.items, ...action.notifications]),
                firstNotificationDate: state.page === 1 ? (action.notifications.length > 0 && action.notifications[0].modifiedAt) : state.firstNotificationDate,
                resultsAvailable: action.notifications.length > 0, // NBTODO: Should be length == 10 (or however many items we asked for)
            })
        case Actions.FAILURE_NOTIFICATIONS: {
            return Object.assign({}, state, {
                loadError: true,
            })
        }
        case Actions.RECEIVE_NOTIFICATION_COUNT: {
            return {...state, count: action.count}
        }
        case LOGOUT: {
            return initialState;
        }
        default:
            return state;
    }
}
