'use strict'
import * as Actions from "./actionsUserProfile";
import { LOGOUT } from "../common/authApi";

const initialState = {
    isLoading: false,
    loadError: false,
    profilePic: '',
    firstName: '',
    lastName: '',
};

export default function (state = initialState, action) {
    switch (action.type) {
        case Actions.REQUEST_USER_DETAILS:
            return Object.assign({}, state, {
                isLoading: true,
                loadError: false,
            })
        case Actions.RECEIVE_USER_DETAILS:
            return Object.assign({}, state, {
                isLoading: false,
                isRefreshing: false,
                profilePic: action.userDetails.profilePic,
                firstName: action.userDetails.firstName,
                lastName: action.userDetails.lastName,
            })
        case Actions.FAILURE_USER_DETAILS: {
            return Object.assign({}, state, {
                loadError: true,
            })
        }
        case LOGOUT: {
            return initialState;
        }
        default:
            return state;
    }

}
