// NBTODO: use strict everywhere...
'use strict'

export const RECEIVE_CURRENT_LOCATION = 'RECEIVE_CURRENT_LOCATION'
export function receiveCurrentLocation(location) {
    return {
        type: RECEIVE_CURRENT_LOCATION,
        location,
    }
}

export const FAILURE_CURRENT_LOCATION = 'FAILURE_CURRENT_LOCATION'
export function failureCurrentLocation() {
    return {
        type: FAILURE_CURRENT_LOCATION,
    }
}
