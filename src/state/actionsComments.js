import { FEED_SERVER_ADDRESS, FEED_SERVER_API_VERSION } from '../common/constants';
import { secureFetch } from '../common/authApi';
import { reportError } from '../common/errorReporter';

export const REQUEST_COMMENTS = 'REQUEST_COMMENTS'
function requestComments(postId) {
    return {
        type: REQUEST_COMMENTS,
        postId: postId,
    }
}

// I export receiveComments function becauase I'll use it to reset comments data
// when entering a new screen. There might be a better way to cause a screen to re-render.
export const RECEIVE_COMMENTS = 'RECEIVE_COMMENTS'
export function receiveComments(comments) {
    return {
        type: RECEIVE_COMMENTS,
        comments: comments,
    }
}

export const FAILURE_COMMENTS = 'FAILURE_COMMENTS'
function failureComments(error) {
    return {
        type: FAILURE_COMMENTS,
        error: error,
    }
}

export const REQUEST_ADD_COMMENT = 'REQUEST_ADD_COMMENT'
function requestAddComment(postId, comment) {
    // NBTODO: I don't think postId and comment are being used in the reducer and are therefore unnecessary.
    return {
        type: REQUEST_ADD_COMMENT,
    }
}

export const RECEIVE_ADD_COMMENT = 'RECEIVE_ADD_COMMENT'
function receiveAddComment(json) {
    return {
        type: RECEIVE_ADD_COMMENT,
    }
}

export const FAILURE_ADD_COMMENT = 'FAILURE_ADD_COMMENT'
function failureAddComment(error) {
    return {
        type: FAILURE_ADD_COMMENT,
        error: error,
    }
}
// NBTODO: Is this action needed? Maybe if page === 1 we consider it a refresh and set isRefreshing?
export const REFRESH_COMMENTS = 'REFRESH_COMMENTS'
export function refreshComments() {
    return {
        type: REFRESH_COMMENTS,
    }
}


export function fetchComments(postId) {
    return function (dispatch) {
        // First dispatch: the app state is updated to inform
        // that the API call is starting.

        dispatch(requestComments(postId));

        let url = `${FEED_SERVER_ADDRESS}/api/${FEED_SERVER_API_VERSION}/posts/comment/${postId}`;

        return secureFetch(url, {
            method: 'GET',
            headers: {
                'Content-Type': 'application/json',
            },
        })
            .then(response => response.json())
            .then(json => {
                if (json.success)
                    dispatch(receiveComments(json.comments))
                else {
                    reportError('fetchComments: secure fetch error occured. ', json);
                    dispatch(failureComments(json));
                }
            })
            .catch(error => {
                reportError('fetchComments: secure fetch caught error: ', error)
                dispatch(failureComments(error));
            })
    }
}

export function addComment(postId, comment) {
    return function (dispatch) {
        // First dispatch: the app state is updated to inform
        // that the API call is starting.

        dispatch(requestAddComment(postId, comment));

        let url = `${FEED_SERVER_ADDRESS}/api/${FEED_SERVER_API_VERSION}/posts/comment/${postId}`;

        return secureFetch(url, {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
            },
            body: JSON.stringify(comment),
        })
            .then(response => response.json())
            .then(json => {
                if (json.success) {
                    dispatch(receiveAddComment());
                    dispatch(receiveComments(json.comments));
                }
                else {
                    reportError('addComment: secure fetch error occured.', json);
                    dispatch(failureAddComment(json));
                }
            })
            .catch(error => {
                reportError('addComment: secure fetch caught error: ', error)
                dispatch(failureAddComment(error));
            })
    }
}

