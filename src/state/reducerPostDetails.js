'use strict'
import * as Actions from "./actionsPostDetails";
import { LOGOUT } from "../common/authApi";

const initialState = {
    isLoading: false,
    loadError: false,
    title: '',
    text: '',
    postedBy: {},
};

export default function (state = initialState, action) {
    switch (action.type) {
        case Actions.REQUEST_POST_DETAILS:
            return Object.assign({}, state, {
                isLoading: true,
                loadError: false,
            })
        case Actions.RECEIVE_POST_DETAILS:
            return Object.assign({}, state, {
                isLoading: false,
                isRefreshing: false,
                title: action.post.title,
                text: action.post.text,
                postedBy: { ...action.post.postedBy },
                postDate: action.post.postDate,
                images: [ ...action.post.images || []],
                originalImagesPrefix: action.post.originalImagesPrefix,
                imageResizerPrefixURL: action.post.imageResizerPrefixURL,
            })
        case Actions.FAILURE_POST_DETAILS: {
            return Object.assign({}, state, {
                loadError: true,
            })
        }
        case LOGOUT: {
            return initialState;
        }
        default:
            return state;
    }

}
