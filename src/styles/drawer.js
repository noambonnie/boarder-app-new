import { StyleSheet } from 'react-native';
import StyleConstants from './constants';

const styles = StyleSheet.create({
	container: {
		flex: 1,
		backgroundColor: StyleConstants.BRAND_COLOR_DARK,  
	},
	drawerListIcon: {
		width: 25,
		height: 25,
		marginRight: 10
	},
	drawerListItem: {
		flexDirection: 'row',
		alignItems: 'center',
		marginBottom: 10,
		paddingHorizontal: 25,
	},
	drawerProfile: {
		marginBottom: 10,
		marginTop: 20, 
		paddingHorizontal: 25,
	},
	drawerListItemText: {
		color: StyleConstants.LIGHT_TEXT_COLOR,
		fontWeight: 'normal',
		fontSize: 20,
		flex: 1
	},
	debug: {
		color: StyleConstants.LIGHT_TEXT_COLOR,
		fontSize: 12,
	}
});

export default styles;