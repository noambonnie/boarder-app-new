import { StyleSheet, Platform } from "react-native";

export default Constants = {
    BRAND_COLOR_LIGHT: "#6495ed",
    BRAND_COLOR_DARK: "#4169e1", // css royalblue
    BRAND_COLOR_LIGHT_DISABLED: "#AEC7F6",
    LIGHT_TEXT_COLOR: "white",
    BACKGROUND_COLOR: "ghostwhite",
    UNSELECTED_TAB_COLOR: "lightblue",
    COMMENT_BG_COLOR: '#e1e6f5',
    UNREAD_NOTIFICATION_BG_COLOR: '#ecf0fc',
    PHOTOS_UPLOAD_COLOR: "forestgreen"
}

export const cardDefaultHeaderStyle = {
    headerTintColor: Constants.BRAND_COLOR_LIGHT,
    headerStyle: {
        backgroundColor: Constants.BACKGROUND_COLOR,
        elevation: 0,       //remove shadow on Android
        shadowOpacity: 0,   //remove shadow on iOS
        borderBottomWidth: 1,
        borderBottomColor: "lightgrey"
    },
}

export const cardDefaultStyle = {
    backgroundColor: Constants.BACKGROUND_COLOR,
}

export const navigationStylesheet = StyleSheet.create({
    headerText: {
        fontSize: Platform.OS === 'ios'? 17 : 20,
        marginLeft: Platform.OS === 'ios'? 0 : 10,
        color: Constants.BRAND_COLOR_LIGHT,
        fontWeight: "bold",
    },
    headerActionButton: { 
        fontSize: Platform.OS === 'ios'? 17 : 18,
        color: Constants.BRAND_COLOR_LIGHT,
        paddingHorizontal: 10,
    },
})
